<?php

/**
 * Home controller
 * @author Fred<mconyango@gmail.com>
 */
class DefaultController extends Controller {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'uploads', 'verification', '_reportCSV', 'summaryreport', 'supplierreport', 'redeemed', 'paymentapproval', 'supplierinvoices', 'supplier', 'query', 'invoicevalidation', 'invoicevalidationforagrodealer', 'agrodealerinputs', 'confirm', 'supplierapprovals', 'suppliersearch', 'inputsummaryreport',
                    'districtme', 'test', 'messages','messagesreplied', 'messagereplydelete','messagesreply', 'messagedelete', 'chat', 'chart', 'viewredemptions', 'evoucherinputs', 'evouchersupplierperformance', 'evoucheragrodealerinputs'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionViewredemptions() {
        $condition = ' agrodealer_id=' . Yii::app()->user->agrodealer_id;
        $this->render('viewredemptions', array(
            'model' => Postransactions::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], '', $condition),
        ));
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionChart() {

        if (Yii::app()->user->user_level === UserLevels::LEVEL_AGRODEALER) {
            $this->redirect(array('verification'));
        } elseif (Yii::app()->user->user_level === UserLevels::LEVEL_DISTRICTME) {
            //  echo "I ak he";
            $this->redirect(array('verification'));
        } elseif (Yii::app()->user->user_level === UserLevels::LEVEL_PROVINCEME) {
            //  echo "I ak he";
            $this->redirect(array('verification'));
        }

        $this->pageTitle = Lang::t('Dashboard');
        $this->activeMenu = 1;

        //Get data from model
        $season_id=null;
        if (isset($_POST['Vwfarmerredemption']['season_id']) && $_POST['Vwfarmerredemption']['season_id'] != '') {
            $season_id=$_POST['Vwfarmerredemption']['season_id'];
        } else{
           $season_id= Agriseason::model()->getScalar('season_id','active=1');
        }
            $province = Vwfarmerredemption::getRedeemedVoucherByProvince($season_id);
            $national = Vwfarmerredemption::getNationalRedemption($season_id);
            $type = Vwfarmerredemption::getRedeemedVoucherType($season_id);
            $gender = Vwfarmerredemption::getRedeemedVoucherGender($season_id);

        $this->render('chart', array(
            'province' => $province,
            'national' => $national,
            'type' => $type,
            'gender' => $gender
        ));
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {

        if (Yii::app()->user->user_level === UserLevels::LEVEL_AGRODEALER) {
            $this->redirect(array('verification'));
        } elseif (Yii::app()->user->user_level === UserLevels::LEVEL_DISTRICTME) {
            //  echo "I ak he";
            $this->redirect(array('verification'));
        } elseif (Yii::app()->user->user_level === UserLevels::LEVEL_PROVINCEME) {
            //  echo "I ak he";
            $this->redirect(array('verification'));
        }

        $this->pageTitle = Lang::t('Dashboard');
        $this->activeMenu = 1;


        $this->render('index', array(
        ));
    }



    public function actionSupplier() {
        $id = Yii::app()->user->id;
        $username = Yii::app()->user->username;
        $reset_password = (int) Suppliers::model()->getScalar('reset_password', '`user_id`=:id AND `supplier_code`=:ag', array(':id' => $id, ':ag' => $username));
        if ($reset_password === 1) {
            $this->redirect(Yii::app()->createUrl('users/default/changePassword'));
        }
        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $this->pageTitle = Lang::t('Supplier');

        $this->render('supplier', array(
            'season' => $seasonName,
            'model' => Invoices::model()->searchModel(array('user_id' => Yii::app()->user->id, 'season_id' => $seasonId)),
        ));
    }

    public function actionSupplierinvoices() {
        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        if (isset($_POST['export'])) {
            $invoice_id = $_POST['invoice_id'];
            $this->renderPartial('_approvalCSVsupplier', array('season' => $seasonName, 'invoice_id' => $invoice_id));
        } else {
            $this->pageTitle = Lang::t('Supplier');

            $this->render('supplier', array(
                'season' => $seasonName,
                'model' => Invoices::model()->searchModel(array('user_id' => Yii::app()->user->id, 'season_id' => $seasonId)),
            ));
        }
    }

    public function actionSuppliersearch() {
        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $this->pageTitle = Lang::t('Invoices');
        $condition = '';
        $season = isset($_GET['Invoices']['season_id']) ? $_GET['Invoices']['season_id'] : '';
        $district = isset($_GET['Invoices']['district_id']) ? $_GET['Invoices']['district_id'] : '';
        $agrodealer_code = isset($_GET['Invoices']['agrodealer_code']) ? $_GET['Invoices']['agrodealer_code'] : '';
        $invoice_status_id = isset($_GET['Invoices']['invoice_status_id']) ? $_GET['Invoices']['invoice_status_id'] : '';
        if (isset($_GET['Invoices']['season_id']) && !empty($_GET['Invoices']['season_id'])) {
            $condition .=empty($condition) ? '' : ' AND ';
            $condition .= 'season_id=' . "$season";
        }
        if (isset($_GET['Invoices']['district_id']) && !empty($_GET['Invoices']['district_id'])) {
            $condition .=empty($condition) ? '' : ' AND ';
            $condition .= 'agrodealer_id IN (SELECT  agrodealer_id FROM tblsuagrodealers WHERE district_id = ' . $district . ')';
        }
        if (isset($_GET['Invoices']['agrodealer_code']) && !empty($_GET['Invoices']['agrodealer_code'])) {
            $condition .=empty($condition) ? '' : ' AND ';
            $condition .= 'agrodealer_id =' . $agrodealer_code;
        }if (isset($_GET['Invoices']['invoice_status_id']) && !empty($_GET['Invoices']['invoice_status_id'])) {
            $condition .=empty($condition) ? '' : ' AND ';
            $condition .= 'invoice_status_id =' . $invoice_status_id;
        }

        $this->render('suppliersearch', array(
            'season' => $seasonName,
            'model' => Invoices::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'invoice_no', $condition),
        ));
    }

    public function actionPaymentapproval() {
        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $this->pageTitle = Lang::t('Payment Approval');

        $this->render('paymentapproval', array(
            'season' => $seasonName,
            'model' => Invoices::model()->searchModel(array('user_id' => Yii::app()->user->id, 'agrodealer_id' => Yii::app()->user->agrodealer_id, 'season_id' => $seasonId, 'invoice_status_id' => 2)),
        ));
    }

    public function actionSupplierapprovals() {
        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $this->pageTitle = Lang::t('Payment Approval');
        $this->render('supplierapprovals', array(
            'season' => $seasonName,
            'model' => Invoices::model()->searchModel(array('user_id' => Yii::app()->user->id, 'agrodealer_id' => Yii::app()->user->agrodealer_id, 'season_id' => $seasonId)),
        ));
    }

    public function actionInvoicevalidationforagrodealer() {
        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $this->pageTitle = Lang::t('Invoice Validation');
        $this->render('invoicevalidationforagrodealer', array(
            'season' => $seasonName,
            'model' => Invoices::model()->searchModel(array('user_id' => Yii::app()->user->id, 'agrodealer_id' => Yii::app()->user->agrodealer_id, 'season_id' => $seasonId)),
        ));
    }

    public function actionInputsummaryreport() {
        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));

        $this->resourceLabel = 'Agrodealer Inputs Summary Report';
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));
        $model = new Vtblvrinvoiceinputs('search');
        $model->unsetAttributes();
        if (isset($_GET['Vtblvrinvoiceinputs'])) {
            $model->attributes = $_GET['Vtblvrinvoiceinputs'];
            $model->agrodealer_id = Yii::app()->user->agrodealer_id;
        } else
            $model->district_id = -1; //prevent display of records on loading of page
        $this->render('agrodealerinputs', array(
            'model' => $model,
            'user_id' => Yii::app()->user->id, 'agrodealer_id' => Yii::app()->user->agrodealer_id, 'season' => $seasonId
        ));
    }

    public function actionSummaryreport() {

        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        if (isset($_POST['export'])) {
            $condition = $_POST['condition'];
            $this->renderPartial('_reportCSV', array('season' => $seasonName, 'condition' => $condition));
        } else {
            $this->pageTitle = Lang::t('Summary Report');
            $user_id = Yii::app()->user->id;
            $agrodealer_id = Agrodealers::model()->getScalar('agrodealer_id', '`user_id`=:t1', array(':t1' => $user_id));
            $this->render('summaryreport', array(
                'season' => $seasonName,
                'model' => ReportView::model()->searchModel(array('user_id' => Yii::app()->user->id, 'season_id' => $seasonId)),
            ));
        }
    }

    public function actionSupplierreport() {
        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        if (isset($_POST['export'])) {
            $condition = $_POST['condition'];

            $this->renderPartial('_reportCSVSupplier', array('season' => $seasonName, 'condition' => $condition));
        } else {
            $this->pageTitle = Lang::t('Summary Report');
            $user_id = Yii::app()->user->id;
            $supplier_id = Suppliers::model()->getScalar('supplier_id', '`user_id`=:t1', array(':t1' => $user_id));
            $this->render('supplierreport', array(
                'season' => $seasonName,
                'model' => ReportView::model()->searchModel(array('user_id' => Yii::app()->user->id, 'season_id' => $seasonId)),
            ));
        }
    }

    public function actionAgrodealerinputs() {
        $this->resourceLabel = 'Agrodealer Inputs Summary Report';
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));
        $model = new Vtblvrinvoiceinputs('search');
        $model->unsetAttributes();
        if (isset($_GET['Vtblvrinvoiceinputs']))
            $model->attributes = $_GET['Vtblvrinvoiceinputs'];
        else
            $model->district_id = -1; //prevent display of records on loading of page
        $this->render('agrodealerinputforsupplier', array(
            'model' => $model
        ));
    }

    public function actionDistrictme() {
        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $this->render('monitor/frmquest', array(
            'season' => $seasonName,
            'model' => Invoices::model()->searchModel(array('user_id' => Yii::app()->user->id, 'season_id' => $seasonId)),
        ));
    }

    public function actionTest() {
        //  echo 'hello there';
        $model = new VoucherVerification();
        $obj = new VoucherVerification();
        $obj->processSmsInbox();
        $this->render('test/index', array('model' => $model));
    }

    public function actionEvouchersupplierperformance() {
        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $this->resourceLabel = 'Evoucher Supplier Performance Summary';
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));
        $model = new Vwsupplierperformance('search');
        $model->unsetAttributes();
        $supplier_id = Suppliers::model()->getScalar('supplier_id', 'user_id=:user_id', array('user_id' => Yii::app()->user->id));
        $model->supplier_id = $supplier_id ? $supplier_id : 'xxx';
        $this->render('evouchersupplierperformance', array(
            'model' => $model,
            'season' => $seasonName
                )
        );
    }

    public function actionEvoucheragrodealerinputs() {
        $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
        $seasonId = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $this->resourceLabel = 'Evouchers Agrodealer Inputs Summary Report';
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));
        $model = new Vwagrodealerinputs('search');
        $model->unsetAttributes();

        $supplier_id = Suppliers::model()->getScalar('supplier_id', 'user_id=:user_id', array('user_id' => Yii::app()->user->id));
        $model->supplier_id = $supplier_id ? $supplier_id : 'xxx';
        $this->render('evoucheragrodealerinputs', array(
            'model' => $model,
            'season' => $seasonName
                )
        );
    }

    public function actionChat() {
        if (isset($_POST['updatechat'])) {
            $user_id = $_POST['user_id'];
            $url_to_update = $_POST['urltoupdate'];
            //check if the user is already in the db
            $chat_id = Chat::model()->getScalar('chat_id', "user_id=$user_id");
            if ($chat_id > 0) {
                $model = Chat::model()->loadModel($chat_id);
                $model->chat_url = $url_to_update;
                $model->save();
            } else {
                $model = new Chat();
                $model->user_id = $user_id;
                $model->chat_url = $url_to_update;

                $model->save();
            }
        }
        if (isset($_POST['loadchat'])) {
            $user_id = $_POST['user_id'];
            $chat_url = Chat::model()->getScalar('chat_url', "user_id=$user_id");
            if ($chat_url) {
                echo $chat_url;
            } else {
                echo 'nourl';
            }
        }
    }

    public function actionMessages() {
        $this->pageTitle = Lang::t('Messages Help Desk');
        $randmobile = Farmers::getsmsnumber();
        $condition = "receiver='$randmobile' AND response_status=0 OR response_status=1 ";
        if (Yii::app()->request->getParam('Ozekimessagein')) {
            $vn = Yii::app()->request->getParam('Ozekimessagein');
            $search = $vn['_search'];
            $condition .= " AND (sender LIKE '%$search%' OR receiver LIKE '%$search%' OR msg LIKE '%$search%')";
        }

        $this->render('messages/index', array(
            'model' => Ozekimessagein::model()->searchModel(array(), 30, 'sender', $condition),
        ));
    }



    public function actionMessagesreply($id) {
        $model = Ozekimessagein::model()->loadModel($id);
        $this->pageTitle = Lang::t('Reply to   ' . $model->sender);
        if (isset($_POST['workingon'])) {
            $model->response_status = 1;
            $model->status = 1;
            $model->save(false);
        }
        if (isset($_POST['Ozekimessagein']['myresponse'])) {
            //   echo 'kim';
            //exit;
            $message = $_POST['Ozekimessagein']['myresponse'];
            $ozekioutmodel = new Ozekimessageout();
            $ozekioutmodel->sender = $model->receiver;
            $ozekioutmodel->receiver = $model->sender;
            $ozekioutmodel->msg = $message;
            $ozekioutmodel->status = 'send';
            $smslogmodel= new Smshelpdesk();
            $smslogmodel->sender=$model->sender;
            $smslogmodel->message_received=$model->msg;
            $smslogmodel->message_sent=$message;
            $smslogmodel->user_id=  Yii::app()->user->id;
            $smslogmodel->date_received=$model->receivedtime;
             $error_message1 = CActiveForm::validate($smslogmodel);  
              $error_message_decoded1 = json_decode($error_message1);
             if (!empty($error_message_decoded1)) {
                        echo json_encode(array('success' => false, 'message' => $error_message1));
                    }
                    else{
             
            $smslogmodel->save();
                    }
            if ($ozekioutmodel->save()) {
                $model_class_name = get_class($model);

                if (isset($_POST[$model_class_name])) {
                    $model->attributes = $_POST[$model_class_name];
                    $error_message = CActiveForm::validate($model);
                    $error_message_decoded = json_decode($error_message);
                    if (!empty($error_message_decoded)) {
                        echo json_encode(array('success' => false, 'message' => $error_message));
                    } else {
                        
            
            
                        $model->response_status = 2;
                        $model->status = 1;
                        $model->save(false);
                        
                        
                        
                        echo json_encode(array('success' => true, 'message' => 'Text Successfully Sent', 'redirectUrl' => UrlManager::getReturnUrl($this->createUrl('default/messages/index'))));
                    }
                    Yii::app()->end();
                }
            }
        }

        $this->renderPartial('messages/_form', array(
            'model' => Ozekimessagein::model()->searchModel(array()), 'id' => $id,
        ));
    }

    public function actionMessagedelete($id) {
        try {
            // $this->checkPrivilege('Icl', $this->input_privileges, Icl::ACTION_DELETE);

            Ozekimessagein::model()->loadModel($id)->delete();
        } catch (Exception $e) {
            
        }
    }
       public function actionMessagesreplied() {
        $this->pageTitle = Lang::t('Messages Help Desk');
        $randmobile = Farmers::getsmsnumber();
        //$condition = "receiver='$randmobile' AND response_status=0 OR response_status=1 ";
        $condition="";
        if (Yii::app()->request->getParam('Smshelpdesk')) {
            $vn = Yii::app()->request->getParam('Smshelpdesk');
            $search = $vn['_search'];
            $condition .= "(sender LIKE '%$search%' OR message_received LIKE '%$search%' OR message_sent LIKE '%$search%' OR user_id IN (SELECT user_id FROM users WHERE name LIKE '%$search%'))";
        }

        $this->render('messages/index2', array(
            'model' => Smshelpdesk::model()->searchModel(array(), 30, 'sender', $condition),
        ));
    }
       public function actionMessagereplydelete($id) {
        try {
            // $this->checkPrivilege('Icl', $this->input_privileges, Icl::ACTION_DELETE);

            Smshelpdesk::model()->loadModel($id)->delete();
        } catch (Exception $e) {
            
        }
    }

}

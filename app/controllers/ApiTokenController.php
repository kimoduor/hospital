<?php class ApiTokenController extends Controller {
 
    public $layout = false;
 
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'postOnly + heartbeat',
        );
    }
 
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
 
 
            $filterErrorArray = array(
                'code' => $error['code'],
                'errorCode' => $error['errorCode'],
                'message' => $error['message'],
            );
 
            if (Yii::app()->params['apidebug']) {
                $filterErrorArray['type'] = $error['type'];
                $filterErrorArray['file'] = $error['file'];
                $filterErrorArray['line'] = $error['line'];
            }
 
 
            echo CJSON::encode($filterErrorArray);
        }
    }
 
    public function actionHeartBeat() {
        $code = '00';
        $message = 'Heartbeat Successful';
        echo CJSON::encode(array(
            'code' => $code,
            'message' => $message
        ));
    }
 
}
<?php

/**
 * AttachmentManagerController
 *
 * @author mconyango <mconyango@gmail.com>
 */
class AttachmentManagerController extends Controller
{

    public function init()
    {
        $this->resourceLabel = 'Attachment';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + delete',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('create', 'update', 'delete', 'download'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionCreate($type, $parent_id = null)
    {
        $this->pageTitle = Lang::t(Constants::LABEL_CREATE . ' ' . $this->resourceLabel);

        $model = new $type();
        if (!empty($model->parent_id_field))
            $model->{$model->parent_id_field} = $parent_id;
        $model_class_name = get_class($model);

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = json_decode($error_message);
            if (!empty($error_message_decoded)) {
                echo json_encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(false);
                echo json_encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl(Yii::app()->createUrl($this->getAfterSaveRoute(), array('id' => $parent_id, AttachmentManager::ATTACHMENT_ID_GET_PARAM => $model->id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), false, true);
    }

    /**
     *
     * @param string $type model_class_name
     * @param type $id
     */
    public function actionUpdate($type, $id)
    {
        $this->pageTitle = Lang::t(Constants::LABEL_UPDATE . ' ' . $this->resourceLabel);

        $model = $type::model()->loadModel($id);
        $model_class_name = get_class($model);

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $error_message = CActiveForm::validate($model);
            $error_message_decoded = json_decode($error_message);
            if (!empty($error_message_decoded)) {
                echo json_encode(array('success' => false, 'message' => $error_message));
            } else {
                $model->save(false);
                $parent_id = !empty($model->parent_id_field) ? $model->{$model->parent_id_field} : null;
                echo json_encode(array('success' => true, 'message' => Lang::t('SUCCESS_MESSAGE'), 'redirectUrl' => UrlManager::getReturnUrl(Yii::app()->createUrl($this->getAfterSaveRoute(), array('id' => $parent_id, AttachmentManager::ATTACHMENT_ID_GET_PARAM => $model->id)))));
            }
            Yii::app()->end();
        }

        $this->renderPartial('_form', array('model' => $model), false, true);
    }

    /**
     *
     * @param string $type model_class_name
     * @param type $id
     */
    public function actionDelete($type, $id)
    {
        $model = $type::model()->loadModel($id);
        $parent_id = !empty($model->parent_id_field) ? $model->{$model->parent_id_field} : null;
        $model->delete();
        echo json_encode(array('redirectUrl' => Yii::app()->createUrl($this->getAfterSaveRoute(), array('id' => $parent_id))));
    }

    public function actionDownload($type, $id)
    {
        $model = $type::model()->loadModel($id);
        $file = $model->getFilePath();
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $download_name = str_replace(' ', '_', $model->title) . '.' . $ext;

        MyYiiUtils::downloadFile($file, $download_name);
    }

    protected function getAfterSaveRoute()
    {
        $route = filter_input(INPUT_GET, AttachmentManager::AFTERSAVE_GO_TO_ROUTE_GET_PARAM);
        if (empty($route))
            throw new CHttpException(400, Lang::t('400_error'));
        return $route;
    }

}

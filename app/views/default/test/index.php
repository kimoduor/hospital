<?php
$form_id = 'message-form';
$form = $this->beginWidget('CActiveForm', array(
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal'),
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">New Message</h3>
    </div>
    <div class="panel-body">
        <fieldset>
            <legend><?php echo Lang::t('Fill in the details') ?></legend>
            <div class="row">
                <div class="col-md-12">       
                    <div class="form-group">
                        <?php echo CHtml::activeLabelEx($model, 'message *', array('class' => 'col-md-4 control-label')); ?>
                        <div class="col-md-8">
                            <?php echo CHtml::activeTextField($model, 'message', array('class' => 'form-control', 'maxlength' => 255, 'required' => true)); ?>
                            <?php echo CHtml::error($model, 'message') ?>
                        </div>
                    </div>

                </div>

            </div>

        </fieldset>

    </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
            <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Create</button>
                    </div>
    </div>
</div>
<?php $this->endWidget(); ?>

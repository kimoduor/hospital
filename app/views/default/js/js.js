    function reloadPage() {
        location.reload();
    }
    function submitInvoice(invoice) {
        $.ajax
                ({
                    type: "POST",
                    url: "../Invoiceinputs/submitInvoiceForAgrodealer?invoice=" + invoice,
                    data: '',
                    cache: false,
                    success: function()
                    {
                        location.reload();

                    },
                    error: function()
                    {
                        alert('That invoice has unvalidated vouchers. Please validate the vouchers first');

                    }
                });
    }
    function showTextBox(text, label) {
        textbox = document.getElementById(text);
        textbox.style.display = 'block';
        elm = document.getElementById(label);
        elm.style.display = 'none';
    }

    function SaveQuantity(text, label, invoiceinput, save) {
        ajaxloader = document.getElementById('ajaxloader');
        ajaxloader.style.display = 'block';
        var value = document.getElementById(text).value;
        function isNotNumber(o) {
            return isNaN(o - 0);
        }
        if ((save !== 'comments') && (save !== 'vouchercomments')) {
            if (isNotNumber(value)) {
                alert('Enter a valid number');
                return false;
            }
            else if (value === '') {
                alert('enter a number greater than zero')
                return false;
            }
        }
        elm = document.getElementById(label);
        $.ajax
                ({
                    type: "POST",
                    url: "../Invoiceinputs/saveQuantity?invoiceinput=" + invoiceinput + "&value=" + value + "&save=" + save,
                    data: '',
                    cache: false,
                    success: function()
                    {
                        textbox = document.getElementById(text);
                        textbox.style.display = 'none';

                        elm.style.display = 'block';
                        elm.innerHTML = value;
                        ajaxloader.style.display = 'none';
                        //$("#reload").load(location.href + " #reload");
                    },
                    error: function() {
                        ajaxloader.style.display = 'none';
                    }
                });

    }
    function saveMultipleValidation(voucher, invoice) {
        checkboxes = document.getElementsByName('checks');
        var values = [];
        var che = '';
        for (var i = 0, n = checkboxes.length; i <= n; i++) {
            if (document.getElementById(i)) {
                if (document.getElementById(i).checked === true) {
                    values[i] = document.getElementById(i).value;
                    che = true;
                }
            }
        }
        if (che === true) {
            $.ajax
                    ({
                        type: "POST",
                        url: "../Invoiceinputs/saveMultipleValidation?invoice=" + invoice + "&voucher=" + voucher + "&posted=" + values,
                        data: values.toString(),
                        cache: false,
                        success: function()
                        {
                            location.reload();

                        }
                    });
        }
        else {
            alert('No checkbox  selected')
        }
    }
    function toggle(source) {
        checkboxes = document.getElementsByName('checks');
        for (var i = 0, n = checkboxes.length; i < n; i++) {
            checkboxes[i].checked = source.checked;
        }
    }
    function toggle_visibility(tbid, lnkid)
    {
        if (document.all) {
            document.getElementById(tbid).style.display = document.getElementById(tbid).style.display == "block" ? "none" : "block";
        }
        else {
            document.getElementById(tbid).style.display = document.getElementById(tbid).style.display == "table" ? "none" : "table";
        }
        // document.getElementById(lnkid).className = document.getElementById(lnkid).className === "fa fa-caret-square-o-down" ? "fa fa-caret-square-o-down" : "fa fa-caret-square-o-down";
    }
    function saveValidation(voucher, invoice) {
        var values = '';
        elem = document.getElementById(invoice + '_' + voucher);
        ajaxloader = document.getElementById('ajaxloader');
        ajaxloader.style.display = 'block';
        $.ajax
                ({
                    type: "POST",
                    url: "../Invoiceinputs/saveValidation?invoice=" + invoice + "&voucher=" + voucher,
                    data: values,
                    cache: false,
                    success: function()
                    {
                        setTimeout();
                        elem.style.display = 'none';
                        elemshow = document.getElementById(invoice + '__' + voucher);
                        elemshow.style.display = 'block';
                        elemshow.style.color = 'white';
                        elemshow.style.background = '#8CC639';
                        ajaxloader.style.display = 'none';
                    },
                    error: function()
                    {
                        ajaxloader.style.display = 'none';
                        alert('Voucher Validation failed. Ensure that the suppliers who supplied the inputs are shortlisted to supply or change the quantity and price to 0.00  ')

                    }
                });
    }

    function overwrite(message, id) {
        var element;
        element = document.getElementById(id);
        if (element) {
            element.innerHTML = message;
            element.style.color = 'red';
        }
    }
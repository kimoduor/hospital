<?php
$model=new Vwfarmerredemption();
$form_id = 'chart-form';
$form = $this->beginWidget('CActiveForm', array(
    'id' => $form_id,
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal'),
        ));
$defaultseason=isset($selectedSeason)?$selectedSeason:Agriseason::model()->getScalar('season_id','active=1');
?>
                    <div class="form-group">
                     <?php echo CHtml::activeLabelEx($model, ' Select Season', array('class' => 'col-md-2 control-label')); ?>    
                       
                        <div class="col-md-4">
                            <?php
                            $selectedSeason = isset($_POST['Vwfarmerredemption']['season_id'])?$_POST['Vwfarmerredemption']['season_id']:$defaultseason;
                            $selectedOptionsSeason[$selectedSeason] = array('selected' => 'selected');
                            ?>
                            <?php                            
                            echo CHtml::activeDropDownList($model, 'season_id',Agriseason::model()->getListData(), array('class' => 'select2', 'required' => true, 'submit' => '', 'options' => $selectedOptionsSeason)); ?>
                        </div>
                    </div>   
<?php $this->endWidget(); ?>
<hr />
<?php
$username = Yii::app()->user->username;
$id = Yii::app()->user->id;
$myselectedSeason=isset($selectedSeason)?"in the Season:".Agriseason::model()->get($selectedSeason,'season'):'';
if (Yii::app()->user->user_level == UserLevels::LEVEL_AGRODEALER) {
    $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
    $agrodealer = Agrodealers::model()->loadModel(Yii::app()->user->agrodealer_id);
    $reset_password = (int) Agrodealers::model()->getScalar('reset_password', '`user_id`=:id', array(':id' => $id));
    // var_dump($reset_password); 
    // exit();
    if ($reset_password === 1) {
        $this->redirect(Yii::app()->createUrl('users/default/changePassword'));
    }

    $this->pageTitle = Lang::t('Verification Page');

    $model = new VoucherVerification();

    $post_key = get_class($model);
    if (isset($_POST[$post_key])) {
        $model->attributes = $_POST[$post_key];
        $model->validate();
    }

    $this->render('verification', array(
        'model' => $model,
        'agrodealer' => $agrodealer,
        'season' => $seasonName,
    ));
} else if (Yii::app()->user->user_level == UserLevels::LEVEL_SUPPLIER) {
    $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
    // $supplier = Suppliers::model()->loadModel(Yii::app()->user->supplier_id);
    $reset_password = (int) Suppliers::model()->getScalar('reset_password', '`user_id`=:id', array(':id' => $id));
    if ($reset_password === 1) {
        $this->redirect(Yii::app()->createUrl('users/default/changePassword'));
    } else {
        $this->redirect(Yii::app()->createUrl('default/supplierinvoices'));
    }
}
else if (Yii::app()->user->user_level == UserLevels::LEVEL_DISTRICTME) {
   // echo "I am here";
    $seasonName = Agriseason::model()->getScalar('season', '`active`=:t1', array(':t1' => 1));
    $reset_password = (int) Tblsudistrictmeusers::model()->getScalar('reset_password', '`user_id`=:id', array(':id' => $id));
    if ($reset_password === 1) {
        $this->redirect(Yii::app()->createUrl('users/default/changePassword'));
    } else {
        $this->redirect(Yii::app()->createUrl('default/districtme'));
    }
}
//} else {
//    $this->redirect(array('index'));
//}
if(isset($selectedSeason)){
?>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Voucher Redemption Per Province <?php echo $myselectedSeason;?></h3>
            </div>
            <div class="panel-body">
                <?php
                $this->Widget('ext.highcharts.HighchartsWidget', array(
                    'options' => array(
                        'chart' => array(
                            'plotBackgroundColor' => '#ffffff',
                            'plotBorderWidth' => null,
                            'plotShadow' => false,
                            'height' => 350,
                            'type' => 'column'
                        ),
                        'title' => array('text' => 'Voucher Redemption Per Province'),
                        'xAxis' => array(
                            'categories' => $province['province']
                        ),
                        'yAxis' => array(
                            'title' => array('text' => 'Vouchers')
                        ),
                        'series' => array(
                            array('name' => 'Valid Vouchers', 'data' => isset($province['vouchers'])?$province['vouchers']:0),
                            array('name' => 'Redeemed Vouchers', 'data' => isset($province['redeemed'])?$province['redeemed']:0),
                            array('name' => 'Unredeemed Vouchers', 'data' => isset($province['not_redeemed'])?$province['not_redeemed']:0)
                        ),
                        'credits' => array('enabled' => false),
                        'scripts' => array(
                            'themes/grid'
                        ),
                    )
                ));
                ?>
            </div>
        </div>   
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">National Voucher Redemption <?php echo $myselectedSeason;?></h3>
            </div>
            <div class="panel-body">
                <?php
                $this->Widget('ext.highcharts.HighchartsWidget', array(
                    'options' => array(
                        'chart' => array(
                            'plotBackgroundColor' => '#ffffff',
                            'plotBorderWidth' => null,
                            'plotShadow' => false,
                            'height' => 350,
                        ),
                        'title' => array('text' => 'National Voucher Redemption'),
                        'plotOptions' => array(
                            'pie' => array(
                                'allowPointSelect' => true,
                                'cursor' => 'pointer',
                                'dataLabels' => array(
                                    'enabled' => true,
                                    'color' => '#AAAAAA',
                                    'connectorColor' => '#AAAAAA',
                                ),
                            )
                        ),
                        'tooltip' => array(
                            'formatter' => 'js:function() { return this.point.name+":  <b> ("+this.y+") "+Math.round(this.point.percentage)+"</b>%"; }',
                        ),
                        'series' => array(
                            array(
                                'type' => 'pie',
                                'name' => 'Voucher Redemption',
                                'data' => isset($national)?$national:''
                            ),
                        ),
                        'credits' => array('enabled' => false),
                        'scripts' => array(
                            'themes/grid'
                        ),
                    )
                ));
                ?>
            </div>
        </div>   
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Voucher Type Redemption <?php echo $myselectedSeason;?></h3>
            </div>
            <div class="panel-body">
                <?php
                $this->Widget('ext.highcharts.HighchartsWidget', array(
                    'options' => array(
                        'chart' => array(
                            'plotBackgroundColor' => '#ffffff',
                            'plotBorderWidth' => null,
                            'plotShadow' => false,
                            'height' => 350,
                        ),
                        'title' => array('text' => 'Voucher Type Redemption'),
                        'plotOptions' => array(
                            'pie' => array(
                                'allowPointSelect' => true,
                                'cursor' => 'pointer',
                                'dataLabels' => array(
                                    'enabled' => true,
                                    'color' => '#AAAAAA',
                                    'connectorColor' => '#AAAAAA',
                                ),
                            )
                        ),
                        'tooltip' => array(
                            'formatter' => 'js:function() { return this.point.name+":  <b> ("+this.y+") "+Math.round(this.point.percentage)+"</b>%"; }',
                        ),
                        'series' => array(
                            array(
                                'type' => 'pie',
                                'name' => 'Voucher Redemption Type',
                                'data' =>isset($type)?$type:0 
                            ),
                        ),
                        'credits' => array('enabled' => false),
                        'scripts' => array(
                            'themes/grid'
                        ),
                    )
                ));
                ?>
            </div>
        </div>   
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Voucher Redemption by Gender <?php echo $myselectedSeason;?></h3>
            </div>
            <div class="panel-body">
                <?php
                $this->Widget('ext.highcharts.HighchartsWidget', array(
                    'options' => array(
                        'chart' => array(
                            'plotBackgroundColor' => '#ffffff',
                            'plotBorderWidth' => null,
                            'plotShadow' => false,
                            'height' => 350,
                        ),
                        'title' => array('text' => 'Voucher Redemption by Gender'),
                        'plotOptions' => array(
                            'pie' => array(
                                'allowPointSelect' => true,
                                'cursor' => 'pointer',
                                'dataLabels' => array(
                                    'enabled' => true,
                                    'color' => '#AAAAAA',
                                    'connectorColor' => '#AAAAAA',
                                ),
                            )
                        ),
                        'tooltip' => array(
                            'formatter' => 'js:function() { return this.point.name+":  <b> ("+this.y+") "+Math.round(this.point.percentage)+"</b>%"; }',
                        ),
                        'series' => array(
                            array(
                                'type' => 'pie',
                                'name' => 'Voucher Redemption by Gender',
                                'data' => isset($gender)?$gender:0
                            ),
                        ),
                        'credits' => array('enabled' => false),
                        'scripts' => array(
                            'themes/grid'
                        ),
                    )
                ));
                ?>
            </div>
        </div>   
    </div>
</div>
<?php
}
else{
    echo '<h6><center>Kindly Select Season to Dispaly the charts.</center></h6>';
}
?>
<aside id="left-panel">
    <nav>
        <ul>
            <li class="<?php echo $this->activeMenu === 1 ? 'active' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('default/index') ?>">
                    <i class="fa fa-lg fa-fw fa-dashboard"></i> <span class="menu-item-parent"><?php echo Lang::t('DASHBOARD') ?>
                    </span></a>
                    <?php $this->renderPartial('live.views.layouts._sideLinks'); ?>
                    <?php $this->renderPartial('video.views.layouts._sideLinks'); ?>
                    <?php $this->renderPartial('transcoder.views.layouts._sideLinks'); ?>
                    <?php $this->renderPartial('streamer.views.layouts._sideLinks'); ?>
                    <?php $this->renderPartial('statistics.views.layouts._sideLinks'); ?>
                <?php $this->renderPartial('subscriber.views.layouts._sideLinks'); ?>
                <?php $this->renderPartial('settings.views.layouts._sideLinks'); ?>
                </ul>
    </nav>
</aside>


<html lang="en">
    <?php echo $this->renderPartial('application.views.layouts._head') ?>
    <body class="fixed-header menu-on-top smart-style-3">
        <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
        <?php echo $this->renderPartial('application.views.layouts._header') ?>
        
        <?php $this->renderPartial('application.views.widgets._alert') ?>        
        <?php echo $content; ?>
        	</div>
			<!-- /.container -->

		</div>
	</div>

      <div class="page-footer">
            <div class="row" style="margin-left: -13px;margin-right: -13px">
                <div class="col-xs-12 col-sm-6">
                    
                </div>
                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                    <script>
                $('.delete').click(function() {
                var urls = $(this).attr("href");
                        bootbox.confirm("Are you sure you want to permanently delete?", function(confirmed) {
                        if (confirmed === true) {
                        $.ajax({
                        type: "GET",
                                url: urls,
                                data: {'confirmdelete': true},
                                success: function(data) {
                                location.reload();
                                },
                                error: function() {
                               // $('#paymentdetails').html('Sorry,the record is being used elseWhere');
                                },
                        });
                        }
                        else{
                          location.reload();  
                        }
                        });
                });



            </script>
                    <?php if ($last_activity = UserActivity::model()->getLastAccountActivity()): ?>
                        <div class="txt-color-white inline-block">
                            <i class="txt-color-blueLight hidden-mobile"><?php echo Lang::t('Last account activity') ?> <i class="fa fa-clock-o"></i> <strong><time class="timeago" datetime="<?php echo $last_activity ?>" title="<?php echo Common::formatDate($last_activity, 'M j, Y g:i a') ?>"><?php echo $last_activity ?></time></strong> </i>
                        </div>
                    <?php endif; ?>
                    <!-- end div-->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
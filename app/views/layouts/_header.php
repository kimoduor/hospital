<?php if (Yii::app()->user->isGuest) { ?>
    <header class="header navbar navbar-fixed-top" role="banner"> </header>
    <div style="margin-top:60px;"></div>
    <?php
} else {
    ?>


    <!-- Header -->
    <header class="header navbar navbar-fixed-top" role="banner">
        <!-- Top Navigation Bar -->
        <div class="container">

            <!-- Only visible on smartphones, menu toggle -->
            <ul class="nav navbar-nav">
                <li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
            </ul>

            <!-- Logo -->
            <a class="navbar-brand" href="<?php echo Yii::app()->createUrl('default/index') ?>">
              <!--  <img  src="<?php echo Yii::app()->theme->baseUrl ?>/assets/img/keroche-logo-back.png" alt="logo" /> -->

                <span style="font-family: Brush Script MT; font-size: 24">Hatua HMIS</span>
                <span style="font-family:fantasy MT; font-size: 10"> Swixteq Product</span>
            </a>
            <!-- /logo -->


            <!-- Sidebar Toggler -->
            <!--			<a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
                                            <i class="icon-reorder"></i>
                                    </a>-->
            <!-- /Sidebar Toggler -->

            <!-- Top Left Menu -->
            <ul class="nav navbar-nav navbar-left hidden-xs hidden-sm">
                <li>
                    <a href="<?php echo Yii::app()->createUrl('/hospital/hospitals/') ?>">
                        <i class="icon-h-sign"></i>  Hospital
                    </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('/supplychain/supplychain/') ?>">
                        <i class="icon-hospital"></i>  Supply Chain
                    </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('/finance/finance/') ?>">
                        <i class="icon-eur"></i>  Finance
                    </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('/reports/report/') ?>">
                        <i class="icon-copy"></i>  Reports
                    </a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->createUrl('/configuration/configuration/') ?>">
                        <i class="icon-cog"></i>  Configurations
                    </a>
                </li>





            </ul>
            <!-- /Top Left Menu -->

            <!-- Top Right Menu -->
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-warning-sign"></i>
                        <span class="badge">5</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 5 new notifications</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">1 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-danger"><i class="icon-warning-sign"></i></span>
                                <span class="message">High CPU load on cluster #2.</span>
                                <span class="time">5 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-success"><i class="icon-plus"></i></span>
                                <span class="message">New user registration.</span>
                                <span class="time">10 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-info"><i class="icon-bullhorn"></i></span>
                                <span class="message">New items are in queue.</span>
                                <span class="time">25 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="label label-warning"><i class="icon-bolt"></i></span>
                                <span class="message">Disk space to 85% full.</span>
                                <span class="time">55 mins</span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all notifications</a>
                        </li>
                    </ul>
                </li>

                <!-- Tasks -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-tasks"></i>
                        <span class="badge">7</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 7 pending tasks</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Preparing new release</span>
                                    <span class="percent">30%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 30%;" class="progress-bar progress-bar-info"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Change management</span>
                                    <span class="percent">80%</span>
                                </span>
                                <div class="progress progress-small progress-striped active">
                                    <div style="width: 80%;" class="progress-bar progress-bar-danger"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Mobile development</span>
                                    <span class="percent">60%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 60%;" class="progress-bar progress-bar-success"></div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="task">
                                    <span class="desc">Database migration</span>
                                    <span class="percent">20%</span>
                                </span>
                                <div class="progress progress-small">
                                    <div style="width: 20%;" class="progress-bar progress-bar-warning"></div>
                                </div>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all tasks</a>
                        </li>
                    </ul>
                </li>

                <!-- Messages -->
                <li class="dropdown hidden-xs hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-envelope"></i>
                        <span class="badge">1</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <li class="title">
                            <p>You have 3 new messages</p>
                        </li>
                        <li>
                            <a href="javascript:void(0);">

                                <span class="photo"><img src="<?php echo Yii::app()->theme->baseUrl ?>/assets/img/demo/avatar-1.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Bob Carter</span>
                                    <span class="time">Just Now</span>
                                </span>
                                <span class="text">
                                    more...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="<?php echo Yii::app()->theme->baseUrl ?>/assets/img/demo/avatar-2.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Johne</span>
                                    <span class="time">45 mins</span>
                                </span>
                                <span class="text">
                                    Auss...
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <span class="photo"><img src="<?php echo Yii::app()->theme->baseUrl ?>/assets/img/demo/avatar-3.jpg" alt="" /></span>
                                <span class="subject">
                                    <span class="from">Patrick Nilson</span>
                                    <span class="time">6 hours</span>
                                </span>
                                <span class="text">
                                    No sea takimata sanctus...
                                </span>
                            </a>
                        </li>
                        <li class="footer">
                            <a href="javascript:void(0);">View all messages</a>
                        </li>
                    </ul>
                </li>

                <!-- .row .row-bg Toggler -->
                <li>
                    <a href="#" class="dropdown-toggle row-bg-toggle">
                        <i class="icon-resize-vertical"></i>
                    </a>
                </li>

                <!-- Project Switcher Button -->
                <li class="dropdown">
                    <a href="#" class="project-switcher-btn dropdown-toggle">
                        <i class="icon-cogs"></i>
                        <span>Settings</span>
                    </a>
                </li>

                <!-- User Login Dropdown -->
                <li class="dropdown user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php echo CHtml::image(UserImages::model()->getProfileImageUrl(Yii::app()->user->id, 32, 32), Lang::t('Me'), array('class' => 'online', 'style' => 'width:34px;height:30px;')); ?>
                        <span class="username"><?php echo Yii::app()->user->name; ?></span>
                        <i class="icon-caret-down small"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo Yii::app()->createUrl('/users/default/view?id=' . Yii::app()->user->id) ?>"><i class="icon-user"></i> My Profile</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('/users/default/change-password') ?>"><i class="icon-archive"></i>Change Password</a></li>

                        <li class="divider"></li>

                        <li><a href="<?php echo Yii::app()->createUrl('/auth/default/logout') ?>"><i class="icon-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- /user login dropdown -->
            </ul>
            <!-- /Top Right Menu -->
        </div>
        <!-- /top navigation bar -->

        <!--=== Project Switcher ===-->
        <div id="project-switcher" class="container project-switcher">
            <div id="scrollbar">
                <div class="handle"></div>
            </div>

            <div id="frame">
                <ul class="project-list">
                    <li>
                        <a href="javascript:void(0);">
                            <span class="image"><i class="icon-cogs"></i></span>
                            <span class="title">Settings</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->createUrl('/users/default') ?>">
                            <span class="image"><i class="icon-user"></i></span>
                            <span class="title">User Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo Yii::app()->createUrl('/users/roles/index') ?>">
                            <span class="image"><i class="icon-male"></i></span>
                            <span class="title">User Roles</span>
                        </a>
                    </li>

                </ul>
            </div> <!-- /#frame -->
        </div> <!-- /#project-switcher -->
        <!-- /#project-switcher -->
    </header> <!-- /.header -->

    <div id="container">
        <div id="sidebar" class="sidebar-fixed">
            <div id="sidebar-content">

                <!-- Search Input -->
                <form class="sidebar-search">
                    <div class="input-box">
                        <button type="submit" class="submit">
                            <i class="icon-search"></i>
                        </button>
                        <span>
                            <input type="text" placeholder="Search...">
                        </span>
                    </div>
                </form>

                <!-- Search Results -->
                <div class="sidebar-search-results">

                    <i class="icon-remove close"></i>
                    <!-- Documents -->
                    <div class="title">
                        Documents
                    </div>
                    <ul class="notifications">
                        <li>
                            <a href="javascript:void(0);">
                                <div class="col-left">
                                    <span class="label label-info"><i class="icon-file-text"></i></span>
                                </div>
                                <div class="col-right with-margin">
                                    <span class="message"><strong>John Doe</strong> received $1.527,32</span>
                                    <span class="time">finances.xls</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="col-left">
                                    <span class="label label-success"><i class="icon-file-text"></i></span>
                                </div>
                                <div class="col-right with-margin">
                                    <span class="message">My name is <strong>John Doe</strong> ...</span>
                                    <span class="time">briefing.docx</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                    <!-- /Documents -->
                    <!-- Persons -->
                    <div class="title">
                        Persons
                    </div>
                    <ul class="notifications">
                        <li>
                            <a href="javascript:void(0);">
                                <div class="col-left">
                                    <span class="label label-danger"><i class="icon-female"></i></span>
                                </div>
                                <div class="col-right with-margin">
                                    <span class="message">Jane <strong>Doe</strong></span>
                                    <span class="time">21 years old</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div> <!-- /.sidebar-search-results -->

                <!--=== Navigation ===-->
                <?php
                switch ($this->menu) {
                    case 'hospital':
                        $this->renderPartial('hospital.views.layouts._sideLinks');
                        break;
                    case 'supplychain':
                        $this->renderPartial('supplychain.views.layouts._sideLinks');
                        break;
                    case 'finance':
                        $this->renderPartial('finance.views.layouts._sideLinks');
                        break;
                    case 'reports':
                        $this->renderPartial('reports.views.layouts._sideLinks');
                        break;
                    case 'configuration':
                        $this->renderPartial('configuration.views.layouts._sideLinks');
                        break;
                }
                ?>

                <!-- /Navigation -->
                <div class="sidebar-title">
                    <span>Notifications</span>
                </div>
                <ul class="notifications demo-slide-in"> <!-- .demo-slide-in is just for demonstration purposes. You can remove this. -->
                    <li style="display: none;"> <!-- style-attr is here only for fading in this notification after a specific time. Remove this. -->
                        <div class="col-left">
                            <span class="label label-danger"><i class="icon-warning-sign"></i></span>
                        </div>
                        <div class="col-right with-margin">
                            <span class="message">Server <strong>#512</strong> crashed.</span>
                            <span class="time">few seconds ago</span>
                        </div>
                    </li>
                    <li style="display: none;"> <!-- style-attr is here only for fading in this notification after a specific time. Remove this. -->
                        <div class="col-left">
                            <span class="label label-info"><i class="icon-envelope"></i></span>
                        </div>
                        <div class="col-right with-margin">
                            <span class="message"><strong>John</strong> sent you a message</span>
                            <span class="time">few second ago</span>
                        </div>
                    </li>
                    <li>
                        <div class="col-left">
                            <span class="label label-success"><i class="icon-plus"></i></span>
                        </div>
                        <div class="col-right with-margin">
                            <span class="message"><strong>Emma</strong>'s account was created</span>
                            <span class="time">4 hours ago</span>
                        </div>
                    </li>
                </ul>

                <div class="sidebar-widget align-center">
                    <div class="btn-group" data-toggle="buttons" id="theme-switcher">
                        <label class="btn active">
                            <input type="radio" name="theme-switcher" data-theme="bright"><i class="icon-sun"></i> Bright
                        </label>
                        <label class="btn">
                            <input type="radio" name="theme-switcher" data-theme="dark"><i class="icon-moon"></i> Dark
                        </label>
                    </div>
                </div>

            </div>
            <div id="divider" class="resizeable"></div>
        </div>
        <!-- /Sidebar -->

        <div id="content" style="<?php echo Yii::app()->user->isGuest ? '' : 'margin-left: 250px;'; ?>">
            <div class="container">
                <!-- Breadcrumbs line -->
                <div class="crumbs">
                    <ul id="breadcrumbs" class="breadcrumb">    
                        <li>
                            <i class="icon-home"></i>
                            <a href="#"><?php echo $this->pageTitle; ?></a>
                        </li>
                        <?php
                        foreach ($this->breadcrumbs as $array) {
                            echo '<li>' . $array . '</li>';
                        }
                        ?>

                    </ul>

                    <ul class="crumb-buttons">
                        <li><a href="<?php echo Yii::app()->createUrl('/statistics/livewatching/index') ?>" title=""><i class="icon-signal"></i><span>Statistics</span></a></li>
                        <?php
                        $users = Users::model()->count();
                        ?>
                        <li class="dropdown"><a href="#" title="" data-toggle="dropdown"><i class="icon-tasks"></i><span>Users <strong>(<?php echo $users; ?>)</strong></span><i class="icon-angle-down left-padding"></i></a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="<?php echo Yii::app()->createUrl('/users/default') ?>" title=""><i class="icon-plus"></i>Add new User</a></li>

                            </ul>
                        </li>
                        <li class="range"><a href="#">
                                <i class="icon-calendar"></i>
                                <span></span>
                                <i class="icon-angle-down"></i>
                            </a></li>
                    </ul>
                </div>

            <?php } ?>
            <script>
                $(window).load(function() {
                    if (document.getElementById('my-form') !== null) {
                        var elem = document.getElementById('my-form').elements;
                        for (var i = 0; i < elem.length; i++)
                        {
                            var currentelem = elem[i].id;
                            $("#" + currentelem).hasClass("has-error") ? $("#" + currentelem).parent().parent().addClass("has-error") : "";
                        }
                    }
                });
            </script>

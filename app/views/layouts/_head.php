<head>
    <meta charset="utf-8">
    <title><?php echo CHtml::encode($this->pageTitle) ?> | Swixteq</title>
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Basic Styles -->
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl ?>/img/favicon.ico" type="image/x-icon">
   
    <script>
        if (!window.jQuery) {
            document.write('<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/libs/jquery-1.10.2.min.js"><\/script>');
        }
    </script>

    <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/libs/jquery-1.10.2.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
   
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/libs/lodash.compat.min.js"></script>
    <?php if (!YII_DEBUG): ?>
    <?php endif; ?>
    <script>
        if (!window.jQuery.ui) {
            document.write('<script src="<?php echo Yii::app()->theme->baseUrl ?>/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
        }
        $(document).ready(function() {
            "use strict";

            App.init(); // Init layout and core plugins
            Plugins.init(); // Init all plugins
            FormComponents.init(); // Init all form-specific plugins
        });
    </script>
    

    <script  src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/libs/breakpoints.js"></script>
    <script  src="<?php echo Yii::app()->theme->baseUrl ?>/plugins/respond/respond.min.js"></script> <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
    <script  src="<?php echo Yii::app()->theme->baseUrl ?>/plugins/cookie/jquery.cookie.min.js"></script>
    <script  src="<?php echo Yii::app()->theme->baseUrl ?>/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script  src="<?php echo Yii::app()->theme->baseUrl ?>/plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>
 
    <script  src="<?php echo Yii::app()->theme->baseUrl ?>/plugins/sparkline/jquery.sparkline.min.js"></script>

    <script src="<?php echo Yii::app()->theme->baseUrl ?>/plugins/pickadate/picker.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/plugins/pickadate/picker.date.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/plugins/pickadate/picker.time.js"></script>

    <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/app.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/plugins.js"></script>
    <script  src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/plugins.form-components.js"></script>

    
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/custom.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/demo/pages_calendar.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/demo/charts/chart_filled_blue.js"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/demo/charts/chart_simple.js"></script>
    <!-- Bootbox -->
	<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl ?>/plugins/bootbox/bootbox.js"></script>
    
    
    <script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/custom.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl ?>/assets/js/my-utils.js"></script>
    
</head>
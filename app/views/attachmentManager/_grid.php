<?php

$model_class_name = get_class($model);
$route = $this->route;
$grid_id = 'attachment-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Lang::t('Attachments'),
    'titleIcon' => '<i class="fa fa-paperclip"></i>',
    'showExportButton' => false,
    'showSearch' => true,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true, 'url' => Yii::app()->createUrl("attachmentManager/create", array("type" => $model_class_name, "parent_id" => $model->{$model->parent_id_field}, AttachmentManager::AFTERSAVE_GO_TO_ROUTE_GET_PARAM => $route)), 'label' => Lang::t(Constants::LABEL_CREATE . ' ' . 'Attachment')),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            array(
                'name' => 'title',
            ),
            array(
                'name' => 'description',
            ),
            array(
                'name' => 'file',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{download}{update}{trash}',
                'htmlOptions' => array('style' => 'width: 120px;'),
                'buttons' => array(
                    'download' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-download fa-2x"></i>',
                        'url' => 'Yii::app()->createUrl("attachmentManager/download", array("type" => "' . $model_class_name . '", "id" => $data->id, "' . AttachmentManager::AFTERSAVE_GO_TO_ROUTE_GET_PARAM . '" => "' . $route . '"))',
                        'options' => array(
                            'title' => Lang::t('Download'),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                       'label' => '<i class="icon-pencil"></i>',
                        'url' => 'Yii::app()->createUrl("attachmentManager/update", array("type" => "' . $model_class_name . '", "id" => $data->id, "' . AttachmentManager::AFTERSAVE_GO_TO_ROUTE_GET_PARAM . '" => "' . $route . '"))',
                        'visible' => '$this->grid->owner->showLink("' . $this->resource . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'trash' => array(
                        'url' => 'Yii::app()->createUrl("attachmentManager/delete", array("type" => "' . $model_class_name . '", "id" => $data->id, "' . AttachmentManager::AFTERSAVE_GO_TO_ROUTE_GET_PARAM . '" => "' . $route . '"))',
                        'imageUrl' => false,
                        'label' => '<i class="fa icon-trash text-danger"></i>',
                        'visible' => '$this->grid->owner->showLink("' . $this->resource . '", "' . Acl::ACTION_DELETE . '")?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
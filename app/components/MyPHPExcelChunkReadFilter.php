<?php

/**
 * Description of PHPExcelHelper
 *
 * @author Joakim <kimoduor@gmail.com>
 */
class MyPHPExcelChunkReadFilter implements PHPExcel_Reader_IReadFilter
{

    private $_startRow = 0;
    private $_endRow = 0;
    private $_columns = array();

    /**
     *
     * @param type $columns
     */
    public function __construct($columns)
    {
        $this->_columns = $columns;
    }

    /**
     * Set the list of rows that we want to read
     * @param type $startRow
     * @param type $chunkSize
     */
    public function setRows($startRow, $chunkSize)
    {
        $this->_startRow = $startRow;
        $this->_endRow = $startRow + $chunkSize;
    }

    /**
     *
     * @param type $column
     * @param type $row
     * @param type $worksheetName
     * @return boolean
     */
    public function readCell($column, $row, $worksheetName = '')
    {
        //  Only read the heading row, and the rows that are configured in $this->_startRow and $this->_endRow
        if (($row >= $this->_startRow && $row < $this->_endRow)) {
            if (in_array($column, $this->_columns)) {
                return true;
            }
        }
        return false;
    }

}

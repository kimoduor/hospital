<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/main';



    //const NUMITEMS ='numitems';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    /**
     * Stores commonly used system settings
     * @uses {@link CmsSettings}
     * @var array
     */
    public $settings = array();

    /**
     *
     * @var type
     */
    public $activeTab;

    /**
     *
     * @var type
     */
    public $activeMenu;

    /**
     *
     * @var type
     */
    public $package = array();

    /**
     *
     * @var type
     */
    public $package_name = 'smart_admin';

    /**
     * The module's published assets base url
     * @var type
     */
    public $package_base_url;

    /**
     * The base title for a controller
     * @var type
     */
    public $resourceLabel;

    /**
     * Stores the user's privileges
     * @var type
     */
    public $privileges;

    /**
     * Stores the user's Access privileges
     * @var type
     */
    public $farmer_privileges;

    /**
     * Stores the user's Agro Dealer privileges
     * @var type
     */
    public $agro_privileges;

    /**
     * Stores the user's Voucher privileges
     * @var type
     */
    public $voucher_privileges;

    /**
     * Stores the user's Invoice privileges
     * @var type
     */
    public $invoice_privileges;

    /**
     * Stores the user's resource privileges
     * @var type
     */
    public $resource_privileges;

    /**
     * Stores the user's Supplier privileges
     * @var type
     */
    public $supplier_privileges;



    /**
     *
     * @var type
     */
    public $resource;

    /**
     *
     * @var type
     */
    public $module_assets_url;

    /**
     *
     * @var type
     */
    public $module_package;

    /**
     * Stores the user's SMS  privileges
     * @var type
     */
    public $sms_privileges;

    public function init() {
        if (YII_DEBUG) {
            //Yii::app()->cache->flush();
        }

        if (!Yii::app()->request->isAjaxRequest) {
            $this->registerPackage();
            $this->registerModulePackage();
        }
        $this->setSettings();

        //form error css
        CHtml::$errorCss = 'has-error';
        CHtml::$errorSummaryCss = 'alert alert-danger fade in';

      //  if (!Yii::app()->user->isGuest) {
           // Users::setSessionVariables();
        //}
       // $this->setPrivileges();
       // $this->setFarmerPrivileges();



        parent::init();
    }

    protected function registerPackage() {
        $this->setPackage();

        $clientScript = Yii::app()->getClientScript();
        $this->package_base_url = $clientScript
                ->addPackage($this->package_name, $this->package)
                ->registerPackage($this->package_name)
                ->getPackageBaseUrl($this->package_name);
    }

    public function getPackageBaseUrl() {
        return $this->package_base_url;
    }

    public function setPackage() {
        //register commonly used assets.
        $this->package = array(
            'baseUrl' => Yii::app()->theme->baseUrl,
            'js' => array(                

                'plugins/touchpunch/jquery.ui.touch-punch.min.js',
                'plugins/event.swipe/jquery.event.move.js',
                'plugins/event.swipe/jquery.event.swipe.js',
                
                 
                 'plugins/flot/jquery.flot.min.js',
                 'plugins/flot/jquery.flot.tooltip.min.js',
                 'plugins/flot/jquery.flot.resize.min.js',
                'plugins/flot/jquery.flot.time.min.js',
                'plugins/flot/jquery.flot.growraf.min.js',
                'plugins/easy-pie-chart/jquery.easy-pie-chart.min.js',

                'plugins/fullcalendar/fullcalendar.min.js',
                'plugins/noty/jquery.noty.js',
                'plugins/noty/layouts/top.js',
                'plugins/noty/themes/default.js',
                'plugins/uniform/jquery.uniform.min.js',
                'plugins/select2/select2.min.js',                
                'assets/js/jquery-ui-1.10.1.min.js',

   
                
              //  'js/timepicker.js',
            ),
            'css' => array(
                'bootstrap/css/bootstrap.min.css',
                'assets/css/main.css',
                'assets/css/plugins.css',
                'assets/css/responsive.css',
                'assets/css/icons.css',
                'assets/css/fontawesome/font-awesome.min.css',
                'assets/css/jquery-ui-1.10.1.css',
                'assets/css/santiago.datepicker.css'
                
            )
        );
    }
    	

    /**
     * Module package should be overriden by the module controller
     */
    public function setModulePackage() {
        $this->module_package = NULL;
    }

    /**
     * Register module specific packages if not NULL
     */
    protected function registerModulePackage() {
        if ($this->publishModuleAssets()) {
            $this->setModulePackage();

            if (!empty($this->module_package)) {
                $package_name = $this->getModuleName() . '_module';
                Yii::app()->getClientScript()
                        ->addPackage($package_name, $this->module_package)
                        ->registerPackage($package_name);
            }
        }
    }

    /**
     * Publish module assets. This only happens if there is "assets" directory in the module root.
     */
    protected function publishModuleAssets() {
        $module_name = $this->getModuleName();
        if (!$module_name)
            return FALSE;
        $path = Yii::getPathOfAlias($module_name . '.assets');
        if (!is_dir($path))
            return FALSE;

        $this->module_assets_url = Yii::app()->assetManager->publish($path, false, -1, YII_DEBUG ? true : null);
        return TRUE;
    }

    /**
     * Returns the url of the published module assets.
     * @return type
     */
    public function getModuleAssetsUrl() {
        return $this->module_assets_url;
    }

    protected function setSettings() {
        if (YII_DEBUG)
            Yii::app()->settings->deleteCache(SettingsModuleConstants::SETTINGS_GENERAL);

        //get some commonly used settings
        $this->settings = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GENERAL, array(
            SettingsModuleConstants::SETTINGS_COMPANY_NAME,
            SettingsModuleConstants::SETTINGS_APP_NAME,
            SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE,
            SettingsModuleConstants::SETTINGS_THEME,
        ));
    }

    /**
     * Returns the module of the current controller
     * @return mixed module name or false if the controller does not belong to a module
     */
    public function getModuleName() {
        $module = $this->getModule();
        if ($module !== null)
            return $module->getName();
        return FALSE;
    }

    



    /**
     * Whether to show link or not
     * @param string $resource
     * @param string $action default to "view"
     * @return boolean  True or False
     */
    public function showLink($resource = null, $action = null) {
        if ($resource === null)
            $resource = $this->resource;

        if ($action === NULL)
            $action = Acl::ACTION_VIEW;
        return Acl::hasPrivilege($this->privileges, $resource, $action, FALSE);
    }



    /**
     * Should be called before any action the require ACL
     * @param string $action
     */
    public function hasPrivilege($action = NULL) {
        if (NULL === $action)
            $action = Acl::ACTION_VIEW;
        Acl::hasPrivilege($this->privileges, $this->resource, $action);
    }

    public function checkPrivilege($cont = null, $privileges = null, $action = NULL) {

        $this->controlLayer($cont, $privileges, $action);
    }

    private function controlLayer($val, $privileges, $action) {

        if (NULL === $action)
            $action = Acl::ACTION_VIEW;
        switch ($val) {

    

            default:
                return Acl::hasPrivilege($privileges, $this->resource, $action);
                break;
        }
    }

}

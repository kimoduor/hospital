<?php

/**
 * Extension of the @link CFormModel
 * @author JoAKIM <kimoduor@gmail.com>
 */
abstract class FormModel extends CFormModel
{

    /**
     * Class name of the form model
     * @return string Class name
     */
    public function getClassName()
    {
        return get_class($this);
    }

    /**
     * Get model errors to JSON
     * @return type
     */
    public function errorsToJson()
    {
        $errors = array();
        foreach ($this->getErrors() as $attribute => $error) {
            $errors[CHtml::activeId($this, $attribute)] = $error;
        }
        return json_encode($errors);
    }

}

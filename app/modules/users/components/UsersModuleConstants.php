<?php

/**
 * Defines all constants used within the module
 *
 * @author Joakim <kimoduor@gmail.com>
 */
class UsersModuleConstants {
    //resources constants

    const RES_USER_ROLES = 'USER_ROLES';
    const RES_USERS = 'USERS';
    const RES_USER_RESOURCES = 'USER_RESOURCES';
    const RES_USER_LEVELS = 'USER_LEVELS';
    const RES_USER_ACTIVITY = 'USER_ACTIVITY';
    const USER_PRIVILEGES = 'Manage Privileges';
    const RES_USER_PRIVILEGES = 'USER_PRIVILEGES';
    const RES_USER_AGRO_PRIVILEGES = 'USER_AGRO_PRIVILEGES';
    const RES_VOUCHER_PRIVILEGES = 'VOUCHER_PRIVILEGES';
    const RES_INVOICE_PRIVILEGES = 'INVOICE_PRIVILEGES';
    const RES_SUPPLIER_PRIVILEGES = 'SUPPLIER_PRIVILEGES';
    const RES_INPUT_PRIVILEGES = 'INPUT_PRIVILEGES';
    const RES_REPORTS_PRIVILEGES = 'REPORTS_PRIVILEGES';
    const RES_SMS_PRIVILEGES = 'SMS_PRIVILEGES';
    //menu and tabs constants
    const MENU_USERS = 'MENU_USERS';
    const TAB_USERS = 'TAB_USERS';
    const TAB_ROLES = 'TAB_ROLES';
    const TAB_USER_LEVELS = 'TAB_USER_LEVELS';
    const TAB_RESOURCES = 'TAB_RESOURCES';
    const TAB_PRIVILEGES = 'TAB_PRIVILEGES';
    const TAB_AGRO_PRIVILEGES = 'AGRO_PRIVILEGES';
    const TAB_VOUCHER_PRIVILEGES = 'VOUCHER_PRIVILEGES';
    const TAB_INVOICE_PRIVILEGES = 'INVOICE_PRIVILEGES';
    const TAB_INPUT_PRIVILEGES = 'INPUT_PRIVILEGES';
    const TAB_SUPPLIER_PRIVILEGES = 'SUPPLIER_PRIVILEGES';
    const TAB_REPORTS_PRIVILEGES = 'REPORTS_PRIVILEGES';
    const TAB_SMS = 'SMS_PRIVILEGES';
    //miscelleneous
    const LOG_ACTIVITY = 'activity_log';

}

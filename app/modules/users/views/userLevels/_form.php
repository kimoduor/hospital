<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-modal-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array(
        'class' => 'form-horizontal',
    )
        ));
?>
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title"><?php echo CHtml::encode($this->pageTitle); ?></h4>
</div>
<div class="modal-body">
        <div class="alert hidden" id="my-modal-notif"></div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'rank', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->textField($model, 'rank', array('class' => 'form-control')); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'id', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->textField($model, 'id', array('class' => 'form-control', 'maxlength' => 30)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'description', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->textField($model, 'description', array('class' => 'form-control', 'maxlength' => 60)); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'banned_resources', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->dropDownList($model, 'banned_resources', UserResources::model()->getListData('id', 'id', false), array('multiple' => 'multiple', 'class' => 'form-control chosen-select')); ?>
                </div>
        </div>
        <div class="form-group">
                <?php echo $form->labelEx($model, 'banned_resources_inheritance', array('class' => 'col-md-3 control-label')); ?>
                <div class="col-md-6">
                        <?php echo $form->dropDownList($model, 'banned_resources_inheritance', UserLevels::model()->getListData('id', 'id', true), array('class' => 'form-control')); ?>
                </div>
        </div>
</div>
<div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></button>
        <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
</div>
<?php $this->endWidget(); ?>
<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/js/plugin/chosen/chosen.min.css')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugin/chosen/chosen.jquery.min.js', CClientScript::POS_END)
        ->registerScript('users.userLevels._form', "$('.chosen-select').chosen();");
?>
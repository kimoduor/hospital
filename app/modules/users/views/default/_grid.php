<?php

$grid_id = 'users-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => '<i class="fa fa-group"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => true, 'modal' => false),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
   // 'enablePagination' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'filter' => $model,
       // 'rowCssClassExpression' => '$data->status==="' . Users::STATUS_BLOCKED . '"?"bg-danger":""',
        'rowHtmlOptionsExpression' => 'array("class"=>"linkable","data-href"=>Yii::app()->controller->createUrl("view",array("id"=>$data->id)))',
        'columns' => array(
            array(
                'name' => 'username',
                'filter' => false,
            ),
            array(
                'name' => 'name',
                'filter' => false,
            ),
            array(
                'name' => 'role_id',
                'type' => 'raw',
                'value' => 'UserRoles::model()->get($data->role_id,"name")',
                'filter' => Users::userRolesOptions(),
            ),
            array(
                'name' => 'email',
                'filter' => false,
            ),
            array(
                'name' => 'phone',
                'filter' => false,
            ),
            array(
                'name' => 'date_created',
                'value' => 'MyYiiUtils::formatDate($data->date_created)',
                'filter' => false,
            ),
//            array(
//                'name' => 'status',
//                'type' => 'raw',
//             //   'value' => 'CHtml::tag("span", array("class"=>$data->status==="' . Users::STATUS_ACTIVE . '"?"label label-success":"label label-danger"), $data->status)',
//             //   'filter' => Users::statusOptions(),
//            ),
            array(
                'class' => 'ButtonColumn',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'template' => '{update}',
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="icon-pencil"></i>',
                       'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',//check this line it is important 
                        'visible' => 'true',
                        'options' => array(
                            //'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),

                )
            ),
        ),
    )
));
?>
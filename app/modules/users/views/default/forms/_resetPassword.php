<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'reset-password',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal'),
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-lock"></i> <?php echo CHtml::encode($this->pageTitle) ?></h3>
    </div>
    <div class="panel-body">
        <?php echo CHtml::errorSummary($model, '') ?>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'password', array('class' => 'col-md-2 control-label')); ?>
            <div class="col-md-4">
                <?php echo $form->passwordField($model, 'password', array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'confirm', array('class' => 'col-md-2 control-label')); ?>
            <div class="col-md-4">
                <?php echo $form->passwordField($model, 'confirm', array('class' => 'form-control')); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-6">
                <label class="checkbox">
                    <?php echo $form->checkBox($model, 'send_email', array()); ?> <span> <?php echo Lang::t('Email the new login details to the user.') ?></span>
                </label>
            </div>
        </div>
    </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
            <a class="btn btn-default btn-sm" href="<?php echo UrlManager::getReturnUrl($this->createUrl('view', array('id' => $model->id))) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Cancel') ?></a>
            <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t('Reset Password') ?></button>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
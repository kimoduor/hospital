<?php
$form_id = 'users-form';
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'my-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('class' => 'form-horizontal'),
        ));
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-edit"></i> <?php echo CHtml::encode($this->pageTitle) ?></h3>
    </div>
    <div class="panel-body">
        <fieldset>
            <legend><?php echo Lang::t('Account details') ?></legend>
            <?php echo CHtml::errorSummary($model, '') ?>
              <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'role_id', array('class' => 'col-md-2 control-label')); ?>
                    <div class="col-md-6">
                        <?php echo CHtml::activeDropDownList($model, 'role_id', UserRoles::model()->getListData('id', 'name'), array('class' => 'select2 col-md-6')); ?>
                    </div> 
                </div>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'user_level', array('class' => 'col-md-2 control-label')); ?>
                    <div class="col-md-6">
                        <?php echo CHtml::activeDropDownList($model, 'user_level', UserLevels::model()->getListData('id', 'description'), array('class' => 'select2 col-md-6')); ?>

                        <?php //echo CHtml::activeHiddenField($model, 'user_level', array('value' =>  UserLevels::LEVEL_ADMIN)); ?>

                    </div> 
                </div>
           
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                    <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control', 'maxlength' => 128)); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'email', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                    <?php echo CHtml::activeEmailField($model, 'email', array('class' => 'form-control', 'maxlength' => 128)); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'phone', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                    <?php echo CHtml::activeTextField($model, 'phone', array('class' => 'form-control', 'maxlength' => 20)); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo CHtml::activeLabelEx($model, 'username', array('class' => 'col-md-2 control-label')); ?>
                <div class="col-md-4">
                    <?php echo CHtml::activeTextField($model, 'username', array('class' => 'form-control', 'maxlength' => 30)); ?>
                </div>
            </div>
            <?php if ($model->isNewRecord): ?>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'password', array('class' => 'col-md-2 control-label')); ?>
                    <div class="col-md-4">
                        <?php echo CHtml::activePasswordField($model, 'password', array('class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <?php echo CHtml::activeLabelEx($model, 'confirm', array('class' => 'col-md-2 control-label')); ?>
                    <div class="col-md-4">
                        <?php echo CHtml::activePasswordField($model, 'confirm', array('class' => 'form-control')); ?>
                    </div>
                </div>
            <?php endif ?>

            <?php if ($model->isNewRecord): ?>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-4">
                        <label class="checkbox"><?php echo CHtml::activeCheckBox($model, 'send_email'); ?> <?php echo Lang::t('Email the login details to the user.') ?></label>
                    </div>
                </div>
            <?php endif; ?>
        </fieldset>
        <fieldset>
            <legend><?php echo Lang::t('Profile Image') ?></legend>
            <?php $this->renderPartial('users.views.default.forms._image_field', array('model' => $model, 'htmlOptions' => array('label_class' => 'col-md-2 control-label', 'field_class' => 'col-md-4'))) ?>
        </fieldset>
    </div>
    <div class="panel-footer clearfix">
        <div class="pull-right">
            <a class="btn btn-default btn-sm" href="<?php echo UrlManager::getReturnUrl($model->isNewRecord ? $this->createUrl('index') : $this->createUrl('view', array('id' => $model->id))) ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Cancel') ?></a>
            <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save Changes') ?></button>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>

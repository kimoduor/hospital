<?php

/**
 * This is the model class for table "user_roles_on_resources".
 *
 * The followings are the available columns in table 'user_roles_on_resources':
 * @property integer $id
 * @property integer $role_id
 * @property string $resource_id
 * @property integer $view
 * @property integer $create
 * @property integer $update
 * @property integer $delete
 * @property integer $disapprove
 * @property integer $nrc_editable
 *
 * The followings are the available model relations:
 * @property UserRoles $role
 * @property UserResources $resource
 */
class UserRolesOnPrivileges extends ActiveRecord {
public $disapprove;
//public $nrc_editable;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserRolesOnResources the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_roles_on_privileges';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('role_id, privilege_id', 'required'),
            array('role_id, view, create, update, delete, approve, disapprove, export, search, a_search,nrc_editable', 'numerical', 'integerOnly' => true),
            array('privilege_id', 'length', 'max' => 128),
            array('id', 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
      //  TbBadge::model();
        return array(
            'role' => array(self::BELONGS_TO, 'UserRoles', 'role_id'),
            'privilege' => array(self::BELONGS_TO, 'UserPrivileges', 'privilege_id'),
        );
    }
   

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'resource_id' => 'Resource',
            'view' => 'View',
            'create' => 'Create',
            'update' => 'Update',
            'delete' => 'Delete',
            'approve' => 'Approve',
            'disapprove' => 'Disapprove',
            'export' => 'Export',
            'search' => 'Normal Search',
            'a_search' => 'Advanced Search',
            'nrc_editable'=>'NRC Editable'
        );
    }

    /**
     *
     * @param type $resource_id
     * @param type $role_id
     * @param type $action
     * @param type $default
     */
    public function getValue($privilege_id, $role_id, $action, $default = NULL) {
        $model = $this->find('`role_id`=:role_id AND `privilege_id`=:privilege_id', array(':role_id' => $role_id, ':privilege_id' => $privilege_id));
        if ($model !== NULL)
            return $model->$action;
        if ($default !== NULL)
            return $default;
        return FALSE;
    }

    /**
     *
     * @param type $resource_id
     * @param type $role_id
     * @param type $values
     */
    public function set($privilege_id, $role_id, $values) {
        $model = $this->find('`role_id`=:role_id AND `privilege_id`=:privilege_id', array(':role_id' => $role_id, ':privilege_id' => $privilege_id));
        if ($model === NULL) {
            $model = new UserRolesOnPrivileges();
            $model->privilege_id = $privilege_id;
            $model->role_id = $role_id;
        }

        foreach ($values as $key => $val) {
            $model->$key = (int) $val;
        }
        if ($model->save())
            return TRUE;
        return FALSE;
    }

    /**
     * 
     * @param type $column
     * @return boolean
     */
    public static function getFarmerPrevilege($column) {
        $bool = null;
        $role_id = Users::model()->getScalar('role_id', '`id`=:t1', array(':t1' => $user_id = Yii::app()->user->id));
        $bool = UserRolesOnPrivileges::model()->getScalar("$column", '`role_id`=:role_id AND `privilege_id`=:previlege', array(':role_id' => $role_id, ':previlege' => 'FARMER_INFO'));
        if ($bool == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function getUserPrevilege($column,$previlegeconstant) {
        $bool = null;
        $role_id = Users::model()->getScalar('role_id', '`id`=:t1', array(':t1' => $user_id = Yii::app()->user->id));
        $bool = UserRolesOnPrivileges::model()->getScalar("$column", '`role_id`=:role_id AND `privilege_id`=:previlege', array(':role_id' => $role_id, ':previlege' => $previlegeconstant));
        if ($bool == 1) {
            return true;
        } else {
            return false;
        }
    }

}

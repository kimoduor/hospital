<?php

/**
 * This is the model class for table "user_farmer_privileges".
 *
 * The followings are the available columns in table 'user_resources':
 * @property string $id
 * @property string $description
 * @property integer $viewable
 * @property integer $createable
 * @property integer $updateable
 * @property integer $approveable
 * @property integer $disapproveable
 * @property integer $deleteable
 * @property integer $nrc_editable
 *
 * The followings are the available model relations:
 * @property UserRolesOnResources[] $usersRolesOnResources
 */
class UserPrivileges extends ActiveRecord {

        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return UserResources the static model class
         */
        public static function model($className = __CLASS__) {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName() {
                return 'user_farmer_privileges';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules() {

                return array(
                    array('id, description', 'required'),
                    array('viewable,deleteable, updateable, approveable,disapproveable,  exportable, createable, normal_search, advanced_search,nrc_editable', 'numerical', 'integerOnly' => true),
                    array('id', 'length', 'max' => 128),
                    array('id', 'unique'),
                    array('description', 'length', 'max' => 255),
                    array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
                );
        }

        /**
         * @return array relational rules.
         */
        public function relations() {
                return array(
                    //'usersRolesOnResources' => array(self::HAS_MANY, 'UserRolesOnResources', 'resource_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels() {
                return array(
                    'id' => Lang::t('ID'),
                    'description' => Lang::t('Description'),
                    'viewable' => Lang::t('Viewable'),
                    'createable' => Lang::t('Createable'),
                    'updateable' => Lang::t('Updateable'),
                    'approveable' => Lang::t('Approveable'),
                    'disapproveable' => Lang::t('Disapproveable'),
                    'exportable' => Lang::t('Exportable'),
                    'deleteable' => Lang::t('Deleteable'),
                    'normal_search' => Lang::t('Searcheable'),
                    'advanced_search' => Lang::t('Advanced Searcheable'),
                );
        }

        public function searchParams() {
                return array(
                    array('description', self::SEARCH_FIELD, true, 'OR'),
                    array('id', self::SEARCH_FIELD, true, 'OR'),
                    'id',
                );
        }

        /**
         * Get resources
         * @param type $exluded_resources. Resources not to be included
         * @return type
         */
        public function getPrivileges() {
                $command = Yii::app()->db->createCommand()
                        ->select()
                        ->from($this->tableName());
                
                return $command->queryAll();
        }

}

<?php

/**
 * The user/staff controller
 * @author Joakim <kimoduor@gmail.com>
 */
class DefaultController extends UsersModuleController {

    /**
     * Executed before every action
     */
    public function init() {
        $this->resource = UsersModuleConstants::RES_USERS;
        $this->resourceLabel = 'User';
        $this->activeMenu = UsersModuleConstants::MENU_USERS;
        $this->activeTab = UsersModuleConstants::TAB_USERS;

        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl',
            'postOnly + delete,changeStatus,deleteLog',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'create', 'update', 'delete', 'changeStatus', 'resetPassword', 'changePassword', 'deleteLog', 'activityLog'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id = NULL) {
        if (NULL === $id)
            $id = Yii::app()->user->id;
        $model = Users::model()->loadModel($id);
        $this->pageTitle = $model->name;

        $this->render('view', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($user_level = NULL) {
        //$this->hasPrivilege(Acl::ACTION_CREATE);
        $this->pageTitle = 'Users';

        $model = new Users(ActiveRecord::SCENARIO_CREATE);
        //   $model->status = Users::STATUS_ACTIVE;
        $model->user_level = $user_level;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(array('view', 'id' => $model->id));
            }
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id) {
        $model = Users::model()->loadModel($id);
        $this->pageTitle = $model->name;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            //  $userlevel= $model->user_level;      
            $model->attributes = $_POST[$model_class_name];
            //  $model->userlevel=$userlevel;
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->refresh();
            }
        }


        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionResetPassword($id) {
        $model = Users::model()->loadModel($id);
        if (!Users::isMyAccount($id)) {
            $model->checkPrivilege($this, Acl::ACTION_UPDATE, true);
        }
        $this->pageTitle = Lang::t('Reset password');

        $model->setScenario(Users::SCENARIO_RESET_PASSWORD);
        $model->password = NULL;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            $user_level = Users::model()->get($id, 'user_level');
        


            if ($model->save()) {

                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->redirect(UrlManager::getReturnUrl($this->createUrl('view', array('id' => $id))));
            }
        }

        $this->render('resetPassword', array(
            'model' => $model,
        ));
    }

    public function actionChangePassword() {
        $id = Yii::app()->user->id;
        $username = Yii::app()->user->username;
        $this->pageTitle = Lang::t('Change your password');

        $model = Users::model()->loadModel($id);
        $model->setScenario(Users::SCENARIO_CHANGE_PASSWORD);
        $model->pass = $model->password;
        $model->password = null;
        $model_class_name = $model->getClassName();
        if (isset($_POST[$model_class_name])) {
            $username = Yii::app()->user->username;
            $model->attributes = $_POST[$model_class_name];
            $currentPass = $model->currentPassword;
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->refresh();
            }

            $model->currentPassword = $currentPass;
        }

        $this->render('changePassword', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $model = Users::model()->loadModel($id);
        $model->checkPrivilege($this, Acl::ACTION_DELETE, true);
        $model->delete();
        echo json_encode(array('redirectUrl' => $this->createUrl('index')));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {


        //$this->hasPrivilege(Acl::ACTION_VIEW);
        $this->pageTitle = Lang::t(Common::pluralize($this->resourceLabel));

        $searchModel = Users::model()->searchModel(array(), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'username', Users::model()->getFetchCondition());
        $this->render('index', array('model' => $searchModel));
    }

    public function actionChangeStatus($id, $status) {
        $model = Users::model()->loadModel($id);

        $model->checkPrivilege($this, Acl::ACTION_UPDATE, true);
        $valid_status = Users::statusOptions();
        if (!in_array($status, $valid_status)) {
            throw new CHttpException(400, Lang::t('400_error'));
        }
        $model->status = $status;
        $model->save(FALSE);

        Yii::app()->user->setFlash('success', Lang::t('Success'));

        echo CJSON::encode(array());
    }

    public function actionActivityLog($id) {
        $this->resource = UsersModuleConstants::RES_USER_ACTIVITY;
        $model = Users::model()->loadModel($id);
        $this->hasPrivilege(Acl::ACTION_VIEW);

        $this->pageTitle = Lang::t('Audit Trail');

        $this->renderPartial('log/_activity', array(
            'model' => UserActivityLog::model()->searchModel(array('user_id' => $model->id), $this->settings[SettingsModuleConstants::SETTINGS_ITEMS_PER_PAGE], 'date_created asc'),
                ), FALSE, TRUE);
    }

    public function actionDeleteLog() {
        $this->resource = UsersModuleConstants::RES_USER_ACTIVITY;
        $this->hasPrivilege(Acl::ACTION_DELETE);
        if ($ids = filter_input(INPUT_POST, 'ids')) {
            UserActivityLog::model()->deleteMany($ids);
        }
    }

}

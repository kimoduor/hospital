<?php

class EthnicityController extends SettingsModuleController {

    public function init() {
       $this->activeTab = 'ethnicity';
        $this->resourceLabel = 'ethnicity';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index','create','update','delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Ethnicity();
        $model_class_name = get_class($model);
          if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        $error_message = CActiveForm::validate($model);
                        $error_message_decoded = CJSON::decode($error_message);
                        
                if (!empty($error_message_decoded)) {
                    $errorlist = '';
                    foreach ($error_message_decoded as $error) {
                        $errorlist.=$error[0] . '<br />';
                    }
                    Yii::app()->user->setFlash('error', $errorlist);
                } else {
                    $model->save(FALSE);
                    Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                    $this->redirect(array('index'));
                    Yii::app()->end();
                }
                }
       
        //Yii::app()->end();
        $this->render('_form', array('model' => $model));
    }

    public function actionUpdate($id) {
        $model = Ethnicity::model()->loadModel($id);
        $model_class_name = get_class($model);
          if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        $error_message = CActiveForm::validate($model);
                        $error_message_decoded = CJSON::decode($error_message);
                        
                         if (!empty($error_message_decoded)) {
                    $errorlist = '';
                    foreach ($error_message_decoded as $error) {
                        $errorlist.=$error[0] . '<br />';
                    }
                    Yii::app()->user->setFlash('error', $errorlist);
                } else {
                    $model->save(FALSE);
                    Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                    $this->redirect(array('index'));
                    Yii::app()->end();
                }
                }

        $this->render('_form', array('model' => $model));
    }

 

    public function actionIndex() {
        $condition = '';
        if (Yii::app()->request->getParam('Ethnicity')) {
            $vn = Yii::app()->request->getParam('Ethnicity');
            $search = $vn['_search'];
            $condition = " ethnic_name LIKE '%$search%' ";
        }

        $this->render('index', 
                array('model' => Ethnicity::model()->searchModel(array(), 10, 'ethnic_id', $condition)));
    }

     public function actionDelete($id) {
         if (isset($_GET['confirmdelete'])) {
            try {
                Ethnicity::model()->loadModel($id)->delete();
                 Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {                
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));

            }
        }
    }

}

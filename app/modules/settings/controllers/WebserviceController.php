<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class WebserviceController extends SettingsModuleController {

    public function init() {
        parent::init();
        $this->resourceLabel = Lang::t('Settings');
        $this->resource = SettingsModuleConstants::RES_SETTINGS;
//  $this->activeTab = SettingsModuleConstants::TAB_GENERAL;
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index'),
                'users' => array('*'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {

        if (isset($_GET['pdts'])) {
            $responsearray = array();
            $pdts = $_GET['pdts'];
            $allproducts = Products::model()->findAll(array('condition' => "product_id NOT IN ($pdts)"));
            $i = 0;
            foreach ($allproducts as $product) {
                $array = array();
                $array["product_id"] = $product->product_id;
                $array["name"] = $product->name;
                $array["category"] = Productcategories::model()->get($product->product_category_id, 'category');
                $array["unit_cost"] = $product->unit_cost;
                $array["measure_unit"] = Measureunits::model()->get($product->measure_unit_id, 'unit');
                $responsearray[] = $array;
            }
            echo json_encode($responsearray);
        } elseif (isset($_GET['regions'])) {
            $responsearray = array();
            $regions = $_GET['regions'];
            $allregions = Regions::model()->findAll(array('condition' => "region_id NOT IN ($regions)"));
            $i = 0;

            foreach ($allregions as $region) {
                $array = array();

                $array["region_id"] = $region->region_id;
                $array["region"] = $region->region;

                $responsearray[] = $array;
            }
            //  var_dump($responsearray);
            echo json_encode($responsearray);
        } elseif (isset($_GET['towns'])) {
            $responsearray = array();
            $towns = $_GET['towns'];
            $alltowns = Towns::model()->findAll(array('condition' => "town_id NOT IN ($towns)"));
            $i = 0;

            foreach ($alltowns as $town) {
                $array = array();

                $array["town_id"] = $town->town_id;
                $array["town"] = $town->town;
                $array["region"] = Regions::model()->get(Details::getRegion_idfromtown($town->town_id), 'region');
                $responsearray[] = $array;
            }
            //  var_dump($responsearray);
            echo json_encode($responsearray);
        } elseif (isset($_GET['locations'])) {
            $responsearray = array();
            $locations = $_GET['locations'];
            $alllocations = Locations::model()->findAll(array('condition' => "location_id NOT IN ($locations)"));
            $i = 0;

            foreach ($alllocations as $location) {
                $array = array();

                $array["location_id"] = $location->location_id;
                $array["location"] = $location->location;
                $array["town"] = Towns::model()->get(Details::getTown_id($location->location_id), 'town');
                $array["region"] = Regions::model()->get(Details::getRegion_id($location->location_id), 'region');
                $responsearray[] = $array;
            }
            //  var_dump($responsearray);
            echo json_encode($responsearray);
        } elseif (isset($_GET['insertsubdistributorstoserver']) && !empty($_GET['insertsubdistributorstoserver'])) {
            $responsearray = array();
            if (isset($_GET['mac_address'])) {
                $mac_address = $_GET['mac_address'];
                $datas = array();
                
                
                if (explode("|", $_GET['insertsubdistributorstoserver'])) {
                    $datas = explode("|", $_GET['insertsubdistributorstoserver']);
                } else {
                    $datas[] = $_GET['insertsubdistributorstoserver'];
                }

                $response = array();
                foreach ($datas as $data) {
                    $aaray = array();
                    $eachrecord = explode(",", $data);

                    $model = new Subdistributors();
                    $model->distributor_id = Distributors::model()->getScalar("distributor_id", "phone_mac_address='$mac_address'");
                    if (!$model->distributor_id) {
                        $aaray['error'] = "Sorry, it seems Your mobile device is not registered";
                        $aaray['lock'] = "regsub_" . $eachrecord[0];
                    } else {
                        $thenames = explode("_", $eachrecord[1]);
                        $model->fname = $thenames[0];
                        $model->lname = $thenames[1];
                        //$model->region_id = Distributors::model()->getScalar("region_id", "phone_mac_address='$mac_address'");
                        $model->email = $eachrecord[2];
                        $model->mobile = $eachrecord[3];
                        $model->location_id = $eachrecord[4];
                        $model->business_name = $eachrecord[5];
                        $model->owner_name = $eachrecord[6];
                        $model->owner_mobile = $eachrecord[7];
                        $querystr = "SELECT id FROM users Order By id  DESC LIMIT 1";
                        $keyarray = Yii::app()->db->createCommand($querystr)->queryAll();
                        $key = $keyarray == NULL ? 1 : ($keyarray[0]['id']) + 1;
                        $model->subdistributor_code = 'S00' . $key;
                        $model->created_by = Distributors::model()->getScalar("user_id", "phone_mac_address='$mac_address'");
// echo  $model->created_by;
//save in the users first
                        $usermodel = new Users();
                        $usermodel->name = $model->fname . " " . $model->lname;
                        $usermodel->username = $model->mobile;
                        $usermodel->email = $model->email;
                        $usermodel->phone = $model->mobile;
                        $usermodel->password = md5($model->mobile) . md5(date('Y-d-m H:i:s'));
                        $usermodel->salt = md5(date('Y-d-m H:i:s'));
                        $usermodel->user_level = 5;
                        $usermodel->role_id = 5;
// echo  $model->created_by;
                        $error_message2 = CActiveForm::validate($usermodel);
                        $error_message_decoded2 = CJSON::decode($error_message2);

                        if (!empty($error_message_decoded2)) {
                            foreach ($error_message_decoded2 as $singleerror) {
// echo json_encode(array(array("error"=>$singleerror[0].$model->name)));
                                $aaray['error'] = $singleerror[0];
                                $aaray['lock'] = "regsubi_" . $eachrecord[0];
                            }
                        } else {
                            $error_message = CActiveForm::validate($model);
                            $error_message_decoded = CJSON::decode($error_message);
                    
                            $usermodel->save();
                            $model->user_id = $usermodel->id;

                            $model->save();
                            $aaray['server_subdistributor_id'] = $model->subdistributor_id;
                            $aaray['subdistributor_code'] = $model->subdistributor_code;
                            $aaray['subdistributor_id'] = $eachrecord[0];
                        }
                    }
                    $responsearray[] = $aaray;
                }
                echo json_encode($responsearray);
            }
        } elseif (isset($_GET['subdistributorsfromserver']) && isset($_GET['mac_address'])) {
            $mac_address = $_GET['mac_address'];
            $responsearray = array();
            $distributor_id = Distributors::model()->getScalar("distributor_id", "phone_mac_address='$mac_address'");

            if (!$distributor_id) {
                $aaray = array();
                $aaray['error'] = "Sorry, it seems Your mobile device is not registered";
                $aaray['lock'] = "regsubij_" . $eachrecord[0];
                $responsearray[] = $aaray;
            } else {
                $subdistributors = $_GET['subdistributorsfromserver'];
// echo "running twos $distributor_id";
                $allsubdistributors = Subdistributors::model()->findAll(array('condition' => "subdistributor_id NOT IN ($subdistributors) AND distributor_id=$distributor_id"));
                $i = 0;
                foreach ($allsubdistributors as $subdistributor) {
                    $array = array();
                    $array["subdistributor_id"] = $subdistributor->subdistributor_id;
                    $array["fname"] = $subdistributor->fname;
                    $array["lname"] = $subdistributor->lname;
                    $array["email"] = $subdistributor->email;
                    $array["mobile"] = $subdistributor->mobile;
                    $array["subdistributor_code"] = $subdistributor->subdistributor_code;
                    $array["businessname"] = $subdistributor->business_name;
                    $array["ownername"] = $subdistributor->owner_name;
                    $array["ownermobile"] = $subdistributor->owner_mobile;
                    $array["location_id"] = $subdistributor->location_id;
                    $responsearray[] = $array;
                }
            }
            echo json_encode($responsearray);
        }
//----------------------start resellers------------------------------------------
        elseif (isset($_GET['insertresellerstoserver']) && !empty($_GET['insertresellerstoserver'])) {
            $responsearray = array();
            if (isset($_GET['mac_address'])) {
                $mac_address = $_GET['mac_address'];
                $datas = array();
                if (explode("|", $_GET['insertresellerstoserver'])) {
                    $datas = explode("|", $_GET['insertresellerstoserver']);
                } else {
                    $datas[] = $_GET['insertresellerstoserver'];
                }

                $response = array();
                foreach ($datas as $data) {
                    $aaray = array();
                    $eachrecord = explode(",", $data);

                    $model = new Resellers();
                    $model->subdistributor_id = Subdistributors::model()->getScalar("subdistributor_id", "phone_mac_address='$mac_address'");
                    if (!$model->subdistributor_id) {


                        $aaray['error'] = "Sorry, it seems Your mobile device is not registered as a Subdistributor";
                        $aaray['lock'] = "regre_" . $eachrecord[0];
                        $responsearray[] = $aaray;
                    } else {
// $aaray = array();

                        $thenames = explode("_", $eachrecord[1]);
                        $model->fname = $thenames[0];
                        $model->lname = $thenames[1];
                        //$model->region_id = Subdistributors::model()->getScalar("region_id", "phone_mac_address='$mac_address'");
                        $model->email = $eachrecord[2];
                        $model->mobile = $eachrecord[3];
                        $model->location_id = $eachrecord[4];
                        $model->business_name = $eachrecord[5];
                        $model->owner_name = $eachrecord[6];
                        $model->owner_mobile = $eachrecord[7];
                        $querystr = "SELECT reseller_id FROM tblresellers Order By reseller_id  DESC LIMIT 1";
                        $keyarray = Yii::app()->db->createCommand($querystr)->queryAll();
                        $key = $keyarray == NULL ? 1 : ($keyarray[0]['reseller_id']) + 1;
                        $model->reseller_code = 'R00' . $key;
                        $model->created_by = Subdistributors::model()->getScalar("user_id", "phone_mac_address='$mac_address'");
                        $model->save();
                        $aaray['server_reseller_id'] = $model->reseller_id;
                        $aaray['reseller_code'] = $model->reseller_code;
                        $aaray['reseller_id'] = $eachrecord[0];
                    }

                    $responsearray[] = $aaray;
                }
                echo json_encode($responsearray);
            }
        } elseif (isset($_GET['newresellersfromserver']) && isset($_GET['mac_address'])) {
            $mac_address = $_GET['mac_address'];
            $responsearray = array();
            $subdistributor_id = Subdistributors::model()->getScalar("subdistributor_id", "phone_mac_address='$mac_address'");

            if (!$subdistributor_id) {

                $aaray = array();
                $aaray['error'] = "Sorry, it seems Your mobile device is not registered";
                $aaray['lock'] = "regsubsx_" . $eachrecord[0];
                $responsearray[] = $aaray;
            } else {

                $resellers = $_GET['newresellersfromserver'];
// echo "running twos $distributor_id";
                $allresellers = Resellers::model()->findAll(array('condition' => "reseller_id NOT IN ($resellers) AND subdistributor_id=$subdistributor_id"));
                $i = 0;
//  echo "I am running".$subdistributor_id;
                foreach ($allresellers as $reseller) {
                    $array = array();
                    $array["reseller_id"] = $reseller->reseller_id;
                    $array["fname"] = $reseller->fname;
                    $array["lname"] = $reseller->lname;
                    $array["email"] = $reseller->email;
                    $array["mobile"] = $reseller->mobile;
                    $array["reseller_code"] = $reseller->reseller_code;
                    $array["businessname"] = $reseller->business_name;
                    $array["ownername"] = $reseller->owner_name;
                    $array["ownermobile"] = $reseller->owner_mobile;
                    $array["location_id"] = $reseller->location_id;
                    $responsearray[] = $array;
                }
            }
            echo json_encode($responsearray);
        }
//----------------------finish resellers-----------------------------------------
        elseif (isset($_GET['distributiontoserver']) && !empty($_GET['distributiontoserver']) && !empty($_GET['mac_address'])) {
            $responsearray = array();
            $mac_address = $_GET['mac_address'];
            $datas = array();
            $paymentdatas = array();
            if (explode("|", $_GET['distributiontoserver'])) {
                $datas = explode("|", $_GET['distributiontoserver']);
            } else {
                $datas[] = $_GET['distributiontoserver'];
            }

            $response = array();

            foreach ($datas as $data) {
                $aaray = array();
                $eachrecord = explode(",", $data);

                $model = new Distribution();
                $model->distributor_id = Distributors::model()->getScalar("distributor_id", "phone_mac_address='$mac_address'");
                if (!$model->distributor_id) {
                    $aaray['error'] = "Sorry, it seems Your mobile device is not registered";
                    $aaray['lock'] = "regsub_" . $eachrecord[0];
                } else {

                    //    mystr =mystr+distribution_id+","+payment_id+","+server_product_id+","+quantity+","+gross_sales_value+","
                    //+discount+","+net_sales_value+","+server_subdistributor_id+","+cash+","+cheque+","+credit;   
                    $paymentmodel = new Payments();
                    $paymentmodel->cash = $eachrecord[8];
                    $paymentmodel->cheque = $eachrecord[9];
                    $paymentmodel->credit = $eachrecord[10];
                    $paymentmodel->save();
                    $model->payment_id = $paymentmodel->payment_id;
                    $model->product_id = $eachrecord[2];
                    $model->quantity = $eachrecord[3];
                    $model->gross_sales_value = $eachrecord[4];
                    $model->discount = $eachrecord[5];
                    $model->net_sales_value = $eachrecord[6];
                    $model->subdistributor_id = $eachrecord[7];
                    $model->created_by = Distributors::model()->getScalar("user_id", "phone_mac_address='$mac_address'");
                    $model->sync = 1;
                    $error_message = CActiveForm::validate($model);
                    $error_message_decoded = CJSON::decode($error_message);
                    if (!empty($error_message_decoded)) {
                        foreach ($error_message_decoded as $singleerror) {
                            $aaray['error'] = $singleerror[0];
                            $aaray['lock'] = "regsubi_" . $eachrecord[0];
                        }
                    } else {
                        $model->save();
                        $aaray['server_distribution_id'] = $model->distribution_id;
                        $aaray['distribution_id'] = $eachrecord[0];
                        $aaray['server_payment_id'] = $paymentmodel->payment_id;
                        $aaray['payment_id'] = $eachrecord[1];
                    }
                }
                $responsearray[] = $aaray;
            }
            echo json_encode($responsearray);
        }
        //----------------------finish distribution to sever----------------------------------------
        elseif (isset($_GET['subdistributiontoserver']) && !empty($_GET['subdistributiontoserver']) && !empty($_GET['mac_address'])) {
            $responsearray = array();

            $mac_address = $_GET['mac_address'];
            $datas = array();
            $paymentdatas = array();
            if (explode("|", $_GET['subdistributiontoserver'])) {
                $datas = explode("|", $_GET['subdistributiontoserver']);
            } else {
                $datas[] = $_GET['subdistributiontoserver'];
            }

            $response = array();

            foreach ($datas as $data) {
                $aaray = array();
                $eachrecord = explode(",", $data);

                $model = new Subdistribution();
                $model->subdistributor_id = Subdistributors::model()->getScalar("subdistributor_id", "phone_mac_address='$mac_address'");
                if (!$model->subdistributor_id) {
                    $aaray['error'] = "Sorry, it seems Your mobile device is not registered";
                    $aaray['lock'] = "regsub_" . $eachrecord[0];
                } else {

                    //    mystr =mystr+distribution_id+","+payment_id+","+server_product_id+","+quantity+","+gross_sales_value+","
                    //+discount+","+net_sales_value+","+server_subdistributor_id+","+cash+","+cheque+","+credit;   
                    $paymentmodel = new Subpayments();
                    $paymentmodel->cash = $eachrecord[8];
                    $paymentmodel->cheque = $eachrecord[9];
                    $paymentmodel->credit = $eachrecord[10];
                    $paymentmodel->save();
                    $model->subpayment_id = $paymentmodel->subpayment_id;
                    $model->product_id = $eachrecord[2];
                    $model->quantity = $eachrecord[3];
                    $model->gross_sales_value = $eachrecord[4];
                    $model->discount = $eachrecord[5];
                    $model->net_sales_value = $eachrecord[6];
                    $model->reseller_id = $eachrecord[7];
                    $model->created_by = Subdistributors::model()->getScalar("user_id", "phone_mac_address='$mac_address'");
                    $model->date_created = date('Y-m-d H:i:s');

                    $model->sync = 1;
                    $error_message = CActiveForm::validate($model);
                    $error_message_decoded = CJSON::decode($error_message);

                    if (!empty($error_message_decoded)) {
                        foreach ($error_message_decoded as $singleerror) {
                            $aaray['error'] = $singleerror[0];
                            $aaray['lock'] = "regsubi_" . $eachrecord[0];
                        }
                    } else {
                        // echo "ready to save";  
                        $model->save();
                        $aaray['server_subdistribution_id'] = $model->subdistribution_id;
                        $aaray['subdistribution_id'] = $eachrecord[0];
                        $aaray['server_subpayment_id'] = $paymentmodel->subpayment_id;
                        $aaray['subpayment_id'] = $eachrecord[1];
                    }
                }
                $responsearray[] = $aaray;
            }
            echo json_encode($responsearray);
        } elseif (isset($_GET['transponder']) && isset($_GET['mac_address'])) {
            $responsearray = array();
            $aaray = array();
            $details = $_GET['transponder'];
            $datas = explode("|", $details);
            $mac_address = $_GET['mac_address'];
            //echo $mac_address;
            //check if the mac iss in distributorsor subdistributors
            $user_id = Distributors::model()->getScalar("user_id", "phone_mac_address='$mac_address'");
            if (!$user_id) {
                $user_id = Subdistributors::model()->getScalar("user_id", "phone_mac_address='$mac_address'");
            }


            if (!user_id) {
                $aaray['failure'] = "Phone not Recorgnised";
                $responsearray[] = $aaray;
                echo json_encode($responsearray);
            } else {
                //find if the user is already saved
                $tracker_id = Trackers::model()->getScalar("tracker_id", "user_id=$user_id");

                if ($tracker_id) {//meaning it is an update
                    $model = Trackers::model()->loadModel($tracker_id);
                    $model->latitude = $datas[0];
                    $model->longitude = $datas[1];
                    $model->save();
                    $aaray['success'] = "Transpnder updated successfully";
                    $responsearray[] = $aaray;
                    echo json_encode($responsearray);
                } else {
                    $model = new Trackers();
                    $model->user_id = $user_id;
                    $model->latitude = $datas[0];
                    $model->longitude = $datas[1];
                    $model->save();
                    $aaray['success'] = "Transpnder updated successfully";
                    $responsearray[] = $aaray;
                    echo json_encode($responsearray);
                }
            }
        } else {
            $responsearray = array();
            $aaray = array();
            $aaray['everythingupdate'] = "Everything up-to-date";
            $responsearray[] = $aaray;
            echo json_encode($responsearray);
        }
    }

}

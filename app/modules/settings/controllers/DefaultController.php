<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class DefaultController extends SettingsModuleController
{

    public function init()
    {
        parent::init();
        $this->resourceLabel = Lang::t('Settings');
        $this->resource = SettingsModuleConstants::RES_SETTINGS;
        $this->activeTab = SettingsModuleConstants::TAB_GENERAL;
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'runtime', 'socialmedia', 'email', 'security', 'payment', 'Payroll', 'map'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex()
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t('General ' . $this->resourceLabel);

        $model = new GeneralSettings();
        $model_class_name = get_class($model);
        if (isset($_POST[$model_class_name])) {
            $this->hasPrivilege(Acl::ACTION_UPDATE);
            $model->attributes = $_POST[$model_class_name];
           $model->smsnumber=$_POST['GeneralSettings']['smsnumber'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                $this->refresh();
            }
        }

        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionRuntime()
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t('System runtime logs');
        $this->activeTab = SettingsModuleConstants::TAB_RUNTIME;

        $log_file = '';
        if (isset($_POST['log_file'])) {
            $log_file = $_POST['log_file'];
        }

        $base_path = Yii::getPathOfAlias('application.runtime') . DS;
        if (empty($log_file))
            $log_file = $base_path . 'application.log';

        if (isset($_POST['clear'])) {
            $this->hasPrivilege(Acl::ACTION_DELETE);
            if (file_exists($log_file)) {
                @unlink($log_file);
            }
        }

        $log_files = Common::getDirectoryFiles($base_path . 'application.log*');

        $this->render('runtime', array(
            'log_files' => $log_files,
            'log_file' => $log_file,
        ));
    }

    public function actionMap()
    {
        $this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->activeTab = SettingsModuleConstants::TAB_MAP;
        $this->pageTitle = Lang::t('Google map settings');

        if (isset($_POST['settings'])) {
            $this->hasPrivilege(Acl::ACTION_UPDATE);
            foreach ($_POST['settings'] as $key => $value) {
                Yii::app()->settings->set(SettingsModuleConstants::SETTINGS_GOOGLE_MAP, $key, $value);
            }

            Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
            $this->refresh();
        }
        $settings = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_GOOGLE_MAP, array(
            SettingsModuleConstants::SETTINGS_GOOGLE_MAP_API_KEY,
            SettingsModuleConstants::SETTINGS_GOOGLE_MAP_DEFAULT_CENTER,
            SettingsModuleConstants::SETTINGS_GOOGLE_MAP_DEFAULT_MAP_TYPE,
            SettingsModuleConstants::SETTINGS_GOOGLE_MAP_CROWD_MAP_ZOOM,
            SettingsModuleConstants::SETTINGS_GOOGLE_MAP_SINGLE_VIEW_ZOOM,
            SettingsModuleConstants::SETTINGS_GOOGLE_MAP_DIRECTION_MAP_ZOOM,
        ));

        $this->render('map', array(
            'settings' => $settings,
        ));
    }

}

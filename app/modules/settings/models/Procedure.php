<?php

/**
 * This is the model class for table "hp_procedure".
 *
 * The followings are the available columns in table 'hp_procedure':
 * @property string $procedure_id
 * @property string $procedure_name
 * @property string $description
 * @property string $revenue_department
 */
class Procedure extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hp_procedure';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('procedure_name', 'required'),
            array('procedure_name, revenue_department', 'length', 'max' => 100),
            array('description', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('procedure_id, procedure_name, description, revenue_department', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'procedure_id' => 'Procedure',
            'procedure_name' => 'Procedure Name',
            'description' => 'Description',
            'revenue_department' => 'Revenue Department',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
//	public function search()
//	{
//		// @todo Please modify the following code to remove attributes that should not be searched.
//
//		$criteria=new CDbCriteria;
//
//		$criteria->compare('procedure_id',$this->procedure_id,true);
//		$criteria->compare('procedure_name',$this->procedure_name,true);
//		$criteria->compare('description',$this->description,true);
//		$criteria->compare('revenue_department',$this->revenue_department,true);
//
//		return new CActiveDataProvider($this, array(
//			'criteria'=>$criteria,
//		));
//	}

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Procedure the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchParams() {
        return array(
                // 'id',
        );
    }

}

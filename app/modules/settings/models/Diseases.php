<?php

/**
 * This is the model class for table "hp_diseases".
 *
 * The followings are the available columns in table 'hp_diseases':
 * @property string $disease_id
 * @property string $icd10_code
 * @property string $name
 * @property string $description
 */
class Diseases extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hp_diseases';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('icd10_code, name', 'required'),
            array('icd10_code', 'length', 'max' => 20),
            array('name', 'length', 'max' => 100),
            array('description', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('disease_id, icd10_code, name, description', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'disease_id' => 'Disease',
            'icd10_code' => 'Icd10 Code',
            'name' => 'Name',
            'description' => 'Description',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Diseases the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchParams() {
        return array(
                // 'id',
        );
    }

}

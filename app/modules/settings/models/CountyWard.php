<?php

/**
 * This is the model class for table "hp_county_ward".
 *
 * The followings are the available columns in table 'hp_county_ward':
 * @property string $ward_id
 * @property string $ward_name
 * @property string $county_id
 *
 * The followings are the available model relations:
 * @property County $county
 */
class CountyWard extends ActiveRecord implements IMyActiveSearch {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'hp_county_ward';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ward_name', 'required'),
            array('ward_name', 'length', 'max' => 200),
            array('county_id', 'length', 'max' => 20),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('ward_id, ward_name, county_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'county' => array(self::BELONGS_TO, 'County', 'county_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'ward_id' => 'Ward',
            'ward_name' => 'Ward Name',
            'county_id' => 'County',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
   

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return CountyWard the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchParams() {
        return array(
                // 'id',
        );
    }

}

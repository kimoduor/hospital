<?php

/**
 * This is the model class for table "tblsubanks".
 *
 * The followings are the available columns in table 'tblsubanks':
 * @property string $bank_id
 * @property string $bank_name
 * @property string $bank_swift_code
 * @property string $date_created
 * @property string $created_by
 *
 */
class Banks extends ActiveRecord implements IMyActiveSearch
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tblsubanks';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('bank_name', 'required'),
            array('bank_name', 'length', 'max' => 255),
            array('bank_swift_code', 'length', 'max' => 10),
            array('bank_swift_code,bank_name', 'unique', 'message' => Lang::t('{value} already exists.')),
            array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'tblsuagrodealers' => array(self::HAS_MANY, 'Agrodealers', 'bank_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'bank_name' => Lang::t('Bank Name'),
            'bank_swift_code' => Lang::t('Swift Code'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Banks the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function searchParams()
    {
        return array(
            array('bank_name', self::SEARCH_FIELD, true, 'OR'),
            array('bank_swift_code', self::SEARCH_FIELD, true, 'OR'),
        );
    }

    /**
     *
     * @param type $valueField
     * @param type $textField
     * @param type $add_tip
     * @param type $conditions
     * @param type $params
     * @param type $order
     * @param type $groupField
     * @return type
     */
    public function getListData($valueField = 'bank_id', $textField = 'bank_name', $add_tip = true, $conditions = '', $params = array(), $order = null, $groupField = '')
    {
        return parent::getListData($valueField, $textField, $add_tip, $conditions, $params, $order, $groupField);
    }

}

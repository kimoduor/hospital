<?php

$grid_id = 'files-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'titleIcon' => null,
    'showExportButton' => false,
    'title'=>'Education Levels',
    'showSearch' => true,
  //  'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'createButton'=>array('visible'=> true,'modal'=>false),
    'toolbarButtons' => array(),
    'showRefreshButton' => false,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            'level_name',
            array(
                'class' => 'ButtonColumn',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'template' => '{update}{delete}',
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="icon-edit"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->level_id))',
                       // 'visible' => UserRolesOnResources::getRolesPrivilege('update', 'ELEARNING')?'true':'false',  
                        //'visible' => '$this->grid->owner->showLink("' . $this->resource . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'visible'=>'true',
                        'options' => array(
                           // 'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="icon-trash text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->level_id))',
                         'visible'=>'true',
                      //  'visible' => UserRolesOnResources::getRolesPrivilege('delete', 'ELEARNING')?'true':'false',  
                     //   'visible' => '$this->grid->owner->showLink("' . $this->resource . '", "' . Acl::ACTION_DELETE . '")&&$data->canDelete()?true:false',
                        //'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>



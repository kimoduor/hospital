<div class="widget box">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'my-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => 'form-horizontal',
            'role' => 'form',
        )
    ));
    ?>
  
        <div class="widget-header">
            <i class="icon-reorder"></i> <h4 class="modal-title">Relationship</h4>

        </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'relation_name', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'relation_name', array('class' => 'form-control', 'maxlength' => 25)); ?>
        </div>
    </div>

    <div class="modal-footer">
         <a href="<?php echo CController::createUrl('index'); ?>" class="btn btn-default"><i class="icon-remove-circle"></i> <?php echo Lang::t('Close') ?></a>
        <button class="btn btn-primary" type="submit"><i class="icon-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
       
    </div>
</div>
    <?php $this->endWidget(); ?>






<?php $can_view_system_settings = $this->showLink(SettingsModuleConstants::RES_SETTINGS); ?>
<div class="list-group">
    <?php if ($can_view_system_settings): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/default/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_GENERAL ? ' active' : '' ?>"><?php echo Lang::t('General Settings') ?></a>
    <?php endif; ?>

    <?php if ($this->showLink(SettingsModuleConstants::RES_MODULES_ENABLED)): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/modules/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_MODULES_ENABLED ? ' active' : '' ?>"><?php echo Lang::t('Manage Modules') ?></a>
    <?php endif; ?>

</div>

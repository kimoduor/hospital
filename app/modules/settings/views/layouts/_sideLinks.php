
<li class="active">
    <a href="#"><i class="fa fa-lg fa-fw fa-cogs"></i> <span class="menu-item-parent"><?php echo Lang::t('Setup') ?></span></a>
    <ul>
        <li>
            <a href="<?php echo Yii::app()->createUrl('/settings/regions/index') ?>"><?php echo Lang::t('Initial Setup') ?></a>
        </li>
        <li>
            <a href="<?php echo Yii::app()->createUrl('users/default/index') ?>"><?php echo Lang::t('User Management') ?></a>
        </li>

        <li>
            <a href="<?php echo Yii::app()->createUrl('/chat/index.php/site_admin/') ?>" target="_window"><?php echo Lang::t('Admin Chat') ?></a>
        </li>

    </ul>
</li>

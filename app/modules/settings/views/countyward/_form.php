<div class="widget box">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'my-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => 'form-horizontal',
            'role' => 'form',
        )
    ));
    ?>

    <div class="widget-header">
        <i class="icon-reorder"></i> <h4 class="modal-title">County Ward</h4>

    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'ward_name', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'ward_name', array('class' => 'form-control', 'maxlength' => 25)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'county_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'county_id', County::model()->getListData('county_id', 'county_Name'), array('class' => 'col-md-6 select2')); ?>
        </div>
    </div> 
    <div class="modal-footer">
        <button class="btn btn-primary" type="submit"><i class="icon-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="icon-remove-circle"></i> <?php echo Lang::t('Close') ?></button>
    </div>
</div>
<?php $this->endWidget(); ?>






<div class="list-group">
    <a href="<?php echo Yii::app()->createUrl('settings/regions/index') ?>"
       class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_REGIONS ? ' active' : '' ?>">
           <?php echo Lang::t('Regions') ?></a>
     <a href="<?php echo Yii::app()->createUrl('settings/towns/index') ?>"
       class="list-group-item<?php echo $this->activeTab === 'towns' ? ' active' : '' ?>">
           <?php echo Lang::t('Towns') ?></a>
         <a href="<?php echo Yii::app()->createUrl('settings/locations/index') ?>"
       class="list-group-item<?php echo $this->activeTab === 'locations' ? ' active' : '' ?>">
           <?php echo Lang::t('Locations') ?></a>
    
        <a href="<?php echo Yii::app()->createUrl('settings/measureunits/index') ?>"
       class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_MEASUREUNITS ? ' active' : '' ?>">
           <?php echo Lang::t('Measure Units') ?></a>
    
     <a href="<?php echo Yii::app()->createUrl('settings/productcategories/index') ?>"
       class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_PRODUCTCATEGORIES ? ' active' : '' ?>">
           <?php echo Lang::t('Product Categories') ?></a>
    
    <a href="<?php echo Yii::app()->createUrl('settings/transactiontypes/index') ?>"
       class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_TRANSACTIONTYPES ? ' active' : '' ?>">
           <?php echo Lang::t('Transaction Types') ?></a>
        <a href="<?php echo Yii::app()->createUrl('settings/products/index') ?>"
       class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_PRODUCTS ? ' active' : '' ?>">
           <?php echo Lang::t('Products') ?></a>

</div>

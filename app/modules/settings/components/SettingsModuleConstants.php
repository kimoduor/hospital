<?php

/**
 * Defines all constants used within the module
 *
 * @author Joakim <kimoduor@gmail.com>
 */
class SettingsModuleConstants {

    //resources constants

    const RES_SETTINGS = 'SETTINGS';
    const RES_MODULES_ENABLED = 'MODULES_ENABLED';
    const RES_JOB_MANAGER = 'JOB_MANAGER';
    const RES_ORG_STRUCTURE = 'ORG_STRUCTURE';
    const RES_GEOGRAPHICAL_BOUNDARY = 'GEOGRAPHICAL_BOUNDARY';
    const RES_USER_AGRO_PRIVILEGES = 'USER_AGRO_PRIVILEGES';
    //menu and tabs constants
    const TAB_MEASUREUNITS = 'TAB_MEASUREUNITS';
    const TAB_PRODUCTCATEGORIES = 'TAB_PRODUCTCATEGORIES';
    const TAB_TRANSACTIONTYPES = 'TAB_TRANSACTIONTYPES';
    const TAB_PRODUCTS = 'TAB_PRODUCTS';
    //general settings constants
    const SETTINGS_GENERAL = 'general';
    const SETTINGS_COMPANY_NAME = 'company_name';
    const SETTINGS_COMPANY_EMAIL = 'company_email';
    const SETTINGS_CURRENCY = 'currency_id';
    const SETTINGS_ITEMS_PER_PAGE = 'items_per_page';
    const SETTINGS_APP_NAME = 'app_name';
    const MENU = 'menu';
    const SETTINGS_DEFAULT_TIMEZONE = 'default_timezone';
    const SETTINGS_DEFAULT_LOCATION_ID = 'default_location_id';
    const SETTINGS_COUNTRY_ID = 'country_id';
    const SETTINGS_THEME = 'theme';
    const SETTINGS_ELEARNING = 'E-learning';
    const SETTINGS_ELEARNING_CATEGORY = 'E-learning-category';
    const SETTINGS_ELEARNING_CATEGORY_ROLES = 'E-learning-category-roles';
    //email settings
    const SETTINGS_EMAIL = 'email';
    const SETTINGS_EMAIL_MAILER = 'email_mailer';
    const SETTINGS_EMAIL_HOST = 'email_host';
    const SETTINGS_EMAIL_PORT = 'email_port';
    const SETTINGS_EMAIL_USERNAME = 'email_username';
    const SETTINGS_EMAIL_PASSWORD = 'email_password';
    const SETTINGS_EMAIL_SECURITY = 'email_security';
    const SETTINGS_EMAIL_MASTER_THEME = 'email_master_theme';
    const SETTINGS_EMAIL_SENDMAIL_COMMAND = 'email_sendmail_command';
    const SETTINGS_EMAIL_SENDING_METHOD = 'email_sending_method';
    const TAB_REGIONS = 'TAB_REGIONS';
    //themes
    const THEME1 = 'smart-style-0';
    const THEME2 = 'smart-style-1';
    const THEME3 = 'smart-style-2';
    const THEME4 = 'smart-style-3';
    //miscellaneous
    const LOCATION_ALIAS = 'Branch';

}

<div class="widget box">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'my-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => 'form-horizontal',
            'role' => 'form',
        )
    ));
    ?>

    <div class="widget-header">
        <i class="icon-reorder"></i> <h4 class="modal-title">Hospitals</h4>

    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'name', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'name', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'county_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'county_id', County::model()->getListData('county_id', 'county_Name'), array('class' => 'col-md-6 select2')); ?>
        </div>
    </div> 
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'country_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'country_id', Country::model()->getListData('country_id', 'country_name'), array('class' => 'col-md-6 select2')); ?>
        </div>
    </div> 

    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'address', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'address', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'location', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'location', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
       <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'telephone', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'telephone', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>

    <div class="modal-footer">
        <a href="<?php echo CController::createUrl('index'); ?>" class="btn btn-default"><i class="icon-times"></i> <?php echo Lang::t('Close') ?></a>
        <button class="btn btn-primary" type="submit"><i class="icon-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
    </div>

  
    <?php $this->endWidget(); ?>
</div>
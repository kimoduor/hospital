<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
$model_class_name = UserRolesOnResources::model()->getClassName();
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('users.views.layouts._tab') ?>
    </div>
    <div class="col-md-10">
        <div class="wells well-lights">
            <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route, $this->actionParams), 'POST', array('class' => '', 'id' => 'my-roles-view-form', 'role' => 'form')) ?>
            <div class="row">
                <div class="col-sm-8">
                    <h1 class="page-title txt-color-blueDark">
                        <?php echo CHtml::encode($this->pageTitle) ?>
                        <small><?php echo CHtml::encode($model->description) ?></small>
                    </h1>
                </div>
                <div class="col-sm-4 padding-top-10">
                    <div class="pull-right">
                        <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t('Save Changes') ?></button>
                        <a class="btn btn-danger" href="<?php echo $this->createUrl('index') ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></a>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-group"></i> <?php echo Lang::t('Add Users') ?></h3>
                </div>
                <div class="panel-body">
                    <?php echo CHtml::dropDownList('users', UserRoles::model()->getUsers($model->id), Users::model()->getListData('id', 'name', FALSE, '`user_level`=:t1', array(':t1' => UserLevels::LEVEL_ADMIN)), array('multiple' => 'multiple', 'class' => 'form-control chosen-select')); ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-edit"></i> <?php echo Lang::t('Review Setup Privileges for ') . CHtml::encode($this->pageTitle) .Lang::t('  Role') ?><span class="pull-right"><button class="btn btn-link my-select-all" type="button"><?php echo Lang::t('Check All') ?></button></span></h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr style="background-color:inherit;background-image: none"><th><?php echo Lang::t('System Resources') ?></th><th><?php echo Lang::t('Can View') ?></th><th><?php echo Lang::t('Can Create') ?></th><th><?php echo Lang::t('Can Update') ?></th><th><?php echo Lang::t('Can Delete') ?></th></tr>
                        </thead>
                        <tbody>
                            <?php foreach ($resources as $r): ?>
                                <tr>
                                    <td><?php echo $r['description'] ?></td>
                                    <td><?php if (UserResources::model()->get($r['id'], 'viewable') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $r['id'] . '][view]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $r['id'] . '][view]', UserRolesOnResources::model()->getValue($r['id'], $model->id, 'view'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (UserResources::model()->get($r['id'], 'createable') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $r['id'] . '][create]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $r['id'] . '][create]', UserRolesOnResources::model()->getValue($r['id'], $model->id, 'create'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (UserResources::model()->get($r['id'], 'updateable') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $r['id'] . '][update]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $r['id'] . '][update]', UserRolesOnResources::model()->getValue($r['id'], $model->id, 'update'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (UserResources::model()->get($r['id'], 'deleteable') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $r['id'] . '][delete]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $r['id'] . '][delete]', UserRolesOnResources::model()->getValue($r['id'], $model->id, 'delete'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>
<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/js/plugin/chosen/chosen.min.css')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugin/chosen/chosen.jquery.min.js', CClientScript::POS_END)
        ->registerScript('users.roles.view', "UsersModule.Roles.init();$('.chosen-select').chosen();");
?>
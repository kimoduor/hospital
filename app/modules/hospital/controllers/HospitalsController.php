<?php

class HospitalsController extends HospitalModuleController {

    public function init() {
       $this->activeTab = 'hospitals';
        $this->resourceLabel = 'hospitals';
        $this->menu="hospital";
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index','create','update','delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Hospitals();
        $model_class_name = get_class($model);
          if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        $error_message = CActiveForm::validate($model);
                        $error_message_decoded = CJSON::decode($error_message);
                        
                if (!empty($error_message_decoded)) {
                    $errorlist = '';
                    foreach ($error_message_decoded as $error) {
                        $errorlist.=$error[0] . '<br />';
                    }
                    Yii::app()->user->setFlash('error', $errorlist);
                } else {
                    $model->save(FALSE);
                    Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                    $this->redirect(array('index'));
                    Yii::app()->end();
                }
                }
       
        //Yii::app()->end();
        $this->render('_form', array('model' => $model));
    }

    public function actionUpdate($id) {
        $model = Hospitals::model()->loadModel($id);
        $model_class_name = get_class($model);
          if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        $error_message = CActiveForm::validate($model);
                        $error_message_decoded = CJSON::decode($error_message);
                        
                         if (!empty($error_message_decoded)) {
                    $errorlist = '';
                    foreach ($error_message_decoded as $error) {
                        $errorlist.=$error[0] . '<br />';
                    }
                    Yii::app()->user->setFlash('error', $errorlist);
                } else {
                    $model->save(FALSE);
                    Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                    $this->redirect(array('index'));
                    Yii::app()->end();
                }
                }

        $this->render('_form', array('model' => $model));
    }

 

    public function actionIndex($id = null) {
        $condition = '';
        
        if (Yii::app()->request->getParam('Hospitals')) {
            $vn = Yii::app()->request->getParam('Hospitals');
            $search = $vn['_search'];
            $condition = " country_id IN (SELECT country_id FROM hp_country WHERE country_name LIKE '%$search%') OR county_id IN (SELECT county_id FROM hp_county WHERE county_Name LIKE '%$search%')"
                    . " OR address LIKE '%$search%' OR location LIKE '%$search%' OR telephone LIKE '%$search%'";
        }

        $this->render('index', 
                array('model' => Hospitals::model()->searchModel(array(), 10, 'hospital_id', $condition)));
    }

     public function actionDelete($id) {
         if (isset($_GET['confirmdelete'])) {
            try {
                Hospitals::model()->loadModel($id)->delete();
                 Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {                
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));

            }
        }
    }

}

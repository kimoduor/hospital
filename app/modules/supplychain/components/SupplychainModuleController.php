<?php

/**
 * @author Joakim <kimoduor@gmail.com>
 * Parent controller for the settings module
 */
class SupplychainModuleController extends Controller
{

    public function init()
    {
        if (empty($this->activeMenu))
            $this->activeMenu = SupplychainModuleConstants::MENU_SUPPLYCHAIN;
        if (empty($this->resource))
            $this->resource = SupplychainModuleConstants::RES_SUPPLYCHAIN;
      //  Yii::import('application.modules.distributors.forms.*');
        parent::init();
    }

    public function setModulePackage()
    {

        $this->module_package = array(
            'baseUrl' => $this->module_assets_url,
            'js' => array(
                'js/module.js',
                        ),
            'css' => array(
            ),
        );
    }

}

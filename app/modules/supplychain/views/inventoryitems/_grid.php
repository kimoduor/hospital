<?php

$grid_id = 'user-roles-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => 'Inventory Items Category',
    'titleIcon' => '<i class="fa fa-check-square-o"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => true, 'modal' => false),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            'item_code',
            'item_name',
            'strength',
            array(
                'name'=>'category_id',
                'value'=>'ItemCategory::model()->get($data->category_id,\'category_name\')'
            ),
             'category_id',
            'reorder_level',
            'min_stock',
            'max_stock',
            'mark_up',
            'smallest_unit',
            'itemcp',
            'itemsp',

            array(
                'class' => 'ButtonColumn',
                //'header' => 'Actions',
                'template' => '{update}{delete}',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="icon-edit"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->category_id))',
                        'visible' => 'true',
                        'options' => array(
                            //'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="icon-trash text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->category_id))',
                        //'visible' => '$this->grid->owner->showLink("' . UsersModuleConstants::RES_USER_PRIVILEGES . '", "' . Acl::ACTION_DELETE . '")?true:false',
                        'visible' => 'true',
                        //'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
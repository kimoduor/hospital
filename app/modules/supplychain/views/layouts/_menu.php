<?php $can_view_system_settings = $this->showLink(SettingsModuleConstants::RES_SETTINGS); ?>
<div class="list-group">
    <?php if ($can_view_system_settings): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/default/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_GENERAL ? ' active' : '' ?>"><?php echo Lang::t('General Settings') ?></a>
    <?php endif; ?>
    <?php if ($this->showLink(SettingsModuleConstants::RES_ORG_STRUCTURE)): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/location/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_ORG_STRUCTURE ? ' active' : '' ?>"><?php echo Lang::t('Organization Structure') ?></a>
    <?php endif; ?>
    <?php if ($this->showLink(SettingsModuleConstants::RES_BANKS)): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/banks/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_BANKS ? ' active' : '' ?>"><?php echo Lang::t('Banks') ?></a>
    <?php endif; ?>
    <?php if ($can_view_system_settings): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/email/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_EMAIL ? ' active' : '' ?>"><?php echo Lang::t('Email Settings') ?></a>
        <a href="<?php echo Yii::app()->createUrl('settings/default/map') ?>" class="hidden list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_MAP ? ' active' : '' ?>"><?php echo Lang::t('Map Settings') ?></a>
        <a href="<?php echo Yii::app()->createUrl('settings/currency/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_CURRENCY ? ' active' : '' ?>"><?php echo Lang::t('Currencies') ?></a>
    <?php endif; ?>
    <?php if ($can_view_system_settings): ?>
        <a href="<?php echo Yii::app()->createUrl('notif/notifTypes/index') ?>" class="list-group-item hidden<?php echo ($this->activeTab === SettingsModuleConstants::TAB_NOTIF) ? ' active' : '' ?>"><?php echo Lang::t('Notifications') ?></a>
    <?php endif; ?>
    <?php if (Yii::app()->user->user_level === UserLevels::LEVEL_ENGINEER): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/numberFormat/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_NUMBERING_FORMAT ? ' active' : '' ?>"><?php echo Lang::t('Numbering Format') ?></a>
    <?php endif; ?>
    <?php if ($this->showLink(SettingsModuleConstants::RES_MODULES_ENABLED)): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/modules/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_MODULES_ENABLED ? ' active' : '' ?>"><?php echo Lang::t('Manage Modules') ?></a>
    <?php endif; ?>
    <?php if ($this->showLink(SettingsModuleConstants::RES_JOB_MANAGER)): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/jobManager/index') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_JOB_MANAGER ? ' active' : '' ?>"><?php echo Lang::t('Job Manager') ?></a>
    <?php endif; ?>
    <?php if ($can_view_system_settings): ?>
        <a href="<?php echo Yii::app()->createUrl('settings/default/runtime') ?>" class="list-group-item<?php echo $this->activeTab === SettingsModuleConstants::TAB_RUNTIME ? ' active' : '' ?>"><?php echo Lang::t('Runtime Logs') ?></a>
    <?php endif; ?>
</div>

<ul id="nav">

    <li  class="<?php echo ($this->activeTab === 'reportsdistribution' || $this->activeTab === 'reportssubdistribution' || $this->activeTab === 'regionaldistribution') ? ' current open' : '' ?>">
        <a href="javascript:void(0);">
            <i class="icon-signal"></i>
            Inventory
        </a>
        <ul class="sub-menu">
            <li class="<?php echo ($this->activeTab === 'county') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/settings/county/index') ?>">
                    <i class="icon-angle-right"></i>
                    Inventory
                </a>
            </li>

        </ul>
    </li>

    <li  class="<?php
    echo ($this->activeTab === 'inventorystores' || $this->activeTab === 'inventorymarkup' || $this->activeTab === 'inventoryitemcategory'
            || $this->activeTab === 'inventoryitems' || $this->activeTab === 'inventorysupplier') ? ' current open' : ''
    ?>">
        <a href="javascript:void(0);">
            <i class="icon-cog"></i>
            Setup
        </a>
        <ul class="sub-menu">

            <li class="<?php echo ($this->activeTab === 'inventorystores') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/supplychain/inventorystores/index') ?>">
                    <i class="icon-angle-right"></i>
                    Inventory Stores
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'inventorymarkup') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/supplychain/inventorymarkup/index') ?>">
                    <i class="icon-angle-right"></i>
                    Inventory Markup
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'inventoryitemcategory') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/supplychain/inventoryitemcategory/index') ?>">
                    <i class="icon-angle-right"></i>
                    Inventory Items Category
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'inventoryitems') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/supplychain/inventoryitems/index') ?>">
                    <i class="icon-angle-right"></i>
                    Inventory Items 
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'inventorysupplier') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/supplychain/inventorysupplier/index') ?>">
                    <i class="icon-angle-right"></i>
                    Suppliers
                </a>
            </li>


        </ul>
    </li>




</ul>
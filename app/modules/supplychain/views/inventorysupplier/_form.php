<div class="widget box">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'my-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => 'form-horizontal',
            'role' => 'form',
        )
    ));
    ?>

    <div class="widget-header">
        <i class="icon-reorder"></i> <h4 class="modal-title">Supplier</h4>

    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'supplier_name', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'supplier_name', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'town', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'town', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'county_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'county_id', County::model()->getListData('county_id', 'county_Name'), array('class' => 'col-md-6 select2')); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'contact_person', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'contact_person', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'telephone', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'telephone', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'bank_account', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'bank_account', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'business_no', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'business_no', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'email_address', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'email_address', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'postal_address', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'postal_address', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'kra_pin', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'kra_pin', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'vat_no', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'vat_no', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'bank_branch', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'bank_branch', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'website', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextArea($model, 'website', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'country_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
                 <?php echo CHtml::activeDropDownList($model, 'country_id', Country::model()->getListData('country_id', 'country_name'), array('class' => 'col-md-6 select2')); ?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-3 col-md-6">
            <div class="checkbox">
                <?php echo $form->checkBox($model, 'status', array()); ?>
                <?php echo Supplier::model()->getAttributeLabel('status') ?>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <a href="<?php echo CController::createUrl('index'); ?>" class="btn btn-default"><i class="icon-times"></i> <?php echo Lang::t('Close') ?></a>
        <button class="btn btn-primary" type="submit"><i class="icon-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
    </div>


    <?php $this->endWidget(); ?>
</div>
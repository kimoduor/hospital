<?php

$grid_id = 'user-roles-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => 'Suppliers',
    'titleIcon' => '<i class="fa fa-check-square-o"></i>',
    'showExportButton' => true,
    'showSearch' => true,
    'createButton' => array('visible' => true, 'modal' => false),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'columns' => array(
            'supplier_name',
            'town',
            array(
                'name' => 'county_id',
                'value' => 'County::model()->get($data->county_id,\'county_Name\')',
            ),
            'contact_person',
            'telephone',
            'bank_account',
            'business_no',
            'email_address',
            'postal_address',
            'kra_pin',
            'vat_no',
            'bank_branch',
            'website',
            array(
                'name' => 'country_id',
                'value' => 'Country::model()->get($data->country_id,\'country_name\')',
            ),
            array(
                      'name' => 'Active',
                'type' => 'raw',
                'value' => 'Details::status($data->status)',
            ),
            array(
                'class' => 'ButtonColumn',
                //'header' => 'Actions',
                'template' => '{update}{delete}',
                'htmlOptions' => array('style' => 'width: 100px;'),
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="icon-edit"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->supplier_id))',
                        'visible' => 'true',
                        'options' => array(
                            //'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="icon-trash text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->supplier_id))',
                        //'visible' => '$this->grid->owner->showLink("' . UsersModuleConstants::RES_USER_PRIVILEGES . '", "' . Acl::ACTION_DELETE . '")?true:false',
                        'visible' => 'true',
                        //'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>
<div class="widget box">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'my-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'class' => 'form-horizontal',
            'role' => 'form',
        )
    ));
    ?>

    <div class="widget-header">
        <i class="icon-reorder"></i> <h4 class="modal-title">Inventory Stores</h4>

    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'mark_up_class', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'mark_up_class', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>
        <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'mark', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeTextField($model, 'mark', array('class' => 'form-control', 'maxlength' => 128)); ?>
        </div>
    </div>

    <div class="form-group">
        <?php echo CHtml::activeLabelEx($model, 'hospital_id', array('class' => 'col-md-3 control-label')); ?>
        <div class="col-md-6">
            <?php echo CHtml::activeDropDownList($model, 'hospital_id', Hospitals::model()->getListData('hospital_id', 'name'), array('class' => 'col-md-6 select2')); ?>
        </div>
    </div> 

    <div class="modal-footer">
        <a href="<?php echo CController::createUrl('index'); ?>" class="btn btn-default"><i class="icon-times"></i> <?php echo Lang::t('Close') ?></a>
        <button class="btn btn-primary" type="submit"><i class="icon-check"></i> <?php echo Lang::t($model->isNewRecord ? 'Create' : 'Save changes') ?></button>
    </div>

  
    <?php $this->endWidget(); ?>
</div>
<?php

/**
 * This is the model class for table "inventory_stores".
 *
 * The followings are the available columns in table 'inventory_stores':
 * @property string $store_id
 * @property string $store_name
 * @property string $store_category
 * @property string $date_created
 * @property integer $created_by
 * @property string $hospital_id
 *
 * The followings are the available model relations:
 * @property HpHospitalRegistration $hospital
 */
class Stores extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inventory_stores';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('store_name, store_category, hospital_id', 'required'),
			array('created_by', 'numerical', 'integerOnly'=>true),
			array('store_name', 'length', 'max'=>50),
			array('store_category', 'length', 'max'=>30),
			array('hospital_id', 'length', 'max'=>20),
			array('date_created', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('store_id, store_name, store_category, date_created, created_by, hospital_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hospital' => array(self::BELONGS_TO, 'HpHospitalRegistration', 'hospital_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'store_id' => 'Store',
			'store_name' => 'Store Name',
			'store_category' => 'Store Category',
			'date_created' => 'Date Created',
			'created_by' => 'Created By',
			'hospital_id' => 'Hospital',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('store_id',$this->store_id,true);
		$criteria->compare('store_name',$this->store_name,true);
		$criteria->compare('store_category',$this->store_category,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('hospital_id',$this->hospital_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Stores the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

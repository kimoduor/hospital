<?php

/**
 * This is the model class for table "inventory_supplier".
 *
 * The followings are the available columns in table 'inventory_supplier':
 * @property string $supplier_id
 * @property string $supplier_name
 * @property string $town
 * @property string $county_id
 * @property string $contact_person
 * @property string $telephone
 * @property string $bank_account
 * @property string $business_no
 * @property string $email_address
 * @property string $postal_address
 * @property string $kra_pin
 * @property string $vat_no
 * @property string $bank_branch
 * @property string $website
 * @property string $country_id
 * @property integer $status
 * @property integer $created_by
 * @property string $date_created
 *
 * The followings are the available model relations:
 * @property HpCountry $country
 * @property HpCounty $county
 */
class Supplier extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inventory_supplier';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('county_id, country_id supplier_name', 'required'),
			array('status, created_by', 'numerical', 'integerOnly'=>true),
			array('supplier_name, postal_address', 'length', 'max'=>100),
			array('town, contact_person, email_address, kra_pin, vat_no, website', 'length', 'max'=>50),
			array('county_id, country_id', 'length', 'max'=>11),
			array('telephone, bank_account, business_no', 'length', 'max'=>30),
			array('bank_branch', 'length', 'max'=>40),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('supplier_id, supplier_name, town, county_id, contact_person, telephone, bank_account, business_no, email_address, postal_address, kra_pin, vat_no, bank_branch, website, country_id, status, created_by, date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'country' => array(self::BELONGS_TO, 'HpCountry', 'country_id'),
			'county' => array(self::BELONGS_TO, 'HpCounty', 'county_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'supplier_id' => 'Supplier',
			'supplier_name' => 'Supplier Name',
			'town' => 'Town',
			'county_id' => 'County',
			'contact_person' => 'Contact Person',
			'telephone' => 'Telephone',
			'bank_account' => 'Bank Account',
			'business_no' => 'Business No',
			'email_address' => 'Email Address',
			'postal_address' => 'Postal Address',
			'kra_pin' => 'Kra Pin',
			'vat_no' => 'Vat No',
			'bank_branch' => 'Bank Branch',
			'website' => 'Website',
			'country_id' => 'Country',
			'status' => 'Status',
			'created_by' => 'Created By',
			'date_created' => 'Date Created',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('supplier_id',$this->supplier_id,true);
		$criteria->compare('supplier_name',$this->supplier_name,true);
		$criteria->compare('town',$this->town,true);
		$criteria->compare('county_id',$this->county_id,true);
		$criteria->compare('contact_person',$this->contact_person,true);
		$criteria->compare('telephone',$this->telephone,true);
		$criteria->compare('bank_account',$this->bank_account,true);
		$criteria->compare('business_no',$this->business_no,true);
		$criteria->compare('email_address',$this->email_address,true);
		$criteria->compare('postal_address',$this->postal_address,true);
		$criteria->compare('kra_pin',$this->kra_pin,true);
		$criteria->compare('vat_no',$this->vat_no,true);
		$criteria->compare('bank_branch',$this->bank_branch,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('country_id',$this->country_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('date_created',$this->date_created,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Supplier the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

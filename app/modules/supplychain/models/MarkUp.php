<?php

/**
 * This is the model class for table "inventory_mark_up".
 *
 * The followings are the available columns in table 'inventory_mark_up':
 * @property string $mark_up_id
 * @property string $mark_up_class
 * @property double $mark
 * @property string $hospital_id
 *
 * The followings are the available model relations:
 * @property HpHospitalRegistration $hospital
 */
class MarkUp extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inventory_mark_up';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mark_up_class', 'required'),
			array('mark', 'numerical'),
			array('mark_up_class', 'length', 'max'=>10),
			array('hospital_id', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('mark_up_id, mark_up_class, mark, hospital_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hospital' => array(self::BELONGS_TO, 'HpHospitalRegistration', 'hospital_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'mark_up_id' => 'Mark Up',
			'mark_up_class' => 'Mark Up Class',
			'mark' => 'Mark',
			'hospital_id' => 'Hospital',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('mark_up_id',$this->mark_up_id,true);
		$criteria->compare('mark_up_class',$this->mark_up_class,true);
		$criteria->compare('mark',$this->mark);
		$criteria->compare('hospital_id',$this->hospital_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MarkUp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

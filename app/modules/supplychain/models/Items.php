<?php

/**
 * This is the model class for table "inventory_items".
 *
 * The followings are the available columns in table 'inventory_items':
 * @property string $inventory_items_id
 * @property string $item_code
 * @property string $item_name
 * @property string $strength
 * @property string $category_id
 * @property double $reorder_level
 * @property double $min_stock
 * @property double $max_stock
 * @property string $mark_up
 * @property string $smallest_unit
 * @property double $itemcp
 * @property double $itemsp
 *
 * The followings are the available model relations:
 * @property ItemCategory $category
 */
class Items extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'inventory_items';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id', 'required'),
			array('reorder_level, min_stock, max_stock, itemcp, itemsp', 'numerical'),
			array('item_code', 'length', 'max'=>10),
			array('item_name', 'length', 'max'=>100),
			array('strength, category_id', 'length', 'max'=>20),
			array('mark_up, smallest_unit', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('inventory_items_id, item_code, item_name, strength, category_id, reorder_level, min_stock, max_stock, mark_up, smallest_unit, itemcp, itemsp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'ItemCategory', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'inventory_items_id' => 'Inventory Items',
			'item_code' => 'Item Code',
			'item_name' => 'Item Name',
			'strength' => 'Strength',
			'category_id' => 'Category',
			'reorder_level' => 'Reorder Level',
			'min_stock' => 'Min Stock',
			'max_stock' => 'Max Stock',
			'mark_up' => 'Mark Up',
			'smallest_unit' => 'Smallest Unit',
			'itemcp' => 'Itemcp',
			'itemsp' => 'Itemsp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('inventory_items_id',$this->inventory_items_id,true);
		$criteria->compare('item_code',$this->item_code,true);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('strength',$this->strength,true);
		$criteria->compare('category_id',$this->category_id,true);
		$criteria->compare('reorder_level',$this->reorder_level);
		$criteria->compare('min_stock',$this->min_stock);
		$criteria->compare('max_stock',$this->max_stock);
		$criteria->compare('mark_up',$this->mark_up,true);
		$criteria->compare('smallest_unit',$this->smallest_unit,true);
		$criteria->compare('itemcp',$this->itemcp);
		$criteria->compare('itemsp',$this->itemsp);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Items the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

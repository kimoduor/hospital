<?php

/**
 * @author Joakim <kimoduor@gmail.com>
 * Parent controller for the settings module
 */
class FinanceModuleController extends Controller
{

    public function init()
    {
        if (empty($this->activeMenu))
            $this->activeMenu = FinanceModuleConstants::MENU_FINANCE;
        if (empty($this->resource))
            $this->resource = FinanceModuleConstants::RES_FINANCE;
      //  Yii::import('application.modules.distributors.forms.*');
        parent::init();
    }

    public function setModulePackage()
    {

        $this->module_package = array(
            'baseUrl' => $this->module_assets_url,
            'js' => array(
                'js/module.js',
                        ),
            'css' => array(
            ),
        );
    }

}

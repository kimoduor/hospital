<?php

class FinanceController extends FinanceModuleController {

    public function init() {
       $this->activeTab = 'finance';
        $this->resourceLabel = 'finance';
        $this->menu='finance';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index','create','update','delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Hospitals();
        $model_class_name = get_class($model);
          if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        $error_message = CActiveForm::validate($model);
                        $error_message_decoded = CJSON::decode($error_message);
                        
                if (!empty($error_message_decoded)) {
                    $errorlist = '';
                    foreach ($error_message_decoded as $error) {
                        $errorlist.=$error[0] . '<br />';
                    }
                    Yii::app()->user->setFlash('error', $errorlist);
                } else {
                    $model->save(FALSE);
                    Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                    $this->redirect(array('index'));
                    Yii::app()->end();
                }
                }
       
        //Yii::app()->end();
        $this->render('_form', array('model' => $model));
    }

    public function actionUpdate($id) {
        $model = Hospitals::model()->loadModel($id);
        $model_class_name = get_class($model);
          if (isset($_POST[$model_class_name])) {
                        $model->attributes = $_POST[$model_class_name];
                        $error_message = CActiveForm::validate($model);
                        $error_message_decoded = CJSON::decode($error_message);
                        
                         if (!empty($error_message_decoded)) {
                    $errorlist = '';
                    foreach ($error_message_decoded as $error) {
                        $errorlist.=$error[0] . '<br />';
                    }
                    Yii::app()->user->setFlash('error', $errorlist);
                } else {
                    $model->save(FALSE);
                    Yii::app()->user->setFlash('success', Lang::t('SUCCESS_MESSAGE'));
                    $this->redirect(array('index'));
                    Yii::app()->end();
                }
                }

        $this->render('_form', array('model' => $model));
    }

 

    public function actionIndex($id = null) {
        $condition = '';
$this->pageTitle="Finance";

        $this->render('index');
    }

     public function actionDelete($id) {
         if (isset($_GET['confirmdelete'])) {
            try {
                Hospitals::model()->loadModel($id)->delete();
                 Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {                
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));

            }
        }
    }

}

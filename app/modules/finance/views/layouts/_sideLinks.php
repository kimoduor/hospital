<ul id="nav">

    <li  class="<?php echo ($this->activeTab === 'reportsdistribution' || $this->activeTab === 'reportssubdistribution' || $this->activeTab === 'regionaldistribution') ? ' current open' : '' ?>">
        <a href="javascript:void(0);">
            <i class="icon-signal"></i>
            Finance
        </a>
        <ul class="sub-menu">


        </ul>
    </li>

    <li  class="<?php
    echo ($this->activeTab === 'county' || $this->activeTab === 'country' || $this->activeTab === 'diseases' || $this->activeTab === 'countyward' || $this->activeTab === 'educationlevel' || $this->activeTab === 'ethnicity' || $this->activeTab === 'procedure' ||
    $this->activeTab === 'relationship' || $this->activeTab === 'religion' || $this->activeTab === 'servicepoint') ? ' current open' : ''
    ?>">
        <a href="javascript:void(0);">
            <i class="icon-cog"></i>
            Setup
        </a>
        <ul class="sub-menu">

            <li class="<?php echo ($this->activeTab === 'county') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/settings/county/index') ?>">
                    <i class="icon-angle-right"></i>
                    County
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'country') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/settings/country/index') ?>">
                    <i class="icon-angle-right"></i>
                    Country
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'countyward') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/settings/countyward/index') ?>">
                    <i class="icon-angle-right"></i>
                    Country Ward
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'diseases') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/settings/diseases/index') ?>">
                    <i class="icon-angle-right"></i>
                    Diseases
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'educationlevel') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/settings/educationlevel/index') ?>">
                    <i class="icon-angle-right"></i>
                    Education Level
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'ethnicity') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/settings/ethnicity/index') ?>">
                    <i class="icon-angle-right"></i>
                    Ethnicity
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'procedure') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/settings/procedure/index') ?>">
                    <i class="icon-angle-right"></i>
                    Procedure
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'relationship') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/settings/relationship/index') ?>">
                    <i class="icon-angle-right"></i>
                    Relationship
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'religion') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/settings/religion/index') ?>">
                    <i class="icon-angle-right"></i>
                    Religion
                </a>
            </li>
            <li class="<?php echo ($this->activeTab === 'servicepoint') ? ' current' : '' ?>">
                <a href="<?php echo Yii::app()->createUrl('/settings/servicepoint/index') ?>">
                    <i class="icon-angle-right"></i>
                    Service Point
                </a>
            </li>

        </ul>
    </li>




</ul>
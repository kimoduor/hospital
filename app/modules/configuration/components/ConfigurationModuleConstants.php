<?php

/**
 * Defines all constants used within the module
 *
 * @author Joakim <kimoduor@gmail.com>
 */
class ConfigurationModuleConstants {
    //resources constants

    const RES_CONFIGURATION = 'Configuration';

      const TAB_GENERAL = 'TAB_GENERAL';
       const MENU_CONFIGURATION = 'MENU_CONFIGURATION';
    

}

<?php

/**
 * This is the model class for table "hp_hospital_registration".
 *
 * The followings are the available columns in table 'hp_hospital_registration':
 * @property string $hospital_id
 * @property string $name
 * @property string $county_id
 * @property string $country_id
 * @property string $address
 * @property string $location
 * @property string $telephone
 *
 * The followings are the available model relations:
 * @property Country $country
 * @property County $county
 */
class Hospitals extends ActiveRecord implements IMyActiveSearch
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hp_hospital_registration';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, county_id', 'required'),
			array('name, address, location, telephone', 'length', 'max'=>255),
			array('county_id, country_id', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('hospital_id, name, county_id, country_id, address, location, telephone', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
			'county' => array(self::BELONGS_TO, 'County', 'county_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'hospital_id' => 'Hospital',
			'name' => 'Name',
			'county_id' => 'County',
			'country_id' => 'Country',
			'address' => 'Address',
			'location' => 'Location',
			'telephone' => 'Telephone',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Hospitals the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
    public function searchParams() {
        return array(
          
                // 'id',
        );
    }
}

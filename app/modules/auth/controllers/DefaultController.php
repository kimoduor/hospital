<?php

/**
 *
 */
class DefaultController extends AuthModuleController {

    public function init() {
        // register class paths for extension captcha extended
        Yii::$classMap = array_merge(Yii::$classMap, array(
            'CaptchaExtendedAction' => Yii::getPathOfAlias('ext.captcha') . DIRECTORY_SEPARATOR . 'CaptchaExtendedAction.php',
            'CaptchaExtendedValidator' => Yii::getPathOfAlias('ext.captcha') . DIRECTORY_SEPARATOR . 'CaptchaExtendedValidator.php'
        ));
        parent::init();
    }

    public function actions() {
        return array(
            'captcha' => array(
                'class' => 'CaptchaExtendedAction',
                // if needed, modify settings
                'mode' => CaptchaExtendedAction::MODE_DEFAULT,
            ),
        );
    }

    public function actionLogin() {
        if (!Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->homeUrl);

        $this->pageTitle = Lang::t('Login page');

        $model = new LoginForm;
        $model_class_name = get_class($model);

        // collect user input data
        if (isset($_POST[$model_class_name])) {
           // $model->userx=$_POST['username'];
            $model->attributes = $_POST[$model_class_name];
            $model->_identity = new UserIdentity($model->username, $model->password);
            $model->_identity->user = Users::model()->find('`username`=:t1 OR `email`=:t1', array(':t1' => $model->username));

            if ($model->validate()) {
                if ($model->_identity->login($model->rememberMe)) {

                    if (Yii::app()->user->user_level === UserLevels::LEVEL_AGRODEALER) {
                        $this->redirect(Yii::app()->createUrl('default/verification'));                       
                    } elseif (Yii::app()->user->user_level === UserLevels::LEVEL_SUPPLIER) {
                        $this->redirect(Yii::app()->createUrl('default/verification'));
                    }
                    elseif (Yii::app()->user->user_level === UserLevels::LEVEL_DISTRICTME) {
                        $this->redirect(Yii::app()->createUrl('default/verification'));
                    }
                     elseif (Yii::app()->user->user_level === UserLevels::LEVEL_PROVINCEME) {
                        $this->redirect(Yii::app()->createUrl('default/verification'));
                    }else {
                         $this->redirect(Yii::app()->user->returnUrl);
                    }
                }
            }
        }

        $this->render('login', array(
            'model' => $model,
        ));
    }

    public function actionActivate($id, $token) {
        $model = Users::model()->loadModel($id);
        if ($model->activation_code !== $token)
            throw new CHttpException(403, Lang::t('403_error'));

        $model->status = Users::STATUS_ACTIVE;
        $model->activation_code = null;
        if ($model->save(false)) {
            $member_model = Customer::model()->find('`user_id`=:t1', array(':t1' => $model->id));
            if (null !== $member_model) {
                MemberNetwork::model()->updateNetwork($member_model->id, $member_model->invited_by);
            }
        }

        Yii::app()->user->setFlash('success', 'You have successfully activated your account. You may now sign in to your account.');
        //redirect to login page
        $this->redirect(array('login'));
    }

    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionForgotPassword() {
        $this->pageTitle = 'Password Recovery';

        $model = new ForgotPasswordForm();
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->validate()) {
                Yii::app()->user->setFlash('success', $model->success_message);
                $this->refresh();
            }
        }

        $this->render('forgotPassword', array(
            'model' => $model,
        ));
    }

    /**
     * User enters a new password
     * @param type $id
     * @param type $token
     * @throws CHttpException
     */
    public function actionResetPassword($id, $token) {
        $this->pageTitle = Lang::t('Reset your Password');
        $model = Users::model()->loadModel($id);

        if ($model->password_reset_code !== $token)
            throw new CHttpException(400, Lang::t('400_error'));

        $model->password_reset_code = null;
        $model->password_reset_date = new CDbExpression('NOW()');
        $model->setScenario(Users::SCENARIO_RESET_PASSWORD);
        $model->password = NULL;
        $model->send_email = FALSE;
        $model_class_name = $model->getClassName();

        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];
            if ($model->validate(array('password', 'confirm'))) {
                $model->save(false);
                Yii::app()->user->setFlash('success', Lang::t('Success. Please use your new password to sign in'));
                $this->redirect(array('login'));
            }
        }

        $this->render('resetPassword', array(
            'model' => $model,
        ));
    }

}

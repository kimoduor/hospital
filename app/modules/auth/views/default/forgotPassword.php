<div class="row">
  
<center><img height="10" width="220" src="<?php echo Yii::app()->theme->baseUrl ?>/assets/img/teractivelogo.png" alt="logo" /></center>
    <br/>
    <div class="col-md-offset-4 col-md-4">
        <div class="well no-padding">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'forgot-password-form',
                'enableClientValidation' => false,
                'focus' => array($model, 'username'),
                'clientOptions' => array(
                    'validateOnSubmit' => false,
                ),
                'htmlOptions' => array(
                    'class' => 'smart-form client-form',
                )
            ));
            ?>
            <center><strong>Password Recovery</strong></center>
            <?php echo $form->errorSummary($model, ''); ?>
            <fieldset>
               
                 Enter Your Email
 
                        <?php echo $form->textField($model, 'username', array('class' => '', 'required' => true,'class'=>'form-control')); ?>
            </fieldset>
            <footer>
                <button type="submit" class="btn btn-primary"><?php echo Lang::t('Submit'); ?></button>
                <a class="btn btn-link" href="<?php echo $this->createUrl('login') ?>"><?php echo Lang::t('Back to login') ?></a>

            </footer>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<div class="row">
    <center></center>
    <br/>
    <div class="col-md-offset-4 col-md-4">
        <div class="well no-padding">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => false,
                'focus' => array($model, 'username'),
                'clientOptions' => array(
                    'validateOnSubmit' => false,
                ),
                'htmlOptions' => array(
                    'class' => 'smart-form client-form',
                )
            ));
            ?>
            <center><strong>Login</strong></center>
            <br />
            <fieldset>
                <?php echo $form->errorSummary($model, ''); ?>
               
                        <?php echo $form->textField($model, 'username', array('style' => 'height:50px;font-size:14', 'required' => true,'class'=>'form-control', 'placeholder' => Lang::t('Username or Email'))); ?>
       
             <br />
                        <?php echo $form->passwordField($model, 'password', array('style' => 'height:50px;font-size:14', 'required' => true,'class'=>'form-control',  'placeholder' => Lang::t('Password'))); ?>
                    
                    <div class="note">
                        <a href="<?php echo $this->createUrl('forgotPassword') ?>"><?php echo Lang::t('Forgot password?') ?></a>
                    </div>
               <br />
                <section>
                    <div class="text-left" style="padding-left: 0px;">
                        <?php if (CCaptcha::checkRequirements()): ?>
                            <?php
                            $this->widget('CCaptcha', array(
                                'buttonLabel' => '&nbsp;New Code',
                                'imageOptions' => array('style' => 'width:200px;'),
                            ));
                            ?>
                            <br/>
                            <?php echo CHtml::activeTextField($model, 'verifyCode', array('style' => 'width:200px;')); ?>
                            <p class="help-block"><?php echo Lang::t('Enter the code above.') ?></p>
                        <?php endif; ?>
                    </div>
                </section>
            </fieldset>
            <footer>
                <button type="submit" class="btn btn-default" style="width:100%"><?php echo Lang::t('Sign in'); ?></button>
            </footer>
            <br />
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
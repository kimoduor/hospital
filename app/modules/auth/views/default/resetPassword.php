<div class="row">
    <div class="col-md-offset-4 col-md-4">
        <div class="well no-padding">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'reset-password-form',
                'enableClientValidation' => false,
                'focus' => array($model, 'username'),
                'clientOptions' => array(
                    'validateOnSubmit' => false,
                ),
                'htmlOptions' => array(
                    'class' => 'smart-form client-form',
                )
            ));
            ?>
            <header><?php echo CHtml::encode($this->pageTitle) ?></header>
            <?php echo $form->errorSummary($model, ''); ?>
            <fieldset>
                <section>
                    <label class="label"><?php echo Lang::t('New Password') ?></label>
                    <label class="input"> <i class="icon-append fa fa-lock"></i>
                        <?php echo $form->passwordField($model, 'password', array('class' => '', 'required' => true)); ?>
                    </label>
                </section>
                <section>
                    <label class="label"><?php echo Lang::t('Confirm New Password') ?></label>
                    <label class="input"> <i class="icon-append fa fa-lock"></i>
                        <?php echo $form->passwordField($model, 'confirm', array('class' => '', 'required' => true)); ?>
                    </label>
                </section>
            </fieldset>
            <footer>
                <button type="submit" class="btn btn-primary"><i class="fa fa-refresh"></i> <?php echo Lang::t('Reset Password'); ?></button>
            </footer>
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
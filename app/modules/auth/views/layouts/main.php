<!DOCTYPE html>
<html lang="en">
    <?php echo $this->renderPartial('application.views.layouts._head') ?>
    <body class="fixed-header menu-on-top smart-style-3">
        <!-- possible classes: minified, fixed-ribbon, fixed-header, fixed-width-->
        <?php echo $this->renderPartial('application.views.layouts._header') ?>
        <div id="main" role="main">
            <!-- MAIN CONTENT -->
            <div id="content">
                <div class="row" style="margin-left: -13px;margin-right: -13px">
                    <div class="col-md-12">
                        <?php $this->renderPartial('application.views.widgets._alert') ?>
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="page-footer">
            <div class="row" style="margin-left: -13px;margin-right: -13px">
                <div class="col-xs-12 col-sm-6">
</div>
                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END MAIN PANEL -->
        <!--modal-->
        <div class="modal fade" id="my_bs_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                </div>
            </div>
        </div>
        <!--end modal-->
    </body>
</html>
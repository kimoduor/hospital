<?php

/**
 * Base controller for the auth module
 * @author Joakim <kimoduor@gmail.com>
 */
class AuthModuleController extends Controller {

        public $layout = '/layouts/main';

        public function init()
        {
                parent::init();
        }

}

<?php

/**
 * @author Joakim <kimoduor@gmail.com>
 * Parent controller for the settings module
 */
class ReportsModuleController extends Controller
{

    public function init()
    {
        if (empty($this->activeMenu))
            $this->activeMenu = ReportsModuleConstants::MENU_REPORTS;
        if (empty($this->resource))
            $this->resource = ReportsModuleConstants::RES_REPORTS;
      //  Yii::import('application.modules.distributors.forms.*');
        parent::init();
    }

    public function setModulePackage()
    {

        $this->module_package = array(
            'baseUrl' => $this->module_assets_url,
            'js' => array(
                'js/module.js',
                        ),
            'css' => array(
            ),
        );
    }

}

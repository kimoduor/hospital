<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class ReportController extends ReportsModuleController {

    public function init() {

        $this->resourceLabel = Lang::t('Reports');
        $this->resource = ReportsModuleConstants::RES_REPORTS;
        $this->activeTab = ReportsModuleConstants::TAB_GENERAL;
        $this->menu='reports';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    public function actionIndex($id = null) {
        $this->pageTitle = "Reports";

        $this->render('index');
    }
   

}

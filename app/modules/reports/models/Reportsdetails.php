<?php

class Reportsdetails {

    public static function generalcolumns() {
        $firstarray = Reportsdetails::first();
        $productaarray = Reportsdetails::products();
        return array_merge($firstarray, $productaarray);
    }

    public static function first() {
        $arrayfirst = array(
            array(
                'name' => 'distributor_id',
                'value' => 'Details::distributorname($data->distributor_id)',
                'visible' => true,
            ),
        );
        return $arrayfirst;
    }

    public static function getqty($distributor_id, $pdt) {
        $startdate = @$_POST['Distribution']['startdate'];
        $enddatex = @$_POST['Distribution']['enddate'];
        $date1 = str_replace('-', '/', $enddatex);
        $enddate = date('Y-m-d', strtotime($date1 . "+1 days"));
        $productlabel = Measureunits::model()->get(Products::model()->get($pdt, 'measure_unit_id'), 'unit');

        $output = Yii::app()->db->createCommand("SELECT  sum(quantity) as quantity FROM tbldistribution WHERE product_id = $pdt AND "
                        . "date_created  BETWEEN '$startdate' AND '$enddate'  AND distributor_id=$distributor_id")->queryScalar();
        $output2 = Yii::app()->db->createCommand("SELECT  sum(net_sales_value) as net_sales_value FROM tbldistribution WHERE product_id = $pdt AND "
                        . "date_created  BETWEEN '$startdate' AND '$enddate'  AND distributor_id=$distributor_id")->queryScalar();
        $foutput = $output > 0 ? $output . " " . $productlabel : '';
        $foutput2 = $output2 > 0 ? number_format($output2, 2) : '';

        return $foutput . '<span class="pull-right">' . $foutput2 . '</span>';
    }

    public static function products() {
        $products = Products::model()->findAll();
        $arrayfirst = array();
        foreach ($products as $product) {
            $arrayfirst[] = array(
                'name' => $product->name,
                'header' => '<span>' . $product->name . '(Quantity)</span> &nbsp; <span class="pull-right"> Sales (Ksh)</span>',
                'type' => 'raw',
                'value' => 'Reportsdetails::getqty($data->distributor_id,' . $product->product_id . ')',
            );
        }


        return $arrayfirst;
    }

    public static function distributionchart($distributor_id, $pdt) {
        $startdate = @$_POST['Distribution']['startdate'];
        $enddatex = @$_POST['Distribution']['enddate'];
        $date1 = str_replace('-', '/', $enddatex);
        $enddate = date('Y-m-d', strtotime($date1 . "+1 days"));
        $productlabel = Measureunits::model()->get(Products::model()->get($pdt, 'measure_unit_id'), 'unit');

        $output = Yii::app()->db->createCommand("SELECT  sum(quantity) as quantity FROM tbldistribution WHERE product_id = $pdt AND "
                        . "date_created  BETWEEN '$startdate' AND '$enddate'  AND distributor_id=$distributor_id")->queryScalar();
        $output2 = Yii::app()->db->createCommand("SELECT  sum(net_sales_value) as net_sales_value FROM tbldistribution WHERE product_id = $pdt AND "
                        . "date_created  BETWEEN '$startdate' AND '$enddate'  AND distributor_id=$distributor_id")->queryScalar();
        $foutput = $output > 0 ? $output . " " . $productlabel : '';
        $foutput2 = $output2 > 0 ? number_format($output2, 2) : '';

        return $foutput . '<span class="pull-right">' . $foutput2 . '</span>';
    }

    public static function generalcolumnssub() {
        $firstarray = Reportsdetails::firstsub();
        $productaarray = Reportsdetails::productssub();
        return array_merge($firstarray, $productaarray);
    }

    public static function firstsub() {
        $arrayfirst = array(
            array(
                'name' => 'subdistributor_id',
                'value' => 'Details::subdistributorname($data->subdistributor_id)',
                'visible' => true,
            ),
        );
        return $arrayfirst;
    }

    public static function getqtysub($subdistributor_id, $pdt) {
        $startdate = @$_POST['Subdistribution']['startdate'];
        $enddatex = @$_POST['Subdistribution']['enddate'];
        $date1 = str_replace('-', '/', $enddatex);
        $enddate = date('Y-m-d', strtotime($date1 . "+1 days"));
        $productlabel = Measureunits::model()->get(Products::model()->get($pdt, 'measure_unit_id'), 'unit');

        $output = Yii::app()->db->createCommand("SELECT  sum(quantity) as quantity FROM tblsubdistribution WHERE product_id = $pdt AND "
                        . "date_created  BETWEEN '$startdate' AND '$enddate'  AND subdistributor_id=$subdistributor_id")->queryScalar();
        $output2 = Yii::app()->db->createCommand("SELECT  sum(net_sales_value) as net_sales_value FROM tblsubdistribution WHERE product_id = $pdt AND "
                        . "date_created  BETWEEN '$startdate' AND '$enddate'  AND subdistributor_id=$subdistributor_id")->queryScalar();
        $foutput = $output > 0 ? $output . " " . $productlabel : '';
        $foutput2 = $output2 > 0 ? number_format($output2, 2) : '';

        return $foutput . '<span class="pull-right">' . $foutput2 . '</span>';
    }

    public static function productssub() {
        $products = Products::model()->findAll();
        $arrayfirst = array();
        foreach ($products as $product) {
            $arrayfirst[] = array(
                'name' => $product->name,
                'header' => '<span>' . $product->name . '(Quantity)</span> &nbsp; <span class="pull-right"> Sales (Ksh)</span>',
                'type' => 'raw',
                'value' => 'Reportsdetails::getqtysub($data->subdistributor_id,' . $product->product_id . ')',
            );
        }


        return $arrayfirst;
    }

    //---------------------------region id in the distrivbution--------------------
    public static function generalcolumnsdistributionregions() {
        $firstarray = Reportsdetails::firstdistributionregions();
        $productaarray = Reportsdetails::productsdistributionregions();
        return array_merge($firstarray, $productaarray);
    }

    public static function firstdistributionregions() {
        $arrayfirst = array(
            array(
                'name' => 'region_id',
                'value' => 'Regions::model()->get($data->region_id,\'region\')',
                'visible' => true,
            ),
        );
        return $arrayfirst;
    }

    public static function getqtydistributionregions($region_id, $pdt) {
        $startdate = @$_POST['Distribution']['startdate'];
        $enddatex = @$_POST['Distribution']['enddate'];
        $date1 = str_replace('-', '/', $enddatex);
        $enddate = date('Y-m-d', strtotime($date1 . "+1 days"));
        $productlabel = Measureunits::model()->get(Products::model()->get($pdt, 'measure_unit_id'), 'unit');

        $output = Yii::app()->db->createCommand("SELECT  sum(quantity) as quantity FROM tbldistribution WHERE product_id = $pdt AND "
                        . "date_created  BETWEEN '$startdate' AND '$enddate'  AND "
                        . "distributor_id IN (SELECT distributor_id FROM tbldistributors WHERE region_id=$region_id)")->queryScalar();
        $output2 = Yii::app()->db->createCommand("SELECT  sum(net_sales_value) as net_sales_value FROM tbldistribution WHERE product_id = $pdt AND "
                        . "date_created  BETWEEN '$startdate' AND '$enddate'  AND "
                        . "distributor_id IN (SELECT distributor_id FROM tbldistributors WHERE region_id=$region_id)")->queryScalar();
        $foutput = $output > 0 ? $output . " " . $productlabel : '';
        $foutput2 = $output2 > 0 ? number_format($output2, 2) : '';

        return $foutput . '<span class="pull-right">' . $foutput2 . '</span>';
    }

    public static function productsdistributionregions() {
        $products = Products::model()->findAll();
        $arrayfirst = array();
        foreach ($products as $product) {
            $arrayfirst[] = array(
                'name' => $product->name,
                'header' => '<span>' . $product->name . '(Quantity)</span> &nbsp; <span class="pull-right"> Sales (Ksh)</span>',
                'type' => 'raw',
                'value' => 'Reportsdetails::getqtydistributionregions($data->region_id,' . $product->product_id . ')',
            );
        }


        return $arrayfirst;
    }

    public static function distributionchartdistributionregions($region_id, $pdt) {
        $startdate = @$_POST['Distribution']['startdate'];
        $enddatex = @$_POST['Distribution']['enddate'];
        $date1 = str_replace('-', '/', $enddatex);
        $enddate = date('Y-m-d', strtotime($date1 . "+1 days"));
        $productlabel = Measureunits::model()->get(Products::model()->get($pdt, 'measure_unit_id'), 'unit');

        $output = Yii::app()->db->createCommand("SELECT  sum(quantity) as quantity FROM tbldistribution WHERE product_id = $pdt AND "
                        . "date_created  BETWEEN '$startdate' AND '$enddate'  AND "
                        . "distributor_id IN (SELECT distributor_id FROM tbldistributors WHERE region_id=$region_id)")->queryScalar();
        $output2 = Yii::app()->db->createCommand("SELECT  sum(net_sales_value) as net_sales_value FROM tbldistribution WHERE product_id = $pdt AND "
                        . "date_created  BETWEEN '$startdate' AND '$enddate'  AND "
                       . "distributor_id IN (SELECT distributor_id FROM tbldistributors WHERE region_id=$region_id)")->queryScalar();
        $foutput = $output > 0 ? $output . " " . $productlabel : '';
        $foutput2 = $output2 > 0 ? number_format($output2, 2) : '';

        return $foutput . '<span class="pull-right">' . $foutput2 . '</span>';
    }

}

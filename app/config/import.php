<?php

/**
 * stores the import configurations
 * @author Fredrick <mconyango@gmail.com>
 */
return array(
    'application.extensions.*',
    'application.models.*',
    'application.models.forms.*',
    'application.components.*',
    'application.components.widgets.*',
    'ext.easyimage.EasyImage',
    //user module
    'application.modules.users.models.*',
    'application.modules.users.components.*',
    'application.modules.users.components.behaviors.*',
    //settings module
    'application.modules.settings.models.*',
    'application.modules.settings.components.*',
    //auth module
    'application.modules.auth.models.*',
    'application.modules.auth.components.*',
    //hospital
    'application.modules.hospital.models.*',
    'application.modules.hospital.components.*',
    //supplychain module
    'application.modules.supplychain.models.*',
    'application.modules.supplychain.components.*',
        //finance module
    'application.modules.finance.models.*',
    'application.modules.finance.components.*',
            //reports module
    'application.modules.reports.models.*',
    'application.modules.reports.components.*',
                //configuration module
    'application.modules.configuration.models.*',
    'application.modules.configuration.components.*',
//
//
//
);

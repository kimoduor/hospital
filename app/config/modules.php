<?php

/**
 * stores the modules configurations
 * @author joakim <kimoduor@gmail.com>
 */
return array(
    'gii' => array(
        'class' => 'system.gii.GiiModule',
        'password' => 'root',
        'ipFilters' => array('127.0.0.1', '::1'),
    ),
    'users',
    'auth',
    'settings',
    'hospital',
    'supplychain',
    'reports',
    'finance',
    'configuration',
);

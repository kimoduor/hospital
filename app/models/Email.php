<?php

/**
 * This is the model class for table "email".
 *
 * The followings are the available columns in table 'email':
 * @property integer $id
 *  @property integer $sent_by
 * @property string $from_name
 * @property string $from_email
 * @property string $to_email
 * @property string $to_name
 * @property string $subject
 * @property string $message
 * @property string $date_created
 * @property string $date_queued
 */
class Email extends ActiveRecord
{

    const EMAIL_SENDING_METHOD_DIRECT = 'Direct';
    const EMAIL_SENDING_METHOD_QUEUED = 'Queued';
    const SENDTYPE_ALL_STAFF = 'all_staff';
    //scenarios
    const SCENARIO_MASS_EMAIL = 'mass_email';

    /**
     * Paths to uploaded images
     * @var type
     */
    public $attachments = array();

    /**
     *
     * @var type
     */
    public $send_type;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Email the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'email';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('from_email, subject, message', 'required'),
            array('to_email', 'required', 'on' => self::SCENARIO_CREATE),
            array('sent_by', 'numerical', 'integerOnly' => true),
            array('from_name,to_name', 'length', 'max' => 60),
            array('from_email, to_email', 'length', 'max' => 128),
            array('subject', 'length', 'max' => 255),
            array('date_created,send_type', 'safe'),
            array('message', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')), //html purification
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'from_name' => 'From Name',
            'from_email' => 'From Email',
            'to_email' => 'To Email',
            'to_name' => 'To Name',
            'subject' => 'Subject',
            'message' => 'Message',
            'date_created' => 'Date Created',
            'date_queued' => 'Date Queued',
            'send_type' => 'Type',
            'sent_by' => 'Sent By',
        );
    }

    public function afterSave()
    {
        return parent::afterSave();
    }

    public function afterValidate()
    {
        return parent::afterValidate();
    }

    /**
     * Process the email queue
     * This functions should be run by console command
     */
    public function processQueues()
    {
        $queueList = SysJobQueue::model()->getQueue(SysJobs::JOB_SEND_EMAIL);
        foreach ($queueList as $queue) {
            $this->processQueue($queue);
        }
    }

    /**
     * Process an email
     * @param array $queue
     */
    public function processQueue($queue)
    {
        $params = unserialize($queue['params']);
        $result = $this->sendEmail($params, $queue['date_created']);
        if (!$result && ($queue['attempts'] < $queue['max_attempts'])) {
            SysJobQueue::model()->incrementAttempts($queue);
        }
    }

    /**
     *  Send email using {@link SwiftMailer} extension
     * @param array $params
     */
    public function sendEmail(array $params, $date_queued = NULL)
    {
        $mailer = Yii::app()->mailer;
        //set properties
        $settings = Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_EMAIL, array(
            SettingsModuleConstants::SETTINGS_EMAIL_MAILER,
            SettingsModuleConstants::SETTINGS_EMAIL_HOST,
            SettingsModuleConstants::SETTINGS_EMAIL_PORT,
            SettingsModuleConstants::SETTINGS_EMAIL_USERNAME,
            SettingsModuleConstants::SETTINGS_EMAIL_PASSWORD,
            SettingsModuleConstants::SETTINGS_EMAIL_SECURITY,
            SettingsModuleConstants::SETTINGS_EMAIL_SENDMAIL_COMMAND,
        ));
        $mailer->mailer = $settings[SettingsModuleConstants::SETTINGS_EMAIL_MAILER];

        if ($mailer->mailer === 'smtp') {
            $mailer->host = $settings[SettingsModuleConstants::SETTINGS_EMAIL_HOST];
            $mailer->port = (int) $settings[SettingsModuleConstants::SETTINGS_EMAIL_PORT];
            $mailer->username = $settings[SettingsModuleConstants::SETTINGS_EMAIL_USERNAME];
            $mailer->password = $settings[SettingsModuleConstants::SETTINGS_EMAIL_PASSWORD];
            $mailer->security = $settings[SettingsModuleConstants::SETTINGS_EMAIL_SECURITY];
        } else if ($mailer->mailer === 'sendmail') {
            $mailer->sendmailCommand = $settings[SettingsModuleConstants::SETTINGS_EMAIL_SENDMAIL_COMMAND];
        }
        $mailer->Subject = $params['subject'];
        $mailer->From = array($params['from_email'] => $params['from_name']);
        $result = $mailer
                ->AddAddresses(explode(',', $params['to_email']))
                ->MsgHTML($params['message'])
                //->AddFiles($email['attachments'])
                ->Send();
        if ($result) {
            Yii::app()->db->createCommand()
                    ->insert($this->tableName(), array(
                        'sent_by' => !empty($params['sent_by']) ? $params['sent_by'] : NULL,
                        'from_name' => $params['from_name'],
                        'from_email' => $params['from_email'],
                        'subject' => $params['subject'],
                        'message' => $params['message'],
                        'date_queued' => $date_queued,
                        'date_created' => new CDbExpression('NOW()'),
            ));
        }
        return $result;
    }

    public function push($params)
    {
        if (Yii::app()->settings->get(SettingsModuleConstants::SETTINGS_EMAIL, SettingsModuleConstants::SETTINGS_EMAIL_SENDING_METHOD, self::EMAIL_SENDING_METHOD_QUEUED) === self::EMAIL_SENDING_METHOD_DIRECT)
            $this->sendEmail($params);
        else
            SysJobQueue::model()->push(SysJobs::JOB_SEND_EMAIL, $params);
    }

    public static function emailSendingMethods()
    {
        return array(
            self::EMAIL_SENDING_METHOD_DIRECT => Lang::t(self::EMAIL_SENDING_METHOD_DIRECT),
            self::EMAIL_SENDING_METHOD_QUEUED => Lang::t(self::EMAIL_SENDING_METHOD_QUEUED),
        );
    }

}

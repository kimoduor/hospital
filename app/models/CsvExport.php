<?php

/**
 * aunthor mutuvasinai@gmail.com
 * This class gets the local mysql connection
 * String based on the Yii::app()->db
 */
class CsvExport extends CActiveRecord {

    public static function ConnectionString() {
        $config = Yii::app()->db;
        $connection_string = ($config->connectionString);
        $exploded = explode('=', $connection_string);
        $database = $exploded[3];
        $host = explode(';', $exploded[1]);
        $host = $host[0];
        $username = $config->username;
        $password = $config->password;
                $mysqli = new mysqli($host, $username, $password, $database);
        return $mysqli;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Suppliersdistrict the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}

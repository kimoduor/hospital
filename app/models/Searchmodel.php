<?php

/**
 * This is the model class for table "tblrmphdacropsales".
 *
 * The followings are the available columns in table 'tblrmphdacropsales':
 * @property string $crop_sales_id
 * @property string $ph_mkt_id
 * @property string $crop_planted_id
 * @property double $area_planted
 * @property integer $unit_area_code
 * @property double $qty_harvested
 * @property integer $unit_harvested_id
 * @property integer $crops_sold
 * @property double $qty_sold
 * @property integer $unit_sold_id
 * @property string $month_sold
 * @property integer $buyer_code_id
 * @property string $other_buyer
 * @property integer $sale_code_id
 * @property double $price_per_unit
 * @property integer $unit_code_sold
 * @property double $expected_qty
 * @property integer $unit_expected_id
 * @property string $expected_month
 * @property integer $crop_in_storage
 * @property double $qty_in_storage
 * @property integer $unit_storage_id
 * @property string $run_out_month
 *
 * The followings are the available model relations:
 * @property Tblfrmcropplantedfield $cropPlanted
 * @property Tblrmphsuareacode $unitAreaCode
 * @property Tblrmphsubuyercode $buyerCode
 * @property Tblrmphsuunitcode $unitExpected
 * @property Tblsumonths $monthSold
 * @property Tblsumonths $expectedMonth
 * @property Tblrmphmkt $phMkt
 * @property Tblsumonths $runOutMonth
 * @property Tblrmphsusalescode $saleCode
 * @property Tblrmphsuunitcode $unitCodeSold
 * @property Tblrmphsuunitcode $unitHarvested
 * @property Tblrmphsuunitcode $unitSold
 * @property Tblrmphsuunitcode $unitStorage
 */
class Tblrmphdacropsales extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'tblrmphdacropsales';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ph_mkt_id, crop_planted_id', 'required'),
            array('unit_area_code, unit_harvested_id, crops_sold, unit_sold_id, buyer_code_id, sale_code_id, unit_code_sold, unit_expected_id, crop_in_storage, unit_storage_id', 'numerical', 'integerOnly' => true),
            array('area_planted, qty_harvested, qty_sold, price_per_unit, expected_qty, qty_in_storage', 'numerical'),
            array('ph_mkt_id, crop_planted_id', 'length', 'max' => 11),
            array('month_sold, expected_month, run_out_month', 'length', 'max' => 2),
            array('other_buyer', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('crop_sales_id, ph_mkt_id, crop_planted_id, area_planted, unit_area_code, qty_harvested, unit_harvested_id, crops_sold, qty_sold, unit_sold_id, month_sold, buyer_code_id, other_buyer, sale_code_id, price_per_unit, unit_code_sold, expected_qty, unit_expected_id, expected_month, crop_in_storage, qty_in_storage, unit_storage_id, run_out_month', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'cropPlanted' => array(self::BELONGS_TO, 'Tblfrmcropplantedfield', 'crop_planted_id'),
            'unitAreaCode' => array(self::BELONGS_TO, 'Tblrmphsuareacode', 'unit_area_code'),
            'buyerCode' => array(self::BELONGS_TO, 'Tblrmphsubuyercode', 'buyer_code_id'),
            'unitExpected' => array(self::BELONGS_TO, 'Tblrmphsuunitcode', 'unit_expected_id'),
            'monthSold' => array(self::BELONGS_TO, 'Tblsumonths', 'month_sold'),
            'expectedMonth' => array(self::BELONGS_TO, 'Tblsumonths', 'expected_month'),
            'phMkt' => array(self::BELONGS_TO, 'Tblrmphmkt', 'ph_mkt_id'),
            'runOutMonth' => array(self::BELONGS_TO, 'Tblsumonths', 'run_out_month'),
            'saleCode' => array(self::BELONGS_TO, 'Tblrmphsusalescode', 'sale_code_id'),
            'unitCodeSold' => array(self::BELONGS_TO, 'Tblrmphsuunitcode', 'unit_code_sold'),
            'unitHarvested' => array(self::BELONGS_TO, 'Tblrmphsuunitcode', 'unit_harvested_id'),
            'unitSold' => array(self::BELONGS_TO, 'Tblrmphsuunitcode', 'unit_sold_id'),
            'unitStorage' => array(self::BELONGS_TO, 'Tblrmphsuunitcode', 'unit_storage_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'crop_sales_id' => 'Crop Sales',
            'ph_mkt_id' => 'Ph Mkt',
            'crop_planted_id' => 'Crop Planted',
            'area_planted' => 'Area Planted',
            'unit_area_code' => 'Unit Area Code',
            'qty_harvested' => 'Qty Harvested',
            'unit_harvested_id' => 'Unit Harvested',
            'crops_sold' => 'Crops Sold',
            'qty_sold' => 'Qty Sold',
            'unit_sold_id' => 'Unit Sold',
            'month_sold' => 'Month Sold',
            'buyer_code_id' => 'Buyer Code',
            'other_buyer' => 'Other Buyer',
            'sale_code_id' => 'Sale Code',
            'price_per_unit' => 'Price Per Unit',
            'unit_code_sold' => 'Unit Code Sold',
            'expected_qty' => 'Expected Qty',
            'unit_expected_id' => 'Unit Expected',
            'expected_month' => 'Expected Month',
            'crop_in_storage' => 'Crop In Storage',
            'qty_in_storage' => 'Qty In Storage',
            'unit_storage_id' => 'Unit Storage',
            'run_out_month' => 'Run Out Month',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('crop_sales_id', $this->crop_sales_id, true);
        $criteria->compare('ph_mkt_id', $this->ph_mkt_id, true);
        $criteria->compare('crop_planted_id', $this->crop_planted_id, true);
        $criteria->compare('area_planted', $this->area_planted);
        $criteria->compare('unit_area_code', $this->unit_area_code);
        $criteria->compare('qty_harvested', $this->qty_harvested);
        $criteria->compare('unit_harvested_id', $this->unit_harvested_id);
        $criteria->compare('crops_sold', $this->crops_sold);
        $criteria->compare('qty_sold', $this->qty_sold);
        $criteria->compare('unit_sold_id', $this->unit_sold_id);
        $criteria->compare('month_sold', $this->month_sold, true);
        $criteria->compare('buyer_code_id', $this->buyer_code_id);
        $criteria->compare('other_buyer', $this->other_buyer, true);
        $criteria->compare('sale_code_id', $this->sale_code_id);
        $criteria->compare('price_per_unit', $this->price_per_unit);
        $criteria->compare('unit_code_sold', $this->unit_code_sold);
        $criteria->compare('expected_qty', $this->expected_qty);
        $criteria->compare('unit_expected_id', $this->unit_expected_id);
        $criteria->compare('expected_month', $this->expected_month, true);
        $criteria->compare('crop_in_storage', $this->crop_in_storage);
        $criteria->compare('qty_in_storage', $this->qty_in_storage);
        $criteria->compare('unit_storage_id', $this->unit_storage_id);
        $criteria->compare('run_out_month', $this->run_out_month, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Tblrmphdacropsales the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public static function getfieldvalues($attribute, $ph_mkt_id, $crop_id, $field = 2) {
        $crop_planted_id = Tblfrmcropplantedfield::model()->getScalar('crop_planted_id', "crop_id=$crop_id AND field_type_id=$field");
        $objvalues = '';
        if ($crop_planted_id) {
            $objvalues = Tblrmphdacropsales::model()->findAll("ph_mkt_id=$ph_mkt_id AND crop_planted_id=$crop_planted_id");
        }
        if (is_array($objvalues)) {
            foreach ($objvalues as $results) {
                return $results->$attribute;
            }
        } else {
            return '';
        }
    }

    public static function getdisabled($attribute, $ph_mkt_id, $input_type_id) {
        //  $input_acq_id = Tblld::model()->getScalar('crop_planted_id', "crop_id=$crop_id AND field_type_id=$field");
        $objvalues = '';
        $objvalues = Tblrmphdainputacquisition::model()->findAll("ph_mkt_id=$ph_mkt_id AND input_type_id=$input_type_id");
        if (is_array($objvalues)) {
            foreach ($objvalues as $results) {
                return $results->$attribute;
            }
        } else {
            return '';
        }
    }

}

<?php

/**
 * This is the model class for table "tblprombdm".
 *
 * The followings are the available columns in table 'tblprombdm':
 * @property string $bdm_id
 * @property string $fname
 * @property string $lname
 * @property string $mobile
 * @property string $email
 * @property string $region_id
 * @property string $date_created
 * @property integer $created_by
 *
 * The followings are the available model relations:
 * @property Regions $region
 * @property Promotionbdm[] $promotionbdms
 */
class Prombdm extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tblprombdm';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fname, lname, region_id', 'required'),
			array('created_by', 'numerical', 'integerOnly'=>true),
			array('fname, lname, mobile, email', 'length', 'max'=>255),
			array('region_id', 'length', 'max'=>11),
			array('date_created', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bdm_id, fname, lname, mobile, email, region_id, date_created, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'region' => array(self::BELONGS_TO, 'Regions', 'region_id'),
			'promotionbdms' => array(self::HAS_MANY, 'Promotionbdm', 'bdm_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bdm_id' => 'Bdm',
			'fname' => 'Fname',
			'lname' => 'Lname',
			'mobile' => 'Mobile',
			'email' => 'Email',
			'region_id' => 'Region',
			'date_created' => 'Date Created',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bdm_id',$this->bdm_id,true);
		$criteria->compare('fname',$this->fname,true);
		$criteria->compare('lname',$this->lname,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('region_id',$this->region_id,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('created_by',$this->created_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Prombdm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

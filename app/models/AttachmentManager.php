<?php

/**
 *  AttachmentManager class. All models for attachment should inherit this class
 * @property string $id
 * @property string $title
 * @property string $description
 * @property string $file
 * @property string $mime_type
 * @property string $asset_id
 * @property string $date_created
 * @property string $created_by
 *
 * @author Joakim <kimoduor@gmail.com>
 */
abstract class AttachmentManager extends ActiveRecord
{

    /**
     *
     * @var type
     */
    public $temp_file;

    /**
     * The route to redirect to after saving the address.
     */
    const AFTERSAVE_GO_TO_ROUTE_GET_PARAM = 'route';

    /**
     *
     */
    const ATTACHMENT_ID_GET_PARAM = 'file_id';

    /**
     * Parent id field of the owner class of the address
     * @var type
     */
    public $parent_id_field;

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('description', 'length', 'max' => 255),
            array('mime_type', 'length', 'max' => 128),
            array('temp_file', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'title' => Lang::t('Title'),
            'description' => Lang::t('Description'),
            'file' => Lang::t('File'),
        );
    }

    public function beforeSave()
    {
        $this->setFile();
        return parent::beforeSave();
    }

    public function afterDelete()
    {
        $dir = $this->getDir();
        $file = $dir . DS . $this->file;
        if (file_exists($file))
            @unlink($file);

        return parent::afterDelete();
    }

    protected function setFile()
    {
        //using fineuploader
        if (!empty($this->temp_file)) {
            $file_name = $file_name = basename($this->temp_file);
            $temp_dir = dirname($this->temp_file);

            $new_path = $this->getDir() . DS . $file_name;
            if (copy($this->temp_file, $new_path)) {
                $this->mime_type = Common::getFileMimeType($this->temp_file);
                if (!empty($temp_dir))
                    Common::deleteDir($temp_dir);

                $this->file = $file_name;
                $this->temp_file = null;
            }
        }
    }

    /**
     * Get file path
     * @return type
     */
    public function getFilePath()
    {
        return $this->getDir() . DS . $this->file;
    }

    /**
     * Check if file exists
     * @return type
     */
    public function fileExists()
    {
        $file = $this->getFilePath();
        return file_exists($file);
    }

}

interface IAttachmentManager
{

    /**
     * Get the directory of the attachment
     * @param type $parent_id
     */
    public function getDir($parent_id = null);
}

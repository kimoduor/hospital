<?php

class Details {

    public static function record($id) {
        switch ($id) {
            case 0:
                return CHtml::tag("span", array("class" => "label label-danger"), "NO");
                break;
            case 1:
                return CHtml::tag("span", array("class" => "label label-success"), "YES");
                break;
        }
    }

    public static function status($id) {
        switch ($id) {
            case 0:
                return CHtml::tag("span", array("class" => "label label-danger"), "Inactive");
                break;
            case 1:
                return CHtml::tag("span", array("class" => "label label-success"), "Active");
                break;
        }
    }

    public static function sync($id) {
        switch ($id) {
            case 0:
                return CHtml::tag("span", array("class" => "label label-success"), "Syncronised");
                break;
            case 1:
                return CHtml::tag("span", array("class" => "label label-success"), "Not Synchronised");
                break;
        }
    }

    public static function getFarmerRegistrationstatus($registration_status_id) {
        // return $registration_status_id;
        if ($registration_status_id == '0') {
            return CHtml::tag("span", array("class" => "label label-info"), "Waiting for approval");
        } elseif ($registration_status_id == '1') {
            return CHtml::tag("span", array("class" => "label label-success"), "Approved");
        } elseif ($registration_status_id == '2') {
            return CHtml::tag("span", array("class" => "label label-danger"), "Disapproved");
        } elseif ($registration_status_id == '3') {
            return CHtml::tag("span", array("class" => "label label-warning"), "District Registration");
        }
    }

    public static function distributorname($distributor_id) {
        return Distributors::model()->get($distributor_id, "CONCAT(fname,' ',lname)");
    }

    public static function subdistributorname($subdistributor_id) {

        return Subdistributors::model()->get($subdistributor_id, "CONCAT(fname,' ',lname)");
    }

    public static function resellername($reseller_id) {

        return Resellers::model()->get($reseller_id, "CONCAT(fname,' ',lname)");
    }

    /**
     * multi-purpose function to calculate the time elapsed between $start and optional $end
     * @param string|null $start the date string to start calculation
     * @param string|null $end the date string to end calculation
     * @param string $suffix the suffix string to include in the calculated string
     * @param string $format the format of the resulting date if limit is reached or no periods were found
     * @param string $separator the separator between periods to use when filter is not true
     * @param null|string $limit date string to stop calculations on and display the date if reached - ex: 1 month
     * @param bool|array $filter false to display all periods, true to display first period matching the minimum, or array of periods to display ['year', 'month']
     * @param int $minimum the minimum value needed to include a period
     * @return string
     */
    public static function elapsedTimeString($start, $end = null, $limit = null, $filter = true, $suffix = 'ago', $format = 'Y-m-d', $separator = ' ', $minimum = 1) {
        $dates = (object) array(
                    'start' => new DateTime($start ? : 'now'),
                    'end' => new DateTime($end ? : 'now'),
                    'intervals' => array('y' => 'year', 'm' => 'month', 'd' => 'day', 'h' => 'hour', 'i' => 'minute', 's' => 'second'),
                    'periods' => array()
        );
        $elapsed = (object) array(
                    'interval' => $dates->start->diff($dates->end),
                    'unknown' => 'unknown'
        );
        if ($elapsed->interval->invert === 1) {
            return trim('0 seconds ' . $suffix);
        }
        if (false === empty($limit)) {
            $dates->limit = new DateTime($limit);
            if (date_create()->add($elapsed->interval) > $dates->limit) {
                return $dates->start->format($format) ? : $elapsed->unknown;
            }
        }
        if (true === is_array($filter)) {
            $dates->intervals = array_intersect($dates->intervals, $filter);
            $filter = false;
        }
        foreach ($dates->intervals as $period => $name) {
            $value = $elapsed->interval->$period;
            if ($value >= $minimum) {
                $dates->periods[] = vsprintf('%1$s %2$s%3$s', array($value, $name, ($value !== 1 ? 's' : '')));
                if (true === $filter) {
                    break;
                }
            }
        }
        if (false === empty($dates->periods)) {
            return trim(vsprintf('%1$s %2$s', array(implode($separator, $dates->periods), $suffix)));
        }

        return $dates->start->format($format) ? : $elapsed->unknown;
    }

    public static function getRegion_id($location_id) {
        return Towns::model()->get(self::getTown_id($location_id), 'region_id');
    }

    public static function getRegion_idfromtown($town_id) {
        return Towns::model()->get($town_id, 'region_id');
    }

    public static function getTown_id($location_id) {
        return Locations::model()->get($location_id, 'town_id');
    }

    public static function getpromotionproduct($promotion_id) {
        return Promotionproducts::model()->get(Promotion::model()->get($promotion_id, 'promotionproduct_id'), 'product');
    }

    public static function getallproductcodes($promotionbdm_id, $model) {
        $counting = $model::model()->count("promotionbdm_id=$promotionbdm_id");
        return CHtml::tag("span", array("class" => "label label-info"), $counting);
    }

    public static function getallproductcodesindistributors($promotiondistributor_id) {
        $counting = Promotiondistributorsproductcode::model()->count("promotiondistributor_id=$promotiondistributor_id");
        return CHtml::tag("span", array("class" => "label label-info"), $counting);
    }
    public static function getallproductcodesinsubdistributors($promotionsubdistributor_id) {
        $counting = Promotionsubdistributorsproductcode::model()->count("promotionsubdistributor_id=$promotionsubdistributor_id");
        return CHtml::tag("span", array("class" => "label label-info"), $counting);
    }
     public static function getallproductcodesinresellers($promotionreseller_id) {
        $counting = Promotionresellersproductcode::model()->count("promotionreseller_id=$promotionreseller_id");
        return CHtml::tag("span", array("class" => "label label-info"), $counting);
    }
    public static function getgiver($code) {
        $firstletter = substr($code, 0, 1);
        switch ($firstletter) {
            case 'D':return $code .  CHtml::tag("span", array("class" => "label label-info"), " Distributor") ;
                break;
            case 'B':return $code .  CHtml::tag("span", array("class" => "label label-warning"), " BDM") ;
                break;
            case 'S':return $code . CHtml::tag("span", array("class" => "label label-success"), " Subdistributor") ;
        }
    }

}

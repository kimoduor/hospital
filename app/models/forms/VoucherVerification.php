<?php

/**
 *
 * @author Joakim <kimoduor@gmail.com>
 */
class VoucherVerification extends FormModel {

    const OZEKI_ENABLED = 1;
    const SCENARIO_SMS = 'sms';
    const SCENARIO_WEB = 'web';

    public $type;

    /**
     *
     * @var type
     */
    public $sender;

    /**
     *
     * @var type
     */
    public $receiver;

    /**
     *
     * @var type
     */
    public $inbox_id;

    /**
     *
     * @var type
     */
    public $nrc_no;

    /**
     *
     * @var type
     */
    public $voucher_no;

    /**
     *
     * @var type
     */
    public $message;

    /**
     *
     * @var type
     */
    public $username;

    /**
     *
     * @var type
     */
    public $user_id;

    /**
     *
     * @var type
     */
    public $agri_camp_id;

    /**
     *
     * @var type
     */
    public $block_id;

    /**
     *
     * @var type
     */
    public $sms_received;
    //warehouse agent
    /**
     *
     * @var type
     */
    public $agent_mobile;

    /**
     *
     * @var type
     */
    public $agent_message;

    public function init() {
        parent::init();
    }

    public function rules() {
        return array(
            array('nrc_no,voucher_no', 'filter', 'filter' => 'trim'),
            array('voucher_no', 'validateSeason'),
            array('sms_received', 'validateSMSMsg', 'on' => self::SCENARIO_SMS),
            // array('nrc_no,voucher_no', 'checkAccountActive', 'on' => self::SCENARIO_WEB),
            array('sender', 'validateMobile', 'on' => self::SCENARIO_SMS),
            array('sender', 'validateAccountStatus', 'on' => self::SCENARIO_SMS),
            array('nrc_no', 'validateNRCNo'),
            array('voucher_no', 'validateVoucherNo'),
            array('voucher_no', 'redeemVoucher'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'nrc_no' => Lang::t('NRC Number'),
            'voucher_no' => Lang::t('Voucher Number'),
        );
    }

    public function validateSeason() {
        if ($this->hasErrors())
            return false;
        $seasonStatus = Agriseason::model()->getScalar('lock_status', '`active`=:t1', array(':t1' => 1));
        $explodedsms = explode('*', $this->sms_received);
        $explodedsms = strtoupper($explodedsms[0]);
        if ($explodedsms != 'WHA' && $explodedsms != 'MKP' && $explodedsms != 'RESET') {
            if ($seasonStatus == 0) {
                $this->message = Lang::t('Redeeming For vouchers is completed. Call CASU toll free line');
                $this->addError('sms_received', $this->message);
                if ($this->scenario === self::SCENARIO_SMS) {
                    $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
                }
            }
        }
    }

    public function validateSMSMsg() {
        if ($this->hasErrors())
            return false;
        $explodedsms = explode('*', $this->sms_received);
        $explodedsms = strtoupper($explodedsms[0]);
        if ($explodedsms != 'WHA' && $explodedsms != 'MKP' && $explodedsms != 'RESET') {
            if (strpos($this->sms_received, '*') === false) {
                $this->message = Lang::t('Your Message was invalid. Please send a message in the format nrc_no*voucher_no');
                $this->addError('sms_received', $this->message);
                $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
            }
        }
    }

    public function validateMobile() {
        if ($this->hasErrors())
            return false;
        $explodedsms = explode('*', $this->sms_received);
        $explodedsms = strtoupper($explodedsms[0]);
        if ($explodedsms != 'WHA' && $explodedsms != 'MKP' && $explodedsms != 'RESET') {

            if (!Agrodealers::model()->exists('`mobile`=:t1', array(':t1' => $this->sender))) {
                $this->message = $this->unRegisteredMobileErrorMsg();
                $this->addError('sender', $this->message);
                $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
            }
        }
    }

    public function validateAccountStatus() {
        if ($this->hasErrors())
            return false;
        $explodedsms = explode('*', $this->sms_received);
        $explodedsms = strtoupper($explodedsms[0]);
        if ($explodedsms != 'WHA' && $explodedsms != 'MKP' && $explodedsms != 'RESET') {
            $status = Agrodealers::model()->getScalar('status', '`mobile`=:t1', array(':t1' => $this->sender));
            if (empty($status)) {
                $this->message = 'Sorry, your account is inactive  kindly call CASU toll free line for further assistance';
                $this->addError('sender', $this->message);
                $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
            }
        }
    }

    public function validateDistrictMatch() {
        if ($this->hasErrors())
            return false;
        $explodedsms = explode('*', $this->sms_received);
        $explodedsms = strtoupper($explodedsms[0]);
        if ($explodedsms != 'WHA' && $explodedsms != 'MKP' && $explodedsms != 'RESET') {
            if ($this->scenario === self::SCENARIO_SMS) {
                $user_id = Agrodealers::model()->getScalar('user_id', '`mobile`=:t1', array(':t1' => $this->sender));
            } else {
                $user_id = Yii::app()->user->id;
            }
            $this->user_id = $user_id;
            $this->username = Users::model()->get($user_id, 'username');
            $districtfarmer = null;
            $district_id = Agrodealers::model()->getScalar('district_id', '`user_id`=:t1 AND `agrodealer_code`=:t2', array(':t1' => $this->user_id, ':t2' => $this->username));
            $voucher = Vouchers::model()->find('REPLACE(`nrc_no`,"/","")=:t1 AND `voucher_no`=:t2', array(':t1' => $this->nrc_no, ':t2' => $this->voucher_no));
            if ($voucher !== null) {
                $camp = Farmers::model()->getScalar('agri_camp_id', '`farmer_id`=:t1', array(':t1' => $voucher->farmer_id));
                $this->agri_camp_id = $camp;
                $block = Agricamps::model()->getScalar('block_id', '`agri_camp_id`=:t1', array(':t1' => $this->agri_camp_id));
                $this->block_id = $block;
                $districtfarmer = Agriblocks::model()->getScalar('district_id', '`agri_block_id`=:t1', array(':t1' => $this->block_id));
            }
            if ($district_id !== $districtfarmer) {
                $this->message = 'Agro-dealer not registered in this district. Consult your CEO for approved agro-dealers or call CASU toll free line for assistance';
                $this->addError('sender', $this->message);
                $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
                return true;
            }
        }
    }

    public function validateNRCNo() {
        if ($this->hasErrors())
            return false;
        $explodedsms = explode('*', $this->sms_received);
        $explodedsms = strtoupper($explodedsms[0]);
        if ($explodedsms != 'WHA' && $explodedsms != 'MKP' && $explodedsms != 'RESET') {
            if ($this->scenario === self::SCENARIO_SMS) {
                $user_id = Agrodealers::model()->getScalar('user_id', '`mobile`=:t1', array(':t1' => $this->sender));
            } else {
                $user_id = Yii::app()->user->id;
            }
            $status = Agrodealers::model()->getScalar('status', 'user_id=' . $user_id);
            if ($status != 1) {
                $this->message = 'Sorry, your account is inactive  kindly call CASU toll free line for further assistance';
                $this->addError('nrc_no', $this->message);
                return false;
            }
            $this->nrc_no = str_replace("/", "", $this->nrc_no);
            if (strlen($this->nrc_no) != 9) {
                $this->message = Lang::t('Kindly confirm the number of digits in the {attribute} {value}, the {attribute} should contain exactly 9 digits', array('{attribute}' => $this->getAttributeLabel('nrc_no'), '{value}' => $this->nrc_no));
                $this->addError('nrc_no', $this->message);
                if ($this->scenario === self::SCENARIO_SMS) {
                    $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
                }
            }
        }
    }

    public function validateVoucherNo() {
        if ($this->hasErrors())
            return false;
        $explodedsms = explode('*', $this->sms_received);
        $explodedsms = strtoupper($explodedsms[0]);
        if ($explodedsms != 'WHA' && $explodedsms != 'MKP' && $explodedsms != 'RESET') {
            if ($this->scenario === self::SCENARIO_SMS) {
                $user_id = Agrodealers::model()->getScalar('user_id', '`mobile`=:t1', array(':t1' => $this->sender));
            } else {
                $user_id = Yii::app()->user->id;
            }
            $status = Agrodealers::model()->getScalar('status', 'user_id=' . $user_id);
            if ($status != 1) {
                $this->message = 'Sorry, your account is inactive  kindly call CASU toll free line for further assistance';
                $this->addError('nrc_no', $this->message);
                return false;
            }
            if (strlen($this->voucher_no) != 13) {
                $this->message = Lang::t('Kindly confirm the number of digits in the {attribute} {value}, the {attribute} should contain exactly 13 digits', array('{attribute}' => $this->getAttributeLabel('voucher_no'), '{value}' => $this->voucher_no));
                $this->addError('voucher_no', $this->message);
                if ($this->scenario === self::SCENARIO_SMS) {
                    $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
                }
            }
        }
    }

    public function redeemVoucher() {
        if ($this->hasErrors())
            return false;
        $explodedsms = explode('*', $this->sms_received);
        $explodedsms = strtoupper($explodedsms[0]);
        if ($explodedsms != 'WHA' && $explodedsms != 'MKP' && $explodedsms != 'RESET') {

            if ($this->scenario === self::SCENARIO_SMS) {
                $user_id = Agrodealers::model()->getScalar('user_id', '`mobile`=:t1', array(':t1' => $this->sender));
            } else {
                $user_id = Yii::app()->user->id;
            }

            $this->username = Users::model()->get($user_id, 'username');
            $this->user_id = $user_id;

            $voucher = Vouchers::model()->find('REPLACE(`nrc_no`,"/","")=:t1 AND `voucher_no`=:t2', array(':t1' => $this->nrc_no, ':t2' => $this->voucher_no));
//not found
            if (null === $voucher) {
                $this->message = $this->getNoMatchMsg();
                $this->addError('', $this->message);
                if ($this->scenario === self::SCENARIO_SMS) {
                    $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
                }
                return false;
            }
//check district match
            else if ($this->validateDistrictMatch() === true) {
                $this->validateDistrictMatch();
                return false;
            }

//has been redeemed
            else if ($voucher->voucher_status_id === Vouchers::VOUCHER_STATUS_REEDEMED) {
                $this->validateDistrictMatch();
                $this->message = $this->getHasBeenRedeemedMsg($voucher);
                $this->addError('', $this->message);
                if ($this->scenario === self::SCENARIO_SMS) {
                    $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
                }
                return false;
            }
//voucher has not been approved
            else if ($voucher->voucher_status_id === Vouchers::VOUCHER_STATUS_PENDING_APPROVAL) {
                $this->message = $this->getNotApprovedMsg($voucher);
                $this->addError('', $this->message);
                if ($this->scenario === self::SCENARIO_SMS) {
                    $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
                }
                return false;
            }
//voucher has been cancelled
            else if ($voucher->voucher_status_id === Vouchers::VOUCHER_STATUS_CANCELLED) {
                $this->message = $this->getVoucherCancelledMsg($voucher);
                $this->addError('', $this->message);
                if ($this->scenario === self::SCENARIO_SMS) {
                    $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
                }
                return false;
            }

            $this->message = $this->getSuccessMsg($voucher);
//success
            $voucher->voucher_status_id = Vouchers::VOUCHER_STATUS_REEDEMED;
            $voucher->date_of_redemption = date('Y-m-d');
            $voucher->agrodealer_id = Agrodealers::model()->getScalar('agrodealer_id', '`user_id`=:id', array(':id' => $user_id));
            $voucher->method_of_redemption = $this->scenario === self::SCENARIO_SMS ? Validationaudit::TYPE_SMS : Validationaudit::TYPE_WEB;
            $voucher->save(false);

            if ($this->scenario === self::SCENARIO_SMS) {
                $this->sendSms($this->sender, $this->receiver, $this->message, $this->inbox_id);
            }
        }
    }

    /**
     *
     * @param type $voucher
     * @return \Vouchers
     */
    public function getSuccessMsg($voucher) {
        return Lang::t("The nrc_no {nrc_no} and The voucher no {voucher_no} are valid. Please proceed to redeem voucher. Farmer's name is {farmer}", array('{nrc_no}' => $voucher->nrc_no, '{voucher_no}' => $voucher->voucher_no, '{farmer}' => $voucher->name));
    }

    /**
     *
     * @param type $voucher
     * @return \Vouchers
     */
    protected function getHasBeenRedeemedMsg($voucher) {
        return Lang::t("The nrc_no {nrc_no} and The voucher no {voucher_no} are not valid. The voucher has already been redeemed kindly call the CASU toll free line for further assistance", array('{nrc_no}' => $voucher->nrc_no, '{voucher_no}' => $voucher->voucher_no));
    }

    protected function getNoMatchMsg() {
        return Lang::t('Sorry, your combination for nrc_no {nrc_no} and voucher_no {voucher_no} does not exist. Please check the NRC and Voucher number and try again or call the CASU toll free line for further assistance', array('{nrc_no}' => $this->nrc_no, '{voucher_no}' => $this->voucher_no));
    }

    /**
     *
     * @param type $voucher
     * @return \Vouchers
     */
    protected function getVoucherCancelledMsg($voucher) {
        return Lang::t("The nrc_no {nrc_no} and The voucher no {voucher_no} are not valid. The voucher has been cancelled kindly call the CASU toll free line for further assistance", array('{nrc_no}' => $voucher->nrc_no, '{voucher_no}' => $voucher->voucher_no));
    }

    /**
     *
     * @param type $voucher
     * @return \Vouchers
     */
    protected function getNotApprovedMsg($voucher) {
        return Lang::t("The nrc_no {nrc_no} and The voucher no {voucher_no} are not valid. The voucher has not been approved call the CASU toll free line for further assistance", array('{nrc_no}' => $voucher->nrc_no, '{voucher_no}' => $voucher->voucher_no));
    }

    /**
     *
     * @param type $receiver
     * @param type $sender
     * @param type $message
     * @param type $inbox_id
     * @param type $status
     */
    public function sendSms($receiver, $sender, $message, $inbox_id, $status = 'send') {
//insert outgoing sms
        $randmobile=  Farmers::getsmsnumber();
        
        if($randmobile!=$sender){
        if (self::OZEKI_ENABLED) {
            $response = Yii::app()->db->createCommand()
                    ->insert(Ozekimessageout::model()->tableName(), array(
//                'sender' => $receiver,
//                'receiver' => $sender,
                'sender' => $sender,
                'receiver' => $receiver,
                'msg' => $message,
                'status' => $status,
            ));
        } else {
            $response = Yii::app()->db->createCommand()
                    ->insert(Messages::model()->tableName(), array(
                'Direction' => 2,
                'Type' => 2,
                'StatusDetails' => 201,
                'Status' => 1,
                'ChannelID' => 1001,
                //'Sender' => $receiver,
//'Recipient' => $sender,
                'Sender' => $sender,
                'Recipient' => $receiver,
                'Body' => $message,
                'MessageReference' => $inbox_id,
            ));
        }


        if ($response) {
            if (self::OZEKI_ENABLED) {
                Yii::app()->db->createCommand()
                        ->update(Ozekimessagein::model()->tableName(), array('status' => 1), '`id`=:id', array(':id' => $inbox_id));
            } else {
                Yii::app()->db->createCommand()
                        ->update(Messages::model()->tableName(), array('StatusDetails' => 101), '`ID`=:t1', array(':t1' => $inbox_id));
            }
        }
        }
    }

    public function processSmsInbox() {
        $randmobile = Farmers::getsmsnumber();
        //  if($randmobile=  Constants::RANDMOBILE;)
        $limit = 20;
        // echo "yesssssssss";
        $data = '';
        if (self::OZEKI_ENABLED) {
            //echo "yesssssssss";
            $data = Ozekimessagein::model()->getData('id,receiver,sender,msg', '`status`=0', array(), null, $limit);
        } else {
            $data = Messages::model()->getData('ID,Recipient,Sender,Body', '`StatusDetails`=102', array(), null, $limit);
        }

        if (!empty($data)) {
            $model = new VoucherVerification(self::SCENARIO_SMS);

            foreach ($data as $row) {

                $new_model = clone $model;
                $message = self::OZEKI_ENABLED ? $row['msg'] : $row['Body'];
                $receiver = self::OZEKI_ENABLED ? $row['receiver'] : $row['Recipient'];
                $sender = self::OZEKI_ENABLED ? $row['sender'] : $row['Sender'];
                $inbox_id = self::OZEKI_ENABLED ? $row['id'] : $row['ID'];
                $new_model->sms_received = $message;

                if ($receiver != $randmobile) {

                    if (!empty($message)) {
                        $message_arr = explode('*', $message);
                        $explodedsms = explode('*', $this->sms_received);
                        $agentmessage = strtoupper($message_arr[0]);
                        if ($agentmessage == 'WHA') {
                            //start looking for Agent Sms when exploded the sms shall be in array(WHA,NRC,CP01,05)
                            // if ($message_arr[0] == 'WHA' || $message_arr[0] == 'wha') {//meaning it is the Agent
                            $whaprocess = new Whaprocessor();
                            $whaprocess->processSmswha($row);
                        } elseif ($agentmessage == 'MKP') {
                            //start looking for Agent Sms when exploded the sms shall be in array(MKP,NRC,CP01,05)
                            // if ($message_arr[0] == 'MKP' {//meaning it is the Agent
                            $mkpprocess = new Mkpprocessor();
                            $mkpprocess->processSmsmkp($row);
                        } elseif ($agentmessage == 'RESET') {
                            //start looking for Agent Sms when exploded the sms shall be in array(MKP,NRC,CP01,05)
                            // if ($message_arr[0] == 'MKP' {//meaning it is the Agent
                            $farmerpassword = new Farmerpassword();
                            $farmerpassword->processSmsrest($row);
                        } else {//meaning not Agent of the warehouse
                            $new_model->nrc_no = $message_arr[0];
                            $new_model->voucher_no = isset($message_arr[1]) ? $message_arr[1] : null;
                            $new_model->receiver = $receiver;
                            $new_model->sender = $sender;
                            $new_model->inbox_id = $inbox_id;
                        }
                    } else if (empty($message)) {
                        //$message_arr = explode('*', $message);
                        $new_model->nrc_no = NULL;
                        $new_model->voucher_no = null;
                        $new_model->receiver = $receiver;
                        $new_model->sender = $sender;
                        $new_model->inbox_id = $inbox_id;
                    }

                    $new_model->validate();
                }
            }
        }
    }

    protected function unRegisteredMobileErrorMsg() {
        return Lang::t('Sorry, your mobile number is not registered with us kindly call CASU toll free line for further assistance');
    }

    public function afterValidate() {
        $this->addAudit();
        return parent::afterValidate();
    }

    protected function addAudit() {

        $success = $this->hasErrors() ? 0 : 1;
        $model = new Validationaudit();
        $model->ip_address = $this->scenario !== self::SCENARIO_SMS ? Common::getIp() : null;
        $model->type = $this->scenario === self::SCENARIO_SMS ? Validationaudit::TYPE_SMS : Validationaudit::TYPE_WEB;

        if ($this->scenario === self::SCENARIO_SMS) {
            $user_id = Agrodealers::model()->getScalar('user_id', '`mobile`=:t1', array(':t1' => $this->sender));
        } else {
            $user_id = Yii::app()->user->id;
        }
        $model->user_id = $user_id;
        $model->username = Users::model()->get($model->user_id, 'username');

        $model->nrc_no = $this->nrc_no;
        $model->voucher_no = $this->voucher_no;
        $model->mobile = $this->sender;
        $model->sms_received = $this->sms_received;
        $model->response_given = $this->message;
        $model->success = $success;

        $model->save(false);
    }

    public static function warehouseagentmobile($mobile_num) {
        // $agent=new Tblwhagent();
        return Tblwhagent::model()->getScalar('*', 'mobile=' . $mobile_num);
    }

}

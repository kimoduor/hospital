<?php

/**
 *
 * @author Admin <kimoduor@gmail.com>
 */
class Whaprocessor extends FormModel {

    const OZEKI_ENABLED = 1;
    const SCENARIO_SMS = 'sms';

    public $message;

    public function processSmswha($row) {
        $limit = 20;
        $model = new Whaprocessor();
        $message = self::OZEKI_ENABLED ? $row['msg'] : $row['Body'];
        $receiver = self::OZEKI_ENABLED ? $row['receiver'] : $row['Recipient'];
        $sender = self::OZEKI_ENABLED ? $row['sender'] : $row['Sender'];
        $inbox_id = self::OZEKI_ENABLED ? $row['id'] : $row['ID'];
        $message_arr = explode('*', $message);
        $mymsg = $message_arr;
        if (!self::warehouseagentmobile($sender)) {
            $this->message = "Sorry you're not registered as an aggregating agent, kindly contact CASU";
            $this->saveaudit($message_arr, $sender, $message, $this->message);
        } elseif (self::warehouseagentmobile($sender)) {//meaning  the agent is also valid
            $this->message = 'Agent Accepted.Checking the request...';
            $this->saveaudit($message_arr, $sender, $message, $this->message);
            //Tblwhsmsaudit::model()->updateByPk($id, array('updated' => '1'));
        }
        $auditdatas = Tblwhsmsaudit::model()->getData('wh_sms_id,nrc_no,wh_input_id,quantity,mobile', '`success`=0 AND updated=0', array(), null, $limit);
        foreach ($auditdatas as $auditdata) {
            //start checking if the input_id is valid Tblwhsmsaudit   Tblwhaggregatedinputs
            $wh_input_id = $auditdata['wh_input_id'];
            $nrc_no = $auditdata['nrc_no'];
            $errorchecker = FALSE;
            $newmessage = '';
            $wh_sms_id = $auditdata['wh_sms_id'];

            $check = Tblwhinput::model()->getScalar('wh_input_id', "wh_input_id='$wh_input_id'");
            // var_dump(count($mymsg));
            if (!self::warehouseagentmobile($sender)) {
                $newmessage = "Sorry you're not registered as an aggregating agent, kindly contact CASU";
                $errorchecker = TRUE;
                //$this->sendSms($sender, $receiver, $this->message, $inbox_id, 'send');
            } elseif (count($mymsg) != 4) {
                $newmessage = "Your Message was invalid. Please send a message in the format WHA*NRC*input-code*qty";
                $errorchecker = TRUE;
            } elseif ($this->checknrcnumber($nrc_no)) {//authenticate the nrc number
                $newmessage = $this->checknrcnumber($nrc_no);
                $errorchecker = TRUE;
            } elseif ($this->checkagentactive($auditdata['mobile'])) {//authenticate the agent if he is active
                $newmessage = $this->checkagentactive($auditdata['mobile']);
                $errorchecker = TRUE;
            } elseif (!$check) {//meaning there is no such input id send the message to the warehouseagent user
                $newmessage = 'Sorry, the Input Code you are Using is Invalid.Kindly call CASU toll free line for further assistance';
                $errorchecker = TRUE;
            } elseif ($this->checkinputactive($wh_input_id)) {//authenticate the input if he is active
                $newmessage = $this->checkinputactive($wh_input_id);
                $errorchecker = TRUE;
            }



            if ($errorchecker == true) {
                $this->sendSms($auditdata['mobile'], $receiver, $newmessage, $inbox_id);

                Tblwhsmsaudit::model()->updateByPk($wh_sms_id, array('updated' => '1', 'response_given' => $newmessage));
            } else {//when the checker turns to be false throughout
                $agent_id = isset($auditdata['mobile']) ? Tblwhagent::model()->getScalar('wh_agent_id', 'mobile=' . $auditdata['mobile']) : '';
                $newmodel = new Tblwhaggregatedinputs();
                $newmodel->wh_agent_id = $agent_id;
                $newmodel->wh_sms_id = $auditdata['wh_sms_id'];
                $newmodel->nrc_no = $auditdata['nrc_no'];
                $newmodel->wh_input_id = $wh_input_id;
                $newmodel->quantity = $auditdata['quantity'];
                $newmodel->save();
                $newmessage = Lang::t('You have successfully aggregated {quantity} units for {input} for {nrc_no}', array('{quantity}' => $newmodel->quantity, '{input}' => $newmodel->wh_input_id, '{nrc_no}' => $newmodel->nrc_no));
                if (isset($auditdata['wh_sms_id'])) {
                    $id = $auditdata['wh_sms_id'];
                    Tblwhsmsaudit::model()->updateByPk($id, array('updated' => '1', 'success' => 1, 'response_given' => 'successfully_procesed'));
                }
                $this->sendSms($auditdata['mobile'], $sender, $newmessage, $inbox_id);
            }
        }
    }

    public static function warehouseagentmobile($mobile_num) {
        // $agent=new Tblwhagent();
        return Tblwhagent::model()->getScalar('*', 'mobile=' . $mobile_num);
    }

    public function sendSms($receiver, $sender, $message, $inbox_id, $status = 'send') {
        $response = Yii::app()->db->createCommand()
                ->insert(Ozekimessageout::model()->tableName(), array(
            'sender' => $sender,
            'receiver' => $receiver,
            'msg' => $message,
            'status' => $status,
        ));
        if ($response) {
            if (self::OZEKI_ENABLED) {
                Yii::app()->db->createCommand()
                        ->update(Ozekimessagein::model()->tableName(), array('status' => 1), '`id`=:id', array(':id' => $inbox_id));
            }
        }
    }

    public function saveaudit($message_arr = null, $sender = null, $message = null, $response = null) {
        $auditware = new Tblwhsmsaudit();
        $auditware->mobile = $sender;
        if (count($message_arr == 4)) {
            $auditware->nrc_no = isset($message_arr[1]) ? $message_arr[1] : '';
            $auditware->wh_input_id = isset($message_arr[2]) ? $message_arr[2] : '';
            $auditware->quantity = isset($message_arr[3]) ? $message_arr[3] : '';
            //$auditware->success = 1;
        }
        $auditware->sms_received = $message;
        $auditware->response_given = $response;
        $auditware->save();
    }

    public function checknrcnumber($nrc) {
        $nrc_no = str_replace("/", "", $nrc);

        if (strlen($nrc_no) != 9 || !is_numeric($nrc_no)) {
            return $message = Lang::t('Kindly confirm the  digits in the {attribute} {value}, the {attribute} should contain exactly 9 digits', array('{attribute}' => 'nrc_no', '{value}' => $nrc));
        } else {
            return false;
        }
    }

    public function checkagentactive($mobile) {
        $active = Tblwhagent::model()->getScalar('active', 'mobile=' . $mobile);
        if ($active != 1) {
            return $message = Lang::t("Sorry, your account is inactive  kindly call CASU toll free line for further assistance");
        } else {
            return false;
        }
    }

    public function checkinputactive($wh_input_id) {
        $check = Tblwhinput::model()->getScalar('active', "wh_input_id like '$wh_input_id'");
        if ($check != 1) {
            return $message = Lang::t("Sorry, the input is inactive  kindly call CASU toll free line for further assistance");
        } else {
            return false;
        }
    }

}

<?php

/**
 *
 * @author Admin <kimoduor@gmail.com>
 */
class Farmerpassword extends FormModel {

    const OZEKI_ENABLED = 1;
    const SCENARIO_SMS = 'sms';

    public $message;

    public function processSmsrest($row) {
        $limit = 20;

        $model = new Farmerpassword();
        $message = self::OZEKI_ENABLED ? $row['msg'] : $row['Body'];
        $receiver = self::OZEKI_ENABLED ? $row['receiver'] : $row['Recipient'];
        $sender = self::OZEKI_ENABLED ? $row['sender'] : $row['Sender'];
        $inbox_id = self::OZEKI_ENABLED ? $row['id'] : $row['ID'];
        $message_arr = explode('*', $message);
        $errorchecker = FALSE;
        $newmessage = '';

//rem reset $message_arr[0] farmerno $message_arr[1] nrc $message_arr[2]
        if (!self::farmermobile($sender)) {
            $this->message = "Sorry you're not registered as a Farmer, kindly contact CASU";
            $newmessage = "Sorry you're not registered as a Farmer, kindly contact CASU";
            $errorchecker = TRUE;
        }
//step 1 check farmer no--
        elseif ($this->checkfarmerno($message_arr[1])) {
            $newmessage = $this->checkfarmerno($message_arr[1]);
            $errorchecker = TRUE;
        }
//step 2 check nrc no--
        elseif ($this->checknrcno($message_arr[2], $message_arr[1])) {
            $newmessage = $this->checknrcno($message_arr[2], $message_arr[1]);
            $errorchecker = TRUE;
        }
//step 3 check the formart of the message 
        elseif (count($message_arr) != 3) {
            $newmessage = "Your Message was invalid. Please send a message in the format RESET*FARMER_NO*NRC";
            $errorchecker = TRUE;
        }
//step 4 check if farmer has a card and only check the active card//that is the one that shall be reset
        elseif ($this->farmercard($message_arr[1])) {
            $newmessage = $this->farmercard($message_arr[1]);
            $errorchecker = TRUE;
        }
//step 5 check if the farmer is available farmer status
        elseif ($this->farmeractive($message_arr[1])) {
            $newmessage = $this->farmeractive($message_arr[1]);
            $errorchecker = TRUE;
        }
        if ($errorchecker == true) {
            $this->sendSms($sender, $receiver, $newmessage, $inbox_id);
        } else {//when the checker turns to be false throughout so reset the password and send
            $farmer_id = Farmers::model()->getScalar('farmer_id', "farmer_no='$message_arr[1]'");
            $smart_card_id = Possmartcards::model()->getScalar('smart_card_id', "farmer_id='$farmer_id'  AND card_status=1");
            $model = Possmartcards::model()->loadModel($smart_card_id);
            $newpin = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
            $model->farmer_pin = md5(date('H i')) . md5($newpin);
            $model->salt = md5(date('H i'));
            $model->save(false);
            $newmessage = Lang::t('You have successfully changed your pin.Your New Farmer PIN is {pin}', array('{pin}' => $newpin));
            $this->sendSms($sender, $receiver, $newmessage, $inbox_id);
        }
    }

    public static function farmermobile($mobile_num) {
        return Farmers::model()->getScalar('*', 'mobile=' . $mobile_num);
    }

    public function checkfarmerno($farmerno) {
        $check = Farmers::model()->getScalar('farmer_no', "farmer_no = '$farmerno'");
        if (!$check) {
            return $message = Lang::t("Sorry, the farmer Number Cannot be Found, kindly contact CASU for Assitance");
        } else {
            return false;
        }
    }

    public function checknrcno($nrcno, $farmerno) {
        $check = Farmers::model()->getScalar('farmer_no', "farmer_no = '$farmerno' AND nrc_no LIKE '$nrcno' ");
        $nrc_no = str_replace("/", "", $nrcno);
        if (strlen($nrc_no) != 9 || !is_numeric($nrc_no)) {
            return $message = Lang::t('Kindly confirm the  digits in the {attribute} {value}, the {attribute} should contain exactly 9 digits', array('{attribute}' => 'nrc_no', '{value}' => $nrcno));
        } elseif (!$check) {
            return $message = Lang::t("Sorry, the Nrc Number is Invalid, kindly contact CASU for Assitance");
        } else {
            return false;
        }
    }

    public function farmercard($farmerno) {
        $farmer_id = Farmers::model()->getScalar('farmer_id', "farmer_no='$farmerno'");
        $smart_card_id = Possmartcards::model()->getScalar('smart_card_id', "farmer_id='$farmer_id'  AND card_status=1");

        if (!$smart_card_id) {
            return $message = Lang::t("Sorry, the Card Number is Invalid, kindly contact CASU for Assitance");
        } else {
            return false;
        }
    }

    public function farmeractive($farmerno) {
        $check = Farmers::model()->getScalar('farmer_id', "farmer_no='$farmerno'");
        if (!$check) {
            return $message = Lang::t("Sorry, your Account as a Farmer is Incative, kindly contact CASU for Assitance");
        } else {
            return false;
        }
    }

    public function sendSms($receiver, $sender, $message, $inbox_id, $status = 'send') {
        $response = Yii::app()->db->createCommand()
                ->insert(Ozekimessageout::model()->tableName(), array(
            'sender' => $sender,
            'receiver' => $receiver,
            'msg' => $message,
            'status' => $status,
        ));
        if ($response) {
            if (self::OZEKI_ENABLED) {
                Yii::app()->db->createCommand()
                        ->update(Ozekimessagein::model()->tableName(), array('status' => 1), '`id`=:id', array(':id' => $inbox_id));
            }
        }
    }

}

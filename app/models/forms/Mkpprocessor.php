<?php

/**
 *
 * @author Admin <kimoduor@gmail.com>
 */
class Mkpprocessor extends FormModel {

    const OZEKI_ENABLED = 1;
    const SCENARIO_SMS = 'sms';

    public $message;

    public function processSmsmkp($row) {
        $limit = 20;

        $model = new Mkpprocessor();
        $message = self::OZEKI_ENABLED ? $row['msg'] : $row['Body'];
        $receiver = self::OZEKI_ENABLED ? $row['receiver'] : $row['Recipient'];
        $sender = self::OZEKI_ENABLED ? $row['sender'] : $row['Sender'];
        $inbox_id = self::OZEKI_ENABLED ? $row['id'] : $row['ID'];
        $message_arr = explode('*', $message);
        $mymsg = $message_arr;
        if (!self::marketagentmobile($sender)) {
            $this->message = "Sorry you're not registered as a Market Price Agent, kindly contact CASU";
            $this->saveaudit($message_arr, $sender, $message, $this->message);
        } elseif (self::marketagentmobile($sender)) {//meaning  the agent is also valid
            $this->message = 'Agent Accepted.Checking the request...';
            $this->saveaudit($message_arr, $sender, $message, $this->message);
        }
        $auditdatas = Tblmkpsmsaudit::model()->getData('mkp_sms_id,mkt_code,mkp_input_id,price,mobile', '`success`=0 AND updated=0', array(), null, $limit);
        foreach ($auditdatas as $auditdata) {
            //start checking if the input_id is valid Tblwhsmsaudit   Tblwhaggregatedinputs
            $mkp_input_id = $auditdata['mkp_input_id'];
            $mkt_code = $auditdata['mkt_code'];
            $errorchecker = FALSE;
            $newmessage = '';
            $mkp_sms_id = $auditdata['mkp_sms_id'];

            $check = Tblmkpinput::model()->getScalar('mkp_input_id', "mkp_input_id='$mkp_input_id'");
            if (!self::marketagentmobile($sender)) {
                $newmessage = "Sorry you're not registered as a Market Price Agent, kindly contact CASU";
                $errorchecker = TRUE;
            } elseif (count($mymsg) != 4) {
                $newmessage = "Your Message was invalid. Please send a message in the format MKP*Mkt-Code*Product*Price";
                $errorchecker = TRUE;
            } elseif ($this->checkmktcode($mkt_code)) {//authenticate the market code
                $newmessage = $this->checkmktcode($mkt_code);
                $errorchecker = TRUE;
            } elseif ($this->checkagentactive($auditdata['mobile'])) {//authenticate the agent if he is active
                $newmessage = $this->checkagentactive($auditdata['mobile']);
                $errorchecker = TRUE;
            } elseif (!$check) {//meaning there is no such input id send the message to the warehouseagent user
                $newmessage = 'Sorry, the Input Code you are Using is Invalid.Kindly call CASU toll free line for further assistance';
                $errorchecker = TRUE;
            } elseif ($this->checkinputactive($mkp_input_id)) {//authenticate the input if it is active
                $newmessage = $this->checkinputactive($mkp_input_id);
                $errorchecker = TRUE;
            } elseif ($this->checkagentmkt($auditdata['mobile'], $mkt_code)) {//check if the agent assigned to that market
                $newmessage = $this->checkagentmkt($auditdata['mobile'], $mkt_code);
                $errorchecker = TRUE;
            } elseif ($this->checkinputmonitored($mkp_input_id, $mkt_code)) {//check if the input is monitored in that market
                $newmessage = $this->checkinputmonitored($mkp_input_id, $mkt_code);
                $errorchecker = TRUE;
            }



            if ($errorchecker == true) {
                $this->sendSms($auditdata['mobile'], $receiver, $newmessage, $inbox_id);

                Tblmkpsmsaudit::model()->updateByPk($mkp_sms_id, array('updated' => '1', 'response_given' => $newmessage));
            } else {//when the checker turns to be false throughout
                $agent_id = isset($auditdata['mobile']) ? Tblmkpagent::model()->getScalar('mkp_agent_id', 'mobile=' . $auditdata['mobile']) : '';
                $newmodel = new Tblmkpinputprices();
                $newmodel->mkp_agent_id = $agent_id;
                $newmodel->mkp_sms_id = $auditdata['mkp_sms_id'];
                $newmodel->mkt_id = $this->getMarketId($mkt_code);
                $newmodel->mkp_input_id = $mkp_input_id;
                $newmodel->price = $auditdata['price'];
                $newmodel->save();
                $newmessage = Lang::t('You have successfully given the price  for {input} from {mkt_code} market whose price is {price}', array('{input}' => $mkp_input_id, '{mkt_code}' => $mkt_code, '{price}' => $newmodel->price));
                if (isset($auditdata['mkp_sms_id'])) {
                    $id = $auditdata['mkp_sms_id'];
                    Tblmkpsmsaudit::model()->updateByPk($id, array('updated' => '1', 'success' => 1, 'response_given' => 'successfully_procesed'));
                }
                $this->sendSms($auditdata['mobile'], $sender, $newmessage, $inbox_id);
            }
        }
    }

    public static function marketagentmobile($mobile_num) {
        // $agent=new Tblwhagent();
        return Tblmkpagent::model()->getScalar('*', 'mobile=' . $mobile_num);
    }

    public function sendSms($receiver, $sender, $message, $inbox_id, $status = 'send') {
        $response = Yii::app()->db->createCommand()
                ->insert(Ozekimessageout::model()->tableName(), array(
            'sender' => $sender,
            'receiver' => $receiver,
            'msg' => $message,
            'status' => $status,
        ));
        if ($response) {
            if (self::OZEKI_ENABLED) {
                Yii::app()->db->createCommand()
                        ->update(Ozekimessagein::model()->tableName(), array('status' => 1), '`id`=:id', array(':id' => $inbox_id));
            }
        }
    }

    public function saveaudit($message_arr = null, $sender = null, $message = null, $response = null) {
        $auditware = new Tblmkpsmsaudit();
        $auditware->mobile = $sender;
        if (count($message_arr == 4)) {
            $auditware->mkt_code = isset($message_arr[1]) ? $message_arr[1] : '';
            $auditware->mkp_input_id = isset($message_arr[2]) ? $message_arr[2] : '';
            $auditware->price = isset($message_arr[3]) ? $message_arr[3] : '';
            //$auditware->success = 1;
        }
        $auditware->sms_received = $message;
        $auditware->response_given = $response;
        $auditware->save();
    }

    public function checkmktcode($code) {
        $mrktcode = Tblmkpmarket::model()->getScalar('mkt_code', "mkt_code LIKE '$code'");

        if (!$mrktcode) {
            return $message = Lang::t('Sorry, you have used Invalid market code {code}', array('{code}' => $code));
        } else {
            return false;
        }
    }

    public function checkagentactive($mobile) {
        $active = Tblmkpagent::model()->getScalar('active', 'mobile=' . $mobile);
        if ($active != 1) {
            return $message = Lang::t("Sorry, your account is inactive  kindly call CASU toll free line for further assistance");
        } else {
            return false;
        }
    }

    public function checkinputactive($mkp_input_id) {
        $check = Tblmkpinput::model()->getScalar('active', "mkp_input_id LIKE '$mkp_input_id'");
        if ($check != 1) {
            return $message = Lang::t("Sorry, the input is inactive  kindly call CASU toll free line for further assistance");
        } else {
            return false;
        }
    }

    public function getMarketId($code) {
        $mrktid = Tblmkpmarket::model()->getScalar('mkt_id', "mkt_code LIKE '$code'");
        return isset($mrktid) ? $mrktid : False;
    }

    public function checkagentmkt($sender, $market_code) {//rem $sender is the mobile Number
        $check = Tblmkpagentmkt::model()->getScalar('agent_mkt_id', "mkp_agent_id IN (SELECT mkp_agent_id FROM tblmkpagent WHERE mobile='$sender') AND mkt_id IN (SELECT mkt_id FROM tblmkpmarket WHERE mkt_code LIKE '$market_code')");
        if (!$check) {
            return $message = Lang::t("Sorry, you are not assigned to {market} market", array('{market}' => $market_code));
        } else {
            return false;
        }
    }

    public function checkinputmonitored($mkp_input_id, $mkt_code) {
        $check = Tblmktinputmarket::model()->getScalar('input_mkt_id', "mkt_input_id ='$mkp_input_id' AND mkt_id IN (SELECT mkt_id FROM tblmkpmarket WHERE mkt_code LIKE '$mkt_code')");
        if (!$check) {
            return $message = Lang::t("Sorry, the input {input_code} is not being monitored in {market} market", array('{market}' => $mkt_code, '{input_code}' => $mkp_input_id));
        } else {
            return false;
        }
    }

}

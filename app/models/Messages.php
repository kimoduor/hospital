<?php

/**
 * This is the model class for table "Messages".
 *
 * The followings are the available columns in table 'Messages':
 * @property integer $ID
 * @property integer $Direction
 * @property integer $Type
 * @property integer $StatusDetails
 * @property integer $Status
 * @property integer $ChannelID
 * @property string $MessageReference
 * @property integer $SentTimeSecs
 * @property integer $ReceivedTimeSecs
 * @property integer $ScheduledTimeSecs
 * @property integer $LastUpdateSecs
 * @property string $Sender
 * @property string $Recipient
 * @property string $Subject
 * @property integer $BodyFormat
 * @property integer $CustomField1
 * @property string $CustomField2
 * @property integer $sysCreator
 * @property integer $sysArchive
 * @property integer $sysLock
 * @property string $sysHash
 * @property integer $sysForwarded
 * @property string $sysGwReference
 * @property string $Header
 * @property string $Body
 * @property string $Trace
 */
class Messages extends ActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'Messages';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('Direction, Type, StatusDetails, Status, ChannelID, SentTimeSecs, ReceivedTimeSecs, ScheduledTimeSecs, LastUpdateSecs, BodyFormat, CustomField1, sysCreator, sysArchive, sysLock, sysForwarded', 'numerical', 'integerOnly' => true),
            array('MessageReference, Sender, Recipient, Subject, CustomField2, sysHash, sysGwReference', 'length', 'max' => 255),
            array('Header, Body, Trace', 'safe'),
            array('ID, Direction, Type, StatusDetails, Status, ChannelID, MessageReference, SentTimeSecs, ReceivedTimeSecs, ScheduledTimeSecs, LastUpdateSecs, Sender, Recipient, Subject, BodyFormat, CustomField1, CustomField2, sysCreator, sysArchive, sysLock, sysHash, sysForwarded, sysGwReference, Header, Body, Trace', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'ID' => 'ID',
            'Direction' => 'Direction',
            'Type' => 'Type',
            'StatusDetails' => 'Status Details',
            'Status' => 'Status',
            'ChannelID' => 'Channel',
            'MessageReference' => 'Message Reference',
            'SentTimeSecs' => 'Sent Time Secs',
            'ReceivedTimeSecs' => 'Received Time Secs',
            'ScheduledTimeSecs' => 'Scheduled Time Secs',
            'LastUpdateSecs' => 'Last Update Secs',
            'Sender' => 'Sender',
            'Recipient' => 'Recipient',
            'Subject' => 'Subject',
            'BodyFormat' => 'Body Format',
            'CustomField1' => 'Custom Field1',
            'CustomField2' => 'Custom Field2',
            'sysCreator' => 'Sys Creator',
            'sysArchive' => 'Sys Archive',
            'sysLock' => 'Sys Lock',
            'sysHash' => 'Sys Hash',
            'sysForwarded' => 'Sys Forwarded',
            'sysGwReference' => 'Sys Gw Reference',
            'Header' => 'Header',
            'Body' => 'Body',
            'Trace' => 'Trace',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Messages the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}

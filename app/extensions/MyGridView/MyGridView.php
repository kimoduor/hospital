<?php

Yii::import('zii.widgets.grid.CGridView');

/**
 * @author Joakim <kimoduor@gmail.com>
 *
 * This extension was written to emulate the wonderful yii2-grid by krajee but for Yii 1.1
 * @link http://demos.krajee.com/grid-demo
 *
 * Also merged the excellent EExcelview extension:
 * @author Nikola Kostadinov
 * @license MIT License
 * @version 0.3
 * @link http://yiiframework.com/extension/eexcelview/
 *
 */
class MyGridView extends CGridView {

    /**
     * Bootstrap Contextual Color Types
     */
    const TYPE_DEFAULT = 'default'; // only applicable for panel contextual style
    const TYPE_PRIMARY = 'primary';
    const TYPE_INFO = 'info';
    const TYPE_DANGER = 'danger';
    const TYPE_WARNING = 'warning';
    const TYPE_SUCCESS = 'success';
    const TYPE_ACTIVE = 'active'; // only applicable for table row contextual style

    /**
     * Grid Export Formats
     */
    const HTML = 'html';
    const CSV = 'csv';
    const TEXT = 'txt';
    const EXCEL = 'xls';

    /**
     * Grid Layout Templates
     */
// panel grid template with `footer`, pager in the `footer`, and `summary` in the `heading`.
    public $template1 = <<< HTML
    {items}
    {after}
    <div class="panel-footer">
        <div class="pull-right">{summary}</div>
        <div class="kv-panel-pager">{pager}</div>
        <div class="clearfix"></div>
    </div>
HTML;
// panel grid template with hidden `footer`.
    public $template2 = <<< HTML
        {items}
HTML;

    /**
     * @var string the template for rendering the {before} part in the layout templates.
     * The following special variables are recognized and will be replaced:
     * - {toolbar}, string which will render the [[$toolbar]] property passed
     * - {export}, string which will render the [[$export]] menu button content
     * - {beforeContent}, string which will render the [[$before]] text passed in the panel settings
     */
    public $beforeTemplate = <<< HTML
<div class="pull-right">
	{toolbar}\n{export}
</div>
{beforeContent}
<div class="clearfix"></div>
HTML;

    /**
     * @var string the template for rendering the {after} part in the layout templates.
     * The following special variables are recognized and will be replaced:
     * - {toolbar}, string which will render the [[$toolbar]] property passed
     * - {export}, string which will render the [[$export]] menu button content
     * - {afterContent}, string which will render the [[$after]] text passed in the panel settings
     */
    public $afterTemplate = '{afterContent}';

    /**
     * @var string the toolbar content to be rendered.
     */
    public $toolbar = '';

    /**
     * @var array the HTML attributes for the grid table element
     */
    public $itemsCssClass = array();

    /**
     * @var boolean whether the grid view will have Bootstrap table styling.
     */
    public $bootstrap = true;

    /**
     * @var boolean whether the grid table will have a `bordered` style.
     * Applicable only if `bootstrap` is `true`. Defaults to `true`.
     */
    public $bordered = true;

    /**
     * @var boolean whether the grid table will have a `striped` style.
     * Applicable only if `bootstrap` is `true`. Defaults to `true`.
     */
    public $striped = true;

    /**
     * @var boolean whether the grid table will have a `condensed` style.
     * Applicable only if `bootstrap` is `true`. Defaults to `false`.
     */
    public $condensed = true;

    /**
     * @var boolean whether the grid table will have a `responsive` style.
     * Applicable only if `bootstrap` is `true`. Defaults to `true`.
     */
    public $responsive = true;

    /**
     * @var boolean whether the grid table will highlight row on `hover`.
     * Applicable only if `bootstrap` is `true`. Defaults to `false`.
     */
    public $hover = true;

    /**
     * @var boolean whether the grid table will have a floating table header.
     * Defaults to `false`.
     */
    public $floatHeader = false;

    /**
     * @var array the plugin options for the floatThead plugin that would render
     * the floating/sticky table header behavior. The default offset from the
     * top of the window where the floating header will 'stick' when scrolling down
     * is set to `50` assuming a fixed bootstrap navbar on top. You can set this to 0
     * or any javascript function/expression.
     * @see http://mkoryak.github.io/floatThead#options
     */
    public $floatHeaderOptions = array('scrollingTop' => 50);

    /**
     * @var array the panel settings. If this is set, the grid widget
     * will be embedded in a bootstrap panel. Applicable only if `bootstrap`
     * is `true`. The following array keys are supported:
     * - `heading`: string, the panel heading. If not set, will not be displayed.
     * - `type`: string, the panel contextual type (one of the TYPE constants,
     *    if not set will default to `default` or `self::TYPE_DEFAULT`),
     * - `footer`: string, the panel footer. If not set, will not be displayed.
     * - 'before': string, content to be placed before/above the grid table (after the header).
     * - `beforeOptions`: array, HTML attributes for the `before` text. If the
     *   `class` is not set, it will default to `kv-panel-before`.
     * - 'after': string, any content to be placed after/below the grid table (before the footer).
     * - `afterOptions`: array, HTML attributes for the `after` text. If the
     *   `class` is not set, it will default to `kv-panel-after`.
     * - `showFooter`: boolean, whether to always show the footer. If so the,
     *    layout will default to the constant `self::TEMPLATE_1`. If this is
     *    set to false, the `pager` will be enclosed within the `kv-panel-after`
     *    container. Defaults to `false`.
     * - `layout`: string, the grid layout to be used if you are using a panel,
     *    If not set, defaults to the constant `self::TEMPLATE_1` if
     *    `showFooter` is `true. If `showFooter` is set to `false`, this will
     *    default to the constant `self::TEMPLATE_2`.
     */
    public $panel = array('showFooter' => true);

    /**
     *
     * @var type
     */
    private $assetUrl;

    /**
     *
     * @var type
     */
    public $nullDisplay = '&nbsp;';

    /**
     * New attribute to regulate the display of summary text;
     * @var type
     */
    public $showPageSummary = true;

    /**
     * Max buttons to show in a pagination
     */
    public $pagerMaxButtons = 10;

    /**
     *
     * @var type
     */
    public $pagerCssClass = 'pagination';

    /**
     *
     */
    public $pager = array(
        'cssFile' => false,
        'header' => '',
        'hiddenPageCssClass' => 'disabled',
        'firstPageLabel' => '<i class="fa fa-angle-double-left"></i>',
        'lastPageLabel' => '<i class="fa fa-angle-double-right"></i>',
        'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
        'prevPageLabel' => '<i class="fa fa-angle-left"></i>',
        'selectedPageCssClass' => 'active',
        'htmlOptions' => array('class' => 'pagination'),
    );

    /**
     *
     * @var type
     */
    public $htmlOptions = array('class' => 'grid-view2');

    /**
     *
     * @var type
     */
    public $cssFile = false;

    /**
     * The model
     * @var type
     */
    public $model;

    /**
     *
     * @var type
     */
    public $primaryKeyAttribute;

    /**
     * EExceView
     */
    public $libPath = 'application.vendors.PHPExcel.Classes.PHPExcel'; //the path to the PHP excel lib
    public static $objPHPExcel = null;
    public static $activeSheet = null;
    //Document properties
    public $creator = '';
    public $title = null;
    public $subject = null;
    public $description = null;
    public $category = null;
    public $lastModifiedBy = null;
    public $keywords = '';
    public $sheetTitle = 'Sheet1';
    public $legal = '';
    public $landscapeDisplay = false;
    public $A4 = false;
    public $RTL = false;
    public $pageFooterText = '&RPage &P of &N';
    //config
    public $autoWidth = true;
    public $exportType = 'Excel2007';
    public $disablePaging = false;
    public $filename = null; //export FileName
    public $stream = true; //stream to browser
    public $gridMode = 'grid'; //Whether to display grid ot export it to selected format. Possible values(grid, export)
    public $gridModeVar = 'grid_mode'; //GET var for the grid mode
    public $gridModeVarAll = 'export'; //GET var for the grid mode
    //options
    public $sumLabel = 'Totals';
    public $decimalSeparator = '.';
    public $thousandsSeparator = ',';
    public $displayZeros = true;
    public $zeroPlaceholder = '-';
    public $border_style;
    public $borderColor = '000000';
    public $bgColor = 'FFFFFF';
    public $textColor = '000000';
    public $rowHeight = 15;
    public $headerBorderColor = '000000';
    public $headerBgColor = 'CCCCCC';
    public $headerTextColor = '000000';
    public $headerHeight = 20;
    public $footerBorderColor = '000000';
    public $footerBgColor = 'FFFFCC';
    public $footerTextColor = '0000FF';
    public $footerHeight = 20;
    public static $fill_solid;
    public static $papersize_A4;
    public static $orientation_landscape;
    public static $horizontal_center;
    public static $horizontal_right;
    public static $vertical_center;
    public static $style = array();
    public static $headerStyle = array();
    public static $footerStyle = array();
    public $automaticSum = false;
    public $summableColumns = array();
    //buttons config
    public $exportButtonsCSS = 'summary';
    public $exportButtons = array('Excel2007');
    public $exportText = 'Export to: ';
    public $exportText2 = 'Export All ';
    //callbacks
    public $onRenderHeaderCell = null;
    public $onRenderDataCell = null;
    public $onRenderFooterCell = null;
    //mime types used for streaming
    public $mimeTypes = array(
        'Excel5' => array(
            'Content-type' => 'application/vnd.ms-excel',
            'extension' => 'xls',
            'caption' => 'Excel(*.xls)',
        ),
        'Excel2007' => array(
            'Content-type' => 'application/vnd.ms-excel',
            'extension' => 'xlsx',
            'caption' => 'Excel(*.xlsx)',
        ),
        'PDF' => array(
            'Content-type' => 'application/pdf',
            'extension' => 'pdf',
            'caption' => 'PDF(*.pdf)',
        ),
        'HTML' => array(
            'Content-type' => 'text/html',
            'extension' => 'html',
            'caption' => 'HTML(*.html)',
        ),
        'CSV' => array(
            'Content-type' => 'application/csv',
            'extension' => 'csv',
            'caption' => 'CSV(*.csv)',
        ),
        'XML' => array(
            'Content-type' => 'application/xml',
            'extension' => 'csv',
            'caption' => 'XML(*.xml)',
        )
    );

    public function init() {
        $this->dataProvider = $this->model->search();
        if (!empty($this->primaryKeyAttribute)) {
            $this->dataProvider->keyAttribute = $this->primaryKeyAttribute;
        }
        if ($this->filterPosition === self::FILTER_POS_HEADER) {
            $this->floatHeader = false;
        }
        if ($this->bootstrap) {
            MyYiiUtils::addCssClass($this->itemsCssClass, 'table');
            if ($this->hover) {
                MyYiiUtils::addCssClass($this->itemsCssClass, 'table-hover');
            }
            if ($this->bordered) {
                MyYiiUtils::addCssClass($this->itemsCssClass, 'table-bordered');
            }
            if ($this->striped) {
                MyYiiUtils::addCssClass($this->itemsCssClass, 'table-striped');
            }
            if ($this->condensed) {
                MyYiiUtils::addCssClass($this->itemsCssClass, 'table-condensed');
            }
        }
        if (isset($this->itemsCssClass['class']))
            $this->itemsCssClass = $this->itemsCssClass['class'];
        $this->summaryText = Lang::t('summary_text');
        $this->pager['maxButtonCount'] = $this->pagerMaxButtons;
        $this->enablePagination = $this->model->enablePagination;
        $this->showPageSummary = $this->model->enableSummary;
        //export
        if (isset($_GET[$this->gridModeVar])) {
            $this->gridMode = $_GET[$this->gridModeVar];
        }
        if (isset($_GET['exportType'])) {
            $this->exportType = $_GET['exportType'];
        }
        $lib = Yii::getPathOfAlias($this->libPath) . '.php';
        if (($this->gridMode == 'export') && (!file_exists($lib))) {
            $this->gridMode = 'grid';
            Yii::log("PHP Excel lib not found($lib). Export disabled !", CLogger::LEVEL_WARNING, 'EExcelview');
        }
        if ($this->gridMode == 'export') {
            $this->initExport();
        } else {
            parent:: init();
        }
        $this->registerAssets();
    }

    public function run() {
        if ($this->bootstrap && !empty($this->panel)) {
            $this->renderPanel();
        }
        if ($this->bootstrap && $this->responsive) {
            $this->template = str_replace('{items}', '<div class="table-responsive">{items}</div>', $this->template);
        }
        if ($this->gridMode === 'export') {
            $this->runExport();
        } else {
            parent::run();
        }
    }

    /**
     * Sets the grid layout based on the template and panel settings
     */
    protected function renderPanel() {
        $heading = ArrayHelper::getValue($this->panel, 'heading', '');
        $footer = ArrayHelper::getValue($this->panel, 'footer', '');
        $showFooter = ArrayHelper::getValue($this->panel, 'showFooter', true);
        if ($showFooter) {
            $template = $this->template1;
        } else {
            $template = $this->template2;
            //$this->enablePagination = false;
            //$this->dataProvider->pagination = false;
        }

        $layout = ArrayHelper::getValue($this->panel, 'layout', $template);
        $after = ArrayHelper::getValue($this->panel, 'after', '');
        $afterOptions = ArrayHelper::getValue($this->panel, 'afterOptions', array());

        if ($after != '' && $layout != $this->template2) {
            if (empty($afterOptions['class'])) {
                $afterOptions['class'] = 'kv-panel-after';
            }
            $content = strtr($this->afterTemplate, ['{afterContent}' => $after]);
            $after = CHtml::tag('div', $afterOptions, $content);
        }

        $this->template = strtr($layout, array(
            '{heading}' => $heading,
            '{footer}' => $footer,
            '{after}' => $after
        ));
    }

    /**
     * Renders the export menu
     *
     * @return string
     */
    public function renderExport() {
        return '';
    }

    /**
     * Register assets
     */
    protected function registerAssets() {
        $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
        $this->assetUrl = Yii::app()->assetManager->publish($assets);
        $cs = Yii::app()->clientScript;
        $cs->registerCssFile($this->assetUrl . '/css/kv-grid.min.css')
                ->registerCssFile($this->assetUrl . '/css/custom.css', 'screen, print');
        //->registerScriptFile($this->assetUrl . '/js/ajax-search.js', CClientScript::POS_END)

        if ($this->floatHeader) {
            $cs->registerScriptFile($this->assetUrl . '/js/jquery.floatThead.min.js', CClientScript::POS_END);
            $this->floatHeaderOptions += array(
                'floatTableClass' => 'kv-table-float',
                'floatContainerClass' => 'kv-thead-float',
            );
            $js = '$("#' . $this->id . ' table").floatThead(' . CJSON::encode($this->floatHeaderOptions) . ');';
            $cs->registerScript($this->id . 'floatThread', $js);
        }
    }

    protected function initExport() {
        if (!isset($this->title)) {
            $this->title = Yii::app()->getController()->getPageTitle();
        }
        $this->initColumns();
        //parent::init();
        //Autoload fix
        spl_autoload_unregister(array('YiiBase', 'autoload'));
        Yii::import($this->libPath, true);

        // Get here some PHPExcel constants in order to use them elsewhere
        self::$papersize_A4 = PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4;
        self::$orientation_landscape = PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE;
        self::$fill_solid = PHPExcel_Style_Fill::FILL_SOLID;
        if (!isset($this->border_style)) {
            $this->border_style = PHPExcel_Style_Border::BORDER_THIN;
        }
        self::$horizontal_center = PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
        self::$horizontal_right = PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
        self::$vertical_center = PHPExcel_Style_Alignment::VERTICAL_CENTER;

        spl_autoload_register(array('YiiBase', 'autoload'));

        // Creating a workbook
        self::$objPHPExcel = new PHPExcel();
        self::$activeSheet = self::$objPHPExcel->getActiveSheet();

        // Set some basic document properties
        if ($this->landscapeDisplay) {
            self::$activeSheet->getPageSetup()->setOrientation(self::$orientation_landscape);
        }

        if ($this->A4) {
            self::$activeSheet->getPageSetup()->setPaperSize(self::$papersize_A4);
        }

        if ($this->RTL) {
            self::$activeSheet->setRightToLeft(true);
        }

        self::$objPHPExcel->getProperties()
                ->setTitle($this->title)
                ->setCreator($this->creator)
                ->setSubject($this->subject)
                ->setDescription($this->description . ' // ' . $this->legal)
                ->setCategory($this->category)
                ->setLastModifiedBy($this->lastModifiedBy)
                ->setKeywords($this->keywords);

        // Initialize styles that will be used later
        self::$style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => $this->border_style,
                    'color' => array('rgb' => $this->borderColor),
                ),
            ),
            'fill' => array(
                'type' => self::$fill_solid,
                'color' => array('rgb' => $this->bgColor),
            ),
            'font' => array(
                //'bold' => false,
                'color' => array('rgb' => $this->textColor),
            )
        );
        self::$headerStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => $this->border_style,
                    'color' => array('rgb' => $this->headerBorderColor),
                ),
            ),
            'fill' => array(
                'type' => self::$fill_solid,
                'color' => array('rgb' => $this->headerBgColor),
            ),
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => $this->headerTextColor),
            )
        );
        self::$footerStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => $this->border_style,
                    'color' => array('rgb' => $this->footerBorderColor),
                ),
            ),
            'fill' => array(
                'type' => self::$fill_solid,
                'color' => array('rgb' => $this->footerBgColor),
            ),
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => $this->footerTextColor),
            )
        );
    }

    public function renderHeader() {
        $a = 0;
        foreach ($this->columns as $column) {
            if ($column instanceof CButtonColumn || $column instanceof CCheckBoxColumn) {
                continue;
            }
            $a = $a + 1;
            if (($column->header === null) && ($column->name !== null)) {
                if ($column->grid->dataProvider instanceof CActiveDataProvider) {
                    $head = $column->grid->dataProvider->model->getAttributeLabel($column->name);
                } else {
                    $head = $column->name;
                }
            } else {
                $head = trim($column->header) !== '' ? $column->header : $column->grid->blankDisplay;
            }

            $cell = self::$activeSheet->setCellValue($this->columnName($a) . '1', $head, true);

            if (is_callable($this->onRenderHeaderCell)) {
                call_user_func_array($this->onRenderHeaderCell, array($cell, $head));
            }
        }

        // Format the header row
        $header = self::$activeSheet->getStyle($this->columnName(1) . '1:' . $this->columnName($a) . '1');
        $header->getAlignment()
                ->setHorizontal(self::$horizontal_center)
                ->setVertical(self::$vertical_center);
        $header->applyFromArray(self::$headerStyle);
        self::$activeSheet->getRowDimension(1)->setRowHeight($this->headerHeight);
    }

    public function renderBody() {
        if (isset($_GET['exportall'])) {
            $this->dataProvider->pagination = false;
        }
        if ($this->disablePaging) {
            //if needed disable paging to export all data
            $this->dataProvider->pagination = false;
        }
        $data = $this->dataProvider->getData();
        $n = count($data);

        if ($n > 0) {
            for ($row = 0; $row < $n; ++$row) {
                $this->renderRow($row);
            }
        }
        return $n;
    }

    public function renderRow($row) {
        $data = $this->dataProvider->getData();

        $a = 0;
        foreach ($this->columns as $n => $column) {
            if ($column instanceof CButtonColumn || $column instanceof CCheckBoxColumn) {
                continue;
            }
            if ($column instanceof CLinkColumn) {
                if ($column->labelExpression !== null) {
                    $value = $column->evaluateExpression($column->labelExpression, array('data' => $data[$row], 'row' => $row));
                } else {
                    $value = $column->label;
                }
            } else if ($column->value !== null) {
                $value = $this->evaluateExpression($column->value, array('data' => $data[$row]));
            } else if ($column->name !== null) {
                //$value = $data[$row][$column->name];
                $value = CHtml::value($data[$row], $column->name);
                $value = $value === null ? "" : $column->grid->getFormatter()->format($value, 'raw');
            }

            $a++;

            if ($this->automaticSum && in_array($a, $this->summableColumns)) {
                // Check if the cell value is a number, then format it accordingly
                // May be improved notably by exposing the formats as public
                // May be usable only for French-style number formatting ?
                if (preg_match("/^[0-9]*\\" . $this->thousandsSeparator . "[0-9]*\\" . $this->decimalSeparator . "[0-9]*$/", strip_tags($value))) {
                    $content = str_replace($this->decimalSeparator, '.', str_replace($this->thousandsSeparator, '', strip_tags($value)));
                    $format = '#\,##0.00';
                } else if (preg_match("/^[0-9]*\\" . $this->decimalSeparator . "[0-9]*$/", strip_tags($value))) {
                    $content = str_replace($this->decimalSeparator, '.', strip_tags($value));
                    $format = '0.00';
                } else if (!$this->displayZeros && ((strip_tags($value) === '0') || (strip_tags($value) === $this->zeroPlaceholder))) {
                    $content = $this->zeroPlaceholder;
                    self::$activeSheet->getStyle($this->columnName($a) . ($row + 2))->getAlignment()->setHorizontal(self::$horizontal_right);
                    $format = '0.00';
                } else if (preg_match("/^[0-9]*\\" . $this->thousandsSeparator . "[0-9]*\\" . $this->thousandsSeparator . "[0-9]*\\" . $this->decimalSeparator . "[0-9]*$/", strip_tags($value))) {
                    $content = str_replace($this->decimalSeparator, '.', str_replace($this->thousandsSeparator, '', strip_tags($value)));
                    $format = '#\,###\,##0.00';
                } else {
                    $content = (float) $value;
                    $format = '0.00';
                }
            } else {
                $content = strip_tags($value);
                $format = null;
            }
            $cell = self::$activeSheet->setCellValue($this->columnName($a) . ($row + 2), $content, true);
            // Format each cell's number - if any
            if (!is_null($format)) {
                self::$activeSheet->getStyle($this->columnName($a) . ($row + 2))->getNumberFormat()->setFormatCode($format);
            }
            if (is_callable($this->onRenderDataCell)) {
                call_user_func_array($this->onRenderDataCell, array($cell, $data[$row], $value));
            }
        }
        // Format the row globally
        $renderedRow = self::$activeSheet->getStyle('A' . ($row + 2) . ':' . $this->columnName($a) . ($row + 2));
        $renderedRow->getAlignment()->setVertical(self::$vertical_center);
        $renderedRow->applyFromArray(self::$style);
        self::$activeSheet->getRowDimension($row + 2)->setRowHeight($this->rowHeight);

    }

    public function renderFooter($row) {
        $a = 0;
        foreach ($this->columns as $n => $column) {
            $a = $a + 1;
            if ($column->footer) {
                $footer = trim($column->footer) !== '' ? $column->footer : $column->grid->blankDisplay;

                $cell = self::$activeSheet->setCellValue($this->columnName($a) . ($row + 2), $footer, true);

                if (is_callable($this->onRenderFooterCell)) {
                    call_user_func_array($this->onRenderFooterCell, array($cell, $footer));
                }
            } else if ($this->automaticSum && in_array($a, $this->summableColumns)) {
                // We want to render automatic sums in the footer if no footer was already present in the grid
                $cell = self::$activeSheet->setCellValue($this->columnName($a) . ($row + 2), '=SUM(' . $this->columnName($a) . '2:' . $this->columnName($a) . ($row + 1) . ')', true);
                $sum = self::$activeSheet->getCell($this->columnName($a) . ($row + 2))->getCalculatedValue();
                if ($sum < 1000) {
                    $format = '0.00';
                } else if ($sum < 1000000) {
                    $format = '#\,##0.00';
                } else {
                    $format = '#\,###\,##0.00';
                }

                // We won't set the whole row's borders and number format, so proceed with each cell individually
                self::$activeSheet->getStyle($this->columnName($a) . ($row + 2))
                        ->applyFromArray(self::$footerStyle)
                        ->getNumberFormat()->setFormatCode($format);

                if (is_callable($this->onRenderFooterCell)) {
                    call_user_func_array($this->onRenderFooterCell, array($cell, $footer));
                }

                // Add a label before the first summable column (supposing it's not the first…)
                if (current($this->summableColumns) == $a) {
                    $cell = self::$activeSheet->setCellValue($this->columnName($a - 1) . ($row + 2), $this->sumLabel, true);
                    self::$activeSheet->getStyle($this->columnName($a - 1) . ($row + 2))
                            ->applyFromArray(array('font' => array('bold' => true)))
                            ->getAlignment()->setHorizontal(self::$horizontal_right);
                    if (is_callable($this->onRenderFooterCell)) {
                        call_user_func_array($this->onRenderFooterCell, array($cell, $footer));
                    }
                }
            }
        }

        // Some global formatting for the footer in case of automatic sum
        if ($this->automaticSum) {
            self::$activeSheet->getStyle('A' . ($row + 2) . ':' . $this->columnName($a) . ($row + 2))->getAlignment()->setVertical(self::$vertical_center);
            self::$activeSheet->getRowDimension($row + 2)->setRowHeight($this->footerHeight);
        }
    }

    /**
     * Returns the corresponding Excel column.(Abdul Rehman from yii forum)
     *
     * @param int $index
     * @return string
     */
    public function columnName($index) {
        --$index;
        if (($index >= 0) && ($index < 26)) {
            return chr(ord('A') + $index);
        } else if ($index > 25) {
            return ($this->columnName($index / 26)) . ($this->columnName($index % 26 + 1));
        } else {
            throw new Exception("Invalid Column # " . ($index + 1));
        }
    }

    public function renderExportButtons() {
        foreach ($this->exportButtons as $key => $button) {
            $item = is_array($button) ? CMap::mergeArray($this->mimeTypes[$key], $button) : $this->mimeTypes[$button];
            $type = is_array($button) ? $key : $button;
            $url = parse_url(Yii::app()->request->requestUri);
            //$content[] = CHtml::link($item['caption'], '?'.$url['query'].'exportType='.$type.'&'.$this->grid_mode_var.'=export');
            if (key_exists('query', $url)) {
                $content[] = CHtml::link($item['caption'], '?' . $url['query'] . '&exportType=' . $type . '&' . $this->gridModeVar . '=export');
            } else {
                $content[] = CHtml::link($item['caption'], '?exportType=' . $type . '&' . $this->gridModeVar . '=export');
            }
        }
        if ($content) {
            echo CHtml::tag('div', array('class' => $this->exportButtonsCSS), $this->exportText . implode(', ', $content));
            echo CHtml::tag('div', array('class' => $this->exportButtonsCSS), $this->exportText2 . implode(', ', $content));
        }
    }

    /**
     * Performs cleaning on mutliple levels.
     *
     * From le_top @ yiiframework.com
     *
     */
    private static function cleanOutput() {
        for ($level = ob_get_level(); $level > 0; --$level) {
            @ob_end_clean();
        }
    }

    protected function runExport() {
        $this->renderHeader();
        $row = $this->renderBody();

        $this->renderFooter($row);

        //set auto width
        if ($this->autoWidth) {
            foreach ($this->columns as $n => $column) {
                self::$activeSheet->getColumnDimension($this->columnName($n + 1))->setAutoSize(true);
            }
        }

        // Set some additional properties
        self::$activeSheet
                ->setTitle($this->sheetTitle)
                ->getSheetView()->setZoomScale(100);
        self::$activeSheet->getHeaderFooter()
                ->setOddHeader('&C' . $this->sheetTitle)
                ->setOddFooter('&L&B' . self::$objPHPExcel->getProperties()->getTitle() . $this->pageFooterText);
        self::$activeSheet->getPageSetup()
                ->setPrintArea('A1:' . $this->columnName(count($this->columns)) . ($row + 2))
                ->setFitToWidth();

        //create writer for saving
        $objWriter = PHPExcel_IOFactory::createWriter(self::$objPHPExcel, $this->exportType);
        if (!$this->stream) {
            $objWriter->save($this->filename);
        } else {
            //output to browser
            if (!$this->filename) {
                $this->filename = Yii::app()->name . '-' . date('d-m-Y');
            }
            $this->cleanOutput();
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-type: ' . $this->mimeTypes[$this->exportType]['Content-type']);
            header('Content-Disposition: attachment; filename="' . $this->filename . '.' . $this->mimeTypes[$this->exportType]['extension'] . '"');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
            Yii::app()->end();
        }
    }

}

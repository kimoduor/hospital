<?php

/**
 * HighCharts widget
 * @uses Twitter Bootrap version 2.1.0
 * @author Joakim <kimoduor@gmail.com>
 */
class MyHighChart extends CWidget
{

    /**
     * Graph data
     * @var type
     */
    public $data = array();

    /**
     * The number of charts in a page
     * @var type
     */
    public $chartCount = 1;

    /**
     * HTML options for the chart container
     * @var type
     */
    public $htmlOptions = array();

    /**
     * Graph container id
     * @var type
     */
    private $containerId;

    /**
     *
     * @var type
     */
    public $chartTemplate = '{filter_form}<hr/>{chart}';

    /**
     *
     * @var type
     */
    public $filterFormTemplate = '<div class="row"><div class="col-md-6">{graph_type}</div><div class="col-md-6">{date_range}</div></div>';

    /**
     *
     * @var type
     */
    public $filterFormHtmlOptions = array();

    /**
     *
     * @var type
     */
    public $graphTypeFilterHtmlOptions = array(
        'class' => '',
        'style' => 'width:auto;'
    );

    /**
     *
     * @var type
     */
    public $dateRangeFilterHtmlOptions = array(
        'class' => 'my-date-range-picker pull-right',
    );

    /**
     *
     * @var type
     */
    public $showDateRangeFilter = true;

    /**
     *
     * @var type
     */
    public $showGraphTypeFilter = true;

    /**
     *
     * @var type
     */
    public $highChartOptions = array();

    /**
     *
     * @var type
     */
    private $showFilter = true;

    /**
     *
     * @var type
     */
    private $dateRangeFrom = null;

    /**
     *
     * @var type
     */
    private $dateRangeTo = null;

    /**
     *
     * @var type
     */
    private $dateRangeFormat = array('php' => 'M d, Y', 'js' => 'MMM D, YYYY');

    /**
     * Graph type filters
     * @array type
     */
    public $graphTypeOptions = array();

    /**
     *
     * @var type
     */
    private $chartID;

    public function init()
    {
        parent::init();

        $this->containerId = 'my_high_chart_' . $this->chartCount;
        $this->htmlOptions['id'] = $this->containerId;
        $this->htmlOptions['id'] = $this->containerId;
        $this->chartID = $this->containerId . '_chart';
        $this->filterFormHtmlOptions['id'] = $this->containerId . '_form';
        $this->dateRangeFilterHtmlOptions['id'] = $this->containerId . '_' . HighChartsHelper::GET_PARAM_DATE_RANGE;
        $this->graphTypeFilterHtmlOptions['id'] = $this->containerId . '_' . HighChartsHelper::GET_PARAM_GRAPH_TYPE;
        if ($this->showGraphTypeFilter || $this->showDateRangeFilter)
            $this->showFilter = TRUE;
        else
            $this->showFilter = FALSE;
        $date_range = HighChartsHelper::explodeDateRange($this->dateRangeFormat['php']);
        $this->dateRangeFrom = $date_range['from'];
        $this->dateRangeTo = $date_range['to'];

        $this->registerAssets();
    }

    public function run()
    {
        $graph_type_filter = '';
        $date_range_filter = '';
        $filter_form = '';
        if ($this->showGraphTypeFilter) {
            $graph_type_filter = CHtml::dropDownList(HighChartsHelper::GET_PARAM_GRAPH_TYPE, HighChartsHelper::getGraphType(), !empty($this->graphTypeOptions) ? $this->graphTypeOptions : HighChartsHelper::graphTypes(), $this->graphTypeFilterHtmlOptions);
        }
        if ($this->showDateRangeFilter) {
            $date_range_string = $this->dateRangeFrom . ' - ' . $this->dateRangeTo;
            $date_range_filter = CHtml::tag('span', array(), $date_range_string);
            $date_range_hidden = CHtml::hiddenField(HighChartsHelper::GET_PARAM_DATE_RANGE);
            $date_range_filter_container = CHtml::tag('div', $this->dateRangeFilterHtmlOptions, $date_range_hidden . '<i class="fa fa-calendar fa-lg"></i>&nbsp;' . $date_range_filter . '&nbsp;<b class = "caret"></b>');
        }
        if ($this->showFilter) {
            $filter_form .= CHtml::beginForm(Yii::app()->createUrl($this->owner->route, $this->owner->actionParams), 'get', $this->filterFormHtmlOptions);
            $filter_form .= Common::myStringReplace($this->filterFormTemplate, array(
                        '{graph_type}' => $graph_type_filter,
                        '{date_range}' => $date_range_filter_container,
            ));
            $filter_form.=CHtml::hiddenField(HighChartsHelper::GET_PARAM_HIGHCHART_FLAG, true);
            $filter_form.=CHtml::endForm();
        }

        $chart = CHtml::tag('div', array('id' => $this->chartID), '', true);
        $contents = strtr($this->chartTemplate, array(
            '{filter_form}' => $filter_form,
            '{chart}' => $chart,
        ));
        echo CHtml::tag('div', $this->htmlOptions, $contents);
    }

    protected function registerAssets()
    {
        $options = array(
            'highchart' => array(
                'containerId' => $this->chartID,
                'highchartOptions' => array(),
            ),
            'filter' => array(
                'containerId' => $this->containerId,
                'showFilter' => $this->showFilter,
                'filterFormId' => $this->filterFormHtmlOptions['id'],
                'dateRangeFilterId' => $this->dateRangeFilterHtmlOptions['id'],
                'graphTypeFilterId' => $this->graphTypeFilterHtmlOptions['id'],
                'dateRangeFrom' => $this->dateRangeFrom,
                'dateRangeTo' => $this->dateRangeTo,
                'dateRangeFormat' => $this->dateRangeFormat['js'],
            )
        );
        //register scripts
        $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript
                ->registerCssFile($baseUrl . '/custom.css')
                ->registerScriptFile($baseUrl . '/highcharts-4.0.3/js/highcharts.js', CClientScript::POS_END)
                ->registerScriptFile($baseUrl . '/highcharts-4.0.3/js/modules/exporting.js', CClientScript::POS_END)
                ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugin/bootstrap-daterangepicker/moment.min.js', CClientScript::POS_END)
                ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugin/bootstrap-daterangepicker/daterangepicker.min.js', CClientScript::POS_END)
                ->registerCssFile(Yii::app()->theme->baseUrl . '/js/plugin/bootstrap-daterangepicker/daterangepicker-bs3.min.css')
                ->registerScriptFile($baseUrl . '/custom.js', CClientScript::POS_END)
                ->registerScript($this->containerId . $this->chartCount, "MyHighChart.init(" . CJavaScript::encode($options) . "," . json_encode($this->data) . ");", CClientScript::POS_READY);
    }

}

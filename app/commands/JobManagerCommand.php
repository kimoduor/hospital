<?php

/**
 * All cron jobs actions are defined here.
 * @author JoAKIM <kimoduor@gmail.com>
 */
class JobManagerCommand extends CConsoleCommand
{

    public function actionSendEmail(array $args)
    {
        $this->startJob(SysJobs::JOB_SEND_EMAIL);
    }

    protected function sendEmail()
    {
        Email::model()->processQueues();
    }

    public function actionCleanUp()
    {
        $this->startJob(SysJobs::JOB_CLEAN_UP);
    }

    protected function cleanUp()
    {
        MyYiiUtils::clearTempFiles();
    }

    public function actionSmsProcessor()
    {
        $this->startJob('smsProcessor');
    }

    protected function smsProcessor()
    {
        $obj = new VoucherVerification();
        $obj->processSmsInbox();
    }

    /**
     * Start a Job
     * @param type $job_id
     * @return type
     */
    protected function startJob($job_id)
    {
        try {
            $job = SysJobs::model()->findByPk($job_id);
            if (null === $job)
                throw new Exception("{$job_id}does not exist.");
            if (!$job->is_active)
                return false;
            //non continuous execution type
            if ($job->execution_type ===
                    SysJobs::EXEC_TYPE_CRON) {
                $this->{$job_id}();
                return SysJobs::model()->updateLastRun($job->id);
            }

            $error = false;

            SysJobProcesses::model()->clearProcesses($job->id);
            if ($job->threads >= $job->max_threads)
                $error = true;


            if ($error === true) {
                $has_timed_out = SysJobs::model()->hasTimedOut($job->id);
                if ($has_timed_out) {
                    $error = false;

                    $job->threads = 1;
                    $job->save(false);
                }
            }
            if ($error === false) {
                //create process
                $process_id = SysJobProcesses::model()->createProcess($job);
                while ($job->is_active) {
                    $job = $this->runJob($job, $process_id);
                }
            }
        } catch (Exception $exc) {
            Yii::log($exc->getMessage(), CLogger::LEVEL_ERROR);
        }
    }

    /**
     * Run a given job
     * @param \SysJobs $job
     * @param string $process_id
     */
    protected function runJob($job, $process_id)
    {
        try {
            if (Yii::app()->db->getActive() === false)
                Yii::app()->db->setActive(true); //open new connection

            if ($job->threads >= $job->max_threads) {
                if (SysJobProcesses::model()->retireProcess($job->id, $process_id, $job->
                                max_threads + 1))
                    Yii::app()->end();
            }


            $process = SysJobProcesses::model()->find('`id`=:id', array(':id' => $process_id));
            if (null === $process) {
                Yii::app()->end();
            }

            //update last run
            $process->last_run_datetime = new CDbExpression('NOW()');
            $process->status = SysJobProcesses::STATUS_RUNNING;
            $process->save(false);

            $this->{$job->id}();

            SysJobs::model()->updateLastRun($job->id);

            $job->refresh();

            $this->sleep($job, $process);
        } catch (CException $exc) {
            //delete the process
            Yii::app()->db->createCommand()
                    ->delete(SysJobProcesses::model()->tableName(), '`id`=:id', array(':id' => $process_id));

            //reduce the threads by one
            $job->threads = $job->threads - 1;
            $job->save(false);

            Yii::log($exc->getMessage(), CLogger::LEVEL_ERROR);
        }

        return $job;
    }

    /**
     *
     * @param \SysJobs $job
     * @param \SysJobProcesses $process
     */
    protected function sleep($job, $process)
    {
        $process->status = SysJobProcesses::STATUS_SLEEPING;
        $process->save(false);

        Yii::app()->db->setActive(false); //close db connection on sleep mode.

        $sleep_seconds = $job->sleep;
        if ($sleep_seconds < 1)
            $sleep_seconds = 1;

        if (function_exists('sys_getloadavg')) {
            do {
                sleep($sleep_seconds);
            } while (sys_getloadavg()[0] > 6
            );
        } else
            sleep((int) $sleep_seconds);
    }

}

/**
 * Contains all functions that are commonly used in the application
 * @author Joakim <kimoduor@gmail.com>
 * @type object
 */
var MyUtils = {
    /**
     *reloads a page
     *@param {string} url  The url to reload to
     *@param {integer} delay : the delay in milliseconds e.g 1000,2000 for 1 sec and 2 sec respectively
     */
    reload: function (url, delay) {
        "use strict";
        if (!url || typeof url === 'undefined') {
            url = location.href;
        }
        if (!delay || typeof delay === 'undefined')
            window.location = url;
        else {
            setTimeout(function () {
                window.location = url;
            }, delay);
        }
    },
    /**
     * Initialises the colorbox modal
     * @param {string} url : The remote Url to load
     * @param {boolean} reloadOnclean : If set to true the page refreshes after the colorbox is closed
     */
    showColorbox: function (url, reloadOnclean) {
        'use strict';
        if (typeof reloadOnclean === 'undefined')
            reloadOnclean = true;
        $.colorbox({
            href: url,
            overlayClose: false,
            onCleanup: function () {
                if (reloadOnclean)
                    MyUtils.reload();
            },
            onComplete: function () {
                $(this).colorbox.resize();
            }
        });
        return false;
    },
    /**
     * Trigger submit event on a form
     * @param {string} selector The form selector
     * @returns {Boolean}
     */
    triggerSubmit: function (selector) {
        $(selector).trigger('submit');
        return false;
    },
    /**
     *Pupulates a select options with a given data in JSON format e.g [{id:"1",name:"Sample1"},{id:"2",name:"Sample2"}]
     * @param {string} selecter
     * @param {object} data JSON data
     * @param {string} selected_id
     * @returns {void}
     */
    populateDropDownList: function (selecter, data, selected_id) {
        'use strict';
        var options = '';
        var jsonObj;
        if (data instanceof Array) {
            jsonObj = $.parseJSON(JSON.stringify(data));
        }
        else if (typeof data === 'string') {
            jsonObj = $.parseJSON(data);
        }
        else {
            jsonObj = data;
        }
        $.each(jsonObj, function (i, item) {
            if (item.id === null)
                item.id = "";
            options += '<option value="' + item.id + '">' + item.name + '</option>';
        });
        var select = typeof selecter === 'object' ? selecter : $(selecter);
        select.html(options);
        if (!MyUtils.empty(selected_id)) {
            select.val(selected_id).change();
        }
    },
    /**
     * Formats a number with grouped thousands~
     * Strip all characters but numerical ones.
     * @param {mixed} number
     * @param {integer} decimals
     * @param {string} dec_point
     * @param {string} thousands_sep
     * @returns {@exp;s@call;join}
     */
    number_format: function (number, decimals, dec_point, thousands_sep) {
        "use strict";
        number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
        var n = !isFinite(+number) ? 0 : +number,
                prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
                sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
                dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
                s = '',
                toFixedFix = function (n, prec) {
                    var k = Math.pow(10, prec);
                    return '' + Math.round(n * k) / k;
                };
        // Fix for IE parseFloat(0.55).toFixed(0) = 0;
        s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    },
    /**
     *Adds more params to a url
     *@param {string} url The url
     *@param {string} key the $_GET key
     *@param {string} value the $_GET value for the key
     */
    addParameterToURL: function (url, key, value) {
        "use strict";
        var _url = url;
        _url += (_url.split('?')[1] ? '&' : '?') + key + '=' + value;
        return _url;
    },
    /**
     * Starts a block UI
     * @param {string} message
     * @returns {void}
     */
    startBlockUI: function (message) {
        if (typeof message === 'undefined') {
            message = 'Please wait ...';
        }
        var content = '<span id="my_block_ui">' + message + '</span>';
        $.blockUI({
            message: content,
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#333C44',
                '-webkit-border-radius': '3px',
                '-moz-border-radius': '3px',
                opacity: 1, color: '#fff',
            },
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.4,
                cursor: 'wait',
                'z-index': 1030,
            },
        });
    },
    /**
     * Stops a block ui
     * @returns {undefined}
     */
    stopBlockUI: function () {
        $.unblockUI();
    },
    /**
     * Checks whether a given string is a valid integer
     * @param {string} number
     * @returns {Boolean}
     */
    isInt: function (number) {
        if (Math.floor(number) == number && $.isNumeric(number))
            return true;
        else
            return false;
    },
    /**
     *Show alert message.
     * @param {string} message
     * @param {string} type
     * @param {string} containerSelector
     * @returns {void}
     */
    showAlertMessage: function (message, type, containerSelector)
    {
        var validTypes = ['success', 'error', 'notice'], html = '';
        if (typeof type === 'undefined' || (jQuery.inArray(type, validTypes) < 0))
            type = validTypes[0];
        if (type === 'success') {
            html += '<div class="alert alert-success alert-block">';
        }
        else if (type === 'error') {
            html += '<div class="alert alert-danger alert-block">';
        }
        else {
            html += '<div class="alert alert-warning alert-block">';
        }
        html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
        html += '<p>' + message + '</p>';
        html += '</div>';
        if (typeof containerSelector === 'undefined')
            containerSelector = '#user-flash-messages';
        var container;
        if (typeof containerSelector === 'object') {
            container = containerSelector;
        } else {
            container = $(containerSelector);
        }
        container.html(html).removeClass('hidden');
    },
    //Grid view operations
    GridView: {
        /**
         *get selection ids from gridview
         *@param grid_id : The ID of the CgridView Table
         *@return selectionIds (e.g 1,2,3)
         */
        getSelectedIds: function (grid_id) {
            "use strict";
            var selectionIds = $.fn.yiiGridView.getSelection(grid_id);
            if (selectionIds.length === 0 || selectionIds === '') {
                return false;
            }
            return selectionIds;
        },
        /**
         * delete many records when the gridview items are selected
         * @param e : element object
         * @param grid_id : The ID of the CgridView Table
         * @param confirmMsg : The delete confirmation message
         */
        deleteMultiple: function (e, confirmMessage) {
            "use strict";

            if (typeof confirmMessage === 'undefined')
                confirmMessage = 'Are you really sure?';
            var grid_id = $(e).data('grid_id')
                    , selectionIds = MyUtils.GridView.getSelectedIds(grid_id);
            if (MyUtils.empty(selectionIds))
                return false;
            if (!confirm(confirmMessage))
                return false;
            var url = $(e).data('ajax-url')
                    , data = 'ids=' + selectionIds;
            return MyUtils.GridView.submitGridView(grid_id, url, data);
        },
        /**
         * Submit Gridview operation via AJAX
         * @param {string} grid_id
         * @param {string} url
         * @param {string} data
         * @returns {Boolean}
         */
        submitGridView: function (grid_id, url, data) {
            "use strict";
            $.fn.yiiGridView.update(grid_id, {
                type: 'POST',
                url: url,
                data: data,
                success: function (data) {
                    $.fn.yiiGridView.update(grid_id);
                },
                error: function (XHR) {
                    MyUtils.showAlertMessage(XHR.responseText, 'error');
                }
            });
            return false;
        },
        /**
         * Passes the selected values to a colorbox
         * @param elem : The element object
         * @param grid_id : The ID of the CgridView Table
         */
        gridViewColorBoxInit: function (e) {
            "use strict";
            var grid_id = $(e).attr('data-grid_id')
                    , ids = MyUtils.GridView.getSelectedIds(grid_id);
            if (!ids)
                return false;
            var href = MyUtils.addParameterToURL($(e).attr('href'), 'ids', ids);
            MyUtils.showColorbox(href);
            return false;
        },
        /**
         *Update gridview
         * @param {string} grid_id
         */
        updateGridView: function (grid_id) {
            $.fn.yiiGridView.update(grid_id);
        }
    },
    bootstrapDatePicker: function (e) {
        'use strict';
        var nowTemp = new Date();
        var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
        $(e).datepicker({
            format: 'yyyy-mm-dd',
            onRender: function (date) {
                if ($(e).data('date-disabled') === 'past') {
                    return date.valueOf() < now.valueOf() ? 'disabled' : '';
                }
                else if ($(e).data('date-disabled') === 'future') {
                    return date.valueOf() > now.valueOf() ? 'disabled' : '';
                }
            }
        });
    },
    empty: function (mixed_var) {
        // Checks if the argument variable is empty
        // undefined, null, false, number 0, empty string,
        // string "0", objects without properties and empty arrays
        // are considered empty
        //
        // http://kevin.vanzonneveld.net
        // +   original by: Philippe Baumann
        // +      input by: Onno Marsman
        // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // +      input by: LH
        // +   improved by: Onno Marsman
        // +   improved by: Francesco
        // +   improved by: Marc Jansen
        // +      input by: Stoyan Kyosev (http://www.svest.org/)
        // +   improved by: Rafal Kukawski
        // *     example 1: empty(null);
        // *     returns 1: true
        // *     example 2: empty(undefined);
        // *     returns 2: true
        // *     example 3: empty([]);
        // *     returns 3: true
        // *     example 4: empty({});
        // *     returns 4: true
        // *     example 5: empty({'aFunc' : function () { alert('humpty'); } });
        // *     returns 5: false
        var undef, key, i, len;
        var emptyValues = [undef, null, false, 0, "", "0"];

        for (i = 0, len = emptyValues.length; i < len; i++) {
            if (mixed_var === emptyValues[i]) {
                return true;
            }
        }

        if (typeof mixed_var === "object") {
            for (key in mixed_var) {
                // TODO: should we check for own properties only?
                //if (mixed_var.hasOwnProperty(key)) {
                return false;
                //}
            }
            return true;
        }

        return false;
    }
    ,
    /**
     * Strip HTML tags
     * @param {string} the_string
     * @returns {undefined}
     */
    strip_tags: function (the_string) {
        return the_string.replace(/(<([^>]+)>)/ig, "");
    }
    ,
    /**
     * Cookie management
     * @type type
     */
    myCookie: {
        options: {
            expires: 7, path: '/',
            domain: undefined, //defaults to the domain where the cookie was created,
            secure: false
        },
        /**
         *
         * @param {string} namespace
         * @param {string} key
         * @param {string} value
         * @returns {void}
         */
        set: function (namespace, key, value, options)
        {
            'use strict';

            options = $.extend({}, this.options, options || {});
            $.cookie(namespace + '_' + key, value, options);
        },
        /**
         * Get stored cookie
         * @param {string} namespace
         * @param {string} key
         * @returns {mixed} value
         */
        get: function (namespace, key)
        {
            'use strict';
            return $.cookie(namespace + '_' + key);
        },
        remove: function (namespace, key)
        {
            'use strict';
            $.removeCookie(namespace + '_' + key, this.options);
        }
    }
    ,
    /**
     *
     * @param {string} string
     * @param {type} notif_container_selector
     * @param {type} show_modal default false (show bootstrap alert)
     * @returns {undefined}
     */
    display_model_errors: function (string, notif_container_selector, show_modal) {
        var message
                , jsonData
                , input_error_class = 'my-form-error'
                , addErrorClass = function (id) {
                    $('#' + id).addClass(input_error_class);
                };
        //remove all errors
        $('.' + input_error_class).removeClass(input_error_class);

        try {
            jsonData = $.parseJSON(string);
            message = '<ul>';
            $.each(jsonData, function (i) {
                if ($.isArray(jsonData[i])) {
                    $.each(jsonData[i], function (j, msg) {
                        message += '<li>' + msg + '</li>';
                    });
                }
                else
                    message += '<li>' + jsonData[i] + '</li>';
                addErrorClass(i);
            });
            message += '</ul>'
        } catch (e) {
            message = string;
        }
        if (MyUtils.empty(show_modal)) {
            this.showAlertMessage(message, 'error', notif_container_selector, show_modal);
        } else {
            BootstrapDialog.alert({
                title: 'WARNING',
                message: message,
                type: BootstrapDialog.TYPE_WARNING,
                closable: true, // <-- Default value is true
            });
        }
    }
    ,
    /**
     *hide model errors
     * @param {string} container_selector
     * @returns {undefined}
     */
    hide_model_errors: function (container_selector) {
        if (typeof container_selector === 'undefined')
            container_selector = '#user-flash-messages';
        var container;
        if (typeof container_selector === 'object') {
            container = container_selector;
        } else {
            container = $(container_selector);
        }
        container.html("").addClass('hidden');
        var error_class = 'my-form-error';
        $('.' + error_class).removeClass(error_class);
    }
    ,
    /**
     * Clear all form values
     * @param {string} form_id
     * @returns {undefined}
     */
    clear_form: function (form_id) {
        $(':input', '#' + form_id).not(':button, :submit, :reset').val('').removeAttr('checked').removeAttr('selected').removeClass('my-form-error');
    }
    ,
    /**
     *
     * @param {string} selector
     * @param {integer} speed
     * @returns {undefined}
     */
    scroll_to: function (selector, speed) {
        if (typeof speed === 'undefined') {
            speed = 2000;
        }
        $('html, body').animate({
            scrollTop: $(selector).offset().top
        }, speed);
    },
    str_pad: function (input, pad_length, pad_string, pad_type) {
        //  discuss at: http://phpjs.org/functions/str_pad/
        // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
        // improved by: Michael White (http://getsprink.com)
        //    input by: Marco van Oort
        // bugfixed by: Brett Zamir (http://brett-zamir.me)
        //   example 1: str_pad('Kevin van Zonneveld', 30, '-=', 'STR_PAD_LEFT');
        //   returns 1: '-=-=-=-=-=-Kevin van Zonneveld'
        //   example 2: str_pad('Kevin van Zonneveld', 30, '-', 'STR_PAD_BOTH');
        //   returns 2: '------Kevin van Zonneveld-----'

        var half = '',
                pad_to_go;

        var str_pad_repeater = function (s, len) {
            var collect = '',
                    i;

            while (collect.length < len) {
                collect += s;
            }
            collect = collect.substr(0, len);

            return collect;
        };

        input += '';
        pad_string = pad_string !== undefined ? pad_string : ' ';

        if (pad_type !== 'STR_PAD_LEFT' && pad_type !== 'STR_PAD_RIGHT' && pad_type !== 'STR_PAD_BOTH') {
            pad_type = 'STR_PAD_RIGHT';
        }
        if ((pad_to_go = pad_length - input.length) > 0) {
            if (pad_type === 'STR_PAD_LEFT') {
                input = str_pad_repeater(pad_string, pad_to_go) + input;
            } else if (pad_type === 'STR_PAD_RIGHT') {
                input = input + str_pad_repeater(pad_string, pad_to_go);
            } else if (pad_type === 'STR_PAD_BOTH') {
                half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
                input = half + input + half;
                input = input.substr(0, pad_length);
            }
        }

        return input;
    },
    /**
     * Print any div contents
     * @param {string} selector
     */
    print_div: function (selector) {
        $(selector).printThis({
            debug: true,
            importCSS: true,
            printContainer: true,
            pageTitle: $('title').html(),
            removeInline: false,
            printDelay: 333,
            header: null,
            formValues: false
        });
    },
    /**
     * Manages input rows in a table
     * e.g Adding Sales Order Items etc
     * @type type
     */
    LineItem: {
        optionDefaults: {
            itemIdInputSelector: undefined,
            headModelId: undefined,
            containerID: 'line_item_wrapper',
            addNewRowSelector: '#add_new_item_line',
            saveClass: 'save-line-item',
            deleteClass: 'delete-line-item',
            insertedRowSelector: 'tr.line-item.inserted',
            autoAddNewRow: true,
            rowTag: 'tr',
            rowContainerTag: 'tbody',
            beforeSave: function (rowTag) {
            },
            afterSave: function (rowTag, response) {
            },
        },
        options: {},
        init: function (options) {
            'use strict';
            var $this = this;
            $this.options = $.extend({}, $this.optionDefaults, options || {});
            $this.delete_line_item();
            $this.save_line_item();
            $this.add_line_item();
        },
        add_line_item: function () {
            var $this = this;
            var add_line_item = function (e) {
                var url = $(e).data('ajax-url');
                var index = $($this.options.insertedRowSelector).length + 1;
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: 'index=' + index,
                    success: function (html) {
                        var container = $('#' + $this.options.containerID).find($this.options.rowContainerTag);
                        container.append(html);
                    }
                });
            };

            //onclick
            $($this.options.addNewRowSelector).off('click').on('click', function (e) {
                e.preventDefault();
                add_line_item(this);
            });
        },
        delete_line_item: function () {
            var $this = this;
            var delete_line_item = function (e) {
                var rowTag = $(e).closest($this.options.rowTag)
                        , url = rowTag.data('delete-url')
                        , item_id = rowTag.find($this.options.itemIdInputSelector).val();
                if (MyUtils.empty(item_id)) {
                    rowTag.remove();
                    return false;
                }

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: 'id=' + item_id,
                    success: function (response) {
                        rowTag.remove();
                    }
                });
            };

            //on click
            $('.' + $this.options.deleteClass).off('click').on('click', function () {
                delete_line_item(this);
            });
        },
        save_line_item: function () {
            var $this = this;
            var input_error_class = 'my-form-error'
                    , show_error = function (rowTag, input_class) {
                        rowTag.find('.' + input_class).addClass(input_error_class).val("");
                        rowTag.addClass('bg-danger');
                    },
                    hide_error = function (rowTag) {
                        rowTag.find('.' + input_error_class).removeClass(input_error_class);
                        rowTag.removeClass('bg-danger');
                    };

            var saved_css_class = 'text-success'
                    , unsaved_css_class = 'text-warning'
                    , mark_as_saved = function (rowTag) {
                        rowTag.find('.' + $this.options.saveClass).removeClass(unsaved_css_class).addClass(saved_css_class);
                    };
            var save_line_item = function (e) {
                //event callback
                var rowTag = $(e).closest($this.options.rowTag);
                $this.options.beforeSave(rowTag);
                var url = rowTag.data('save-url')
                        , data = rowTag.find('input,select,textarea').serialize();
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            //event callback
                            $this.options.afterSave(rowTag, response);
                            var head_model_id = $this.options.headModelId;
                            var head_id = $('#' + head_model_id).val();
                            if (!MyUtils.empty(response.data.head_id) && MyUtils.empty(head_id)) {
                                $('#' + head_model_id).val(response.data.head_id);
                            }
                            hide_error(rowTag);
                            if ($this.options.autoAddNewRow && MyUtils.empty(rowTag.find($this.options.itemIdInputSelector).val())) {
                                $($this.options.addNewRowSelector).trigger('click');
                            }
                            rowTag.find($this.options.itemIdInputSelector).val(response.data.id);
                            mark_as_saved(rowTag);
                        }
                        else {
                            console.log(response.message);
                            hide_error(rowTag);
                            //show error
                            var jsonData = $.parseJSON(response.message);
                            $.each(jsonData, function (i) {
                                show_error(rowTag, i);
                            });
                            //MyUtils.display_model_errors(response.message, $this.options.notif_wrapper_selector);
                        }
                    }
                });
            };

            //on click
            $('#' + $this.options.containerID).find('.' + $this.options.saveClass).off('click').on('click', function () {
                save_line_item(this);
            });
        }
    },
    LineItemHeader: {
        optionsDefault: {
            form_container_selector: undefined,
            head_model_id: undefined,
            cancel_selector: '#cancel_line_item_form',
            submit_selector: '#submit_line_item_form',
            notif_selector: '#line_items_notif_wrapper',
            head_inputs_wrapper: 'line_items_head_inputs_wrapper',
            beforeSubmit: function (e) {
            },
            afterSubmit: function ($this, response) {
                if (response.success) {
                    $($this.options.form_container_selector).html(response.html);
                    MyUtils.showAlertMessage(response.message, 'success', $this.options.notif_selector);
                }
                else {
                    MyUtils.display_model_errors(response.message, $this.options.notif_selector);
                }
            },
        },
        options: {},
        init: function (options) {
            'use strict';
            var $this = this;
            $this.options = $.extend({}, $this.optionsDefault, options || {});
            $this.cancel();
            $this.submit();
        },
        cancel: function () {
            var $this = this;
            var cancel = function (e) {
                var url = $(e).data('ajax-url')
                        , id = $('#' + $this.options.head_model_id).val();
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: 'id=' + id,
                    success: function (html) {
                        $($this.options.form_container_selector).html(html);
                    }
                });
            };
            //on click
            $($this.options.cancel_selector).off('click').on('click', function (e) {
                e.preventDefault();
                cancel(this);
            });
        },
        submit: function () {
            var $this = this;
            var submit = function (e) {
                $this.options.beforeSubmit(e);
                var url = $(e).data('ajax-url')
                        , data = $('#' + $this.options.head_inputs_wrapper).find('input,select,textarea').serialize();
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function (response) {
                        $this.options.afterSubmit($this, response);
                    },
                    error: function (XHR) {
                        console.log(XHR.responseText);
                    }
                });
            };
            //on click
            $($this.options.submit_selector).off('click').on('click', function (e) {
                e.preventDefault();
                submit(this);
            });
        }
    }
}
